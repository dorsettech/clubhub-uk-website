<?php 
	define('WP_USE_THEMES', false);
	require('wp-load.php');

	ini_set('memory_limit', '-1');
	ini_set('max_execution_time', 0);

	function make_post_requst_to_paypal($url, $postdata,$token,$is_post=1, $return_status=0) {
       /*
        * 
        $headers = array(
        'Authorization'  => 'Bearer ' .$token,
        'Accept'       => 'application/json',
        'Content-Type'   => 'application/json',
       );

        
        
        $response_con = wp_remote_post( $url, array(
                'headers'       =>  $headers,
                'method'        =>  'POST',
                'timeout'       =>  45,
                'redirection'   => 5,
                'httpversion'   => '1.0',
                'blocking'      => true,   
                'body'          => $postdata,
                'cookies'       => array()
                )
            );
        $response = wp_remote_retrieve_body( $response_con );
            
        if ( is_wp_error( $response ) ) {
               exit('conection error');
        } 

        // Convert the result from JSON format to a PHP array 
        $jsonResponse = json_decode($response, TRUE);
        return $jsonResponse;
        */
    //global $token;
    $curl = curl_init($url); 
    if($is_post == 1) {
        curl_setopt($curl, CURLOPT_POST, true);
    }
    if($postdata != '') {
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata); 
    }

    if($return_status ==1) {
        curl_setopt($curl, CURLOPT_POSTFIELDS, ''); 
    }

    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Authorization: Bearer '.$token,
                'Accept: application/json',
                'Content-Type: application/json'
                ));
    
    
    curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
    $response = curl_exec( $curl );
    // echo $token;
    // if($return_status == 1) {
    //     echo 'exit';
    //     print_r($url);
    //     print_r($response);
    //     exit;
    // }


    if (empty($response) || $return_status == 1) {

        if($return_status == 1) {
            curl_error($curl);
            $info = curl_getinfo($curl);
            curl_close($curl);

            //something wrong with paypal header response
            // status staticlly sending 204
            return 204;
            // return $info['http_code'];
        
        } else {
            // some kind of an error happened
            die(curl_error($curl));
            curl_close($curl); // close cURL handler
        }
    } else {

        $info = curl_getinfo($curl);
        //echo "Time took: " . $info['total_time']*1000 . "ms\n";
        curl_close($curl); // close cURL handler
        if($info['http_code'] != 200 && $info['http_code'] != 201 ) {
            echo "Received error: " . $info['http_code']. "\n";
            echo "Raw response:".$response."\n";
            // die();
        }
    }

    // Convert the result from JSON format to a PHP array 
    $jsonResponse = json_decode($response, TRUE);
    return $jsonResponse;
    }

	global $wpdb;

	$args = array(
        'post_type'     =>  'estate_property',
        'posts_per_page' =>  -1,
        'post_status'      =>  array( 'any' ),
        'meta_query' => array(
        	array(
				'key' => 'club_subscription_id',
				'value' => '',
				'compare' => '!=',
			),
        ),
    );
    $clubs = new WP_Query( $args );
    // echo '<pre>';
    // print_r($clubs);
    // exit;

    $paypal_status              =   esc_html( get_option('wp_estate_paypal_api','') );
	$host                       =   'https://api.sandbox.paypal.com';
	if($paypal_status == 'live'){
		$host='https://api.paypal.com';
	}

	$clientId                       =   esc_html( get_option('wp_estate_paypal_client_id','') );
	$clientSecret                   =   esc_html( get_option('wp_estate_paypal_client_secret','') );

	$url                =   $host.'/v1/oauth2/token'; 
    $postArgs           =   'grant_type=client_credentials';
    $token              =   wpestate_get_access_token($url,$postArgs);
    echo $token;
	foreach ($clubs->posts as $club) {
		$subscriptions_id = get_post_meta($club->ID, 'club_subscription_id', true);
		$url = $host.'/v1/billing/subscriptions/'.$subscriptions_id;

		$dump_req = '';
		$json = '';
		$subscription_response_json = make_post_requst_to_paypal($url, $json,$token,0);
		
		print_r($subscription_response_json);
		if(isset($subscription_response_json['status']) && $subscription_response_json['status'] == 'ACTIVE') {
			update_post_meta($club->ID, 'club_active_subscription', 1);
			echo "activated club id". $club->ID;
			echo '<br/>';
			//additional field to update for the package
			update_post_meta($club->ID, 'prop_weblink', 1);
			update_post_meta($club->ID, 'weblink-expires', date('Y-m-d',strtotime("+1 year")));

		}
	}
