<?php 
	define('WP_USE_THEMES', true);
	require('wp-load.php');

	ini_set('memory_limit', '-1');
	ini_set('max_execution_time', 0);
	global $wpdb;

	echo '<pre>';
	echo "Started ".current_time("Y-m-d Y-m-d h:i:s A") ."<br/>";

	// $path= wpestate_get_pin_file_path_write();
	$path=get_template_directory().'/pins.txt';


	wp_suspend_cache_addition(true);
	set_time_limit (0);
	$counter                    =   0;
	$unit                       =   get_option('wp_estate_measure_sys','');
	$currency                   =   get_option('wp_estate_currency_symbol','');
	$where_currency             =   get_option('wp_estate_where_currency_symbol', '');
	$cache                      =   get_option('wp_estate_cache','');
	$place_markers              =   array();
	$markers                    =   array();


	$args = array(
		'post_type'      =>  'estate_property',
		'post_status'    =>  'publish',
		'cache_results'  => false,
		'update_post_meta_cache'  =>   false,
		'update_post_term_cache'  =>   false,
		'nopaging'       =>  'true',
		// 'posts_per_page'		=> 500,
	);


	$prop_selection = new WP_Query($args);
	// print_r($prop_selection);
	$custom_advanced_search =   get_option('wp_estate_custom_advanced_search','');
	$show_slider_price      =   get_option('wp_estate_show_slider_price','');
	$has_slider             =   0; 

	while($prop_selection->have_posts()): $prop_selection->the_post();
		$counter++;
		$markers[]=wpestate_pin_unit_creation( get_the_ID(),$currency,$where_currency,$counter );
	endwhile;

	wp_reset_query(); 
	wp_suspend_cache_addition(false);


	//just a validation if the query went okay or not
	if(sizeof($markers)> 0) {
		$myfile = fopen($path, "w") or die("Unable to open file!");
		fwrite($myfile, json_encode($markers));
		fclose($myfile);
	}
	echo "Ended ".current_time("Y-m-d Y-m-d h:i:s A") ."<br/>";
	echo "all done!! Gn";
