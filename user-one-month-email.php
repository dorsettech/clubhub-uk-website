<?php 
	define('WP_USE_THEMES', false);
	require('wp-load.php');

	ini_set('memory_limit', '-1');
	ini_set('max_execution_time', 0);
	echo '<pre>';
	global $wpdb;

	if(!function_exists( 'send_one_month_email' )) {
		function send_one_month_email ($email) {

			$promo_message = "<p>We'd love to know what you think!</p>";

			$promo_message .= "<p>Here at Club Hub we're always striving to be the best! We're constantly updating our platform to offer you the best user experience, and we're in daily contact with club owners and kids' activity providers to bring you the best activities in your local area all under one app/website.</p>";

			$promo_message .= "<p>We'd love to hear about your experience so far.</p>";

			$promo_message .= "<p>There are a number of ways you can do this, and we promise it won't take long!</p>";

			$promo_message .= "<p><b>Review our app!</b><br/>Just click on the Club Hub app in the <a href='https://itunes.apple.com/in/app/club-hub-uk-free/id1139824440?mt=8'>App Store</a> or <a href='https://play.google.com/store/apps/details?id=uk.co.clubhubukfree'>Google Play</a>, click on the stars and this will take your straight to 'Write a review'.";

			$promo_message .= "<p>You can also review us on <a href='https://www.facebook.com/pg/clubhubapp/reviews/?ref=page_internal'>Facebook</a> or <a href='https://www.google.com/search?q=Club+HUb+UK&rlz=1C1CHBF_enGB883GB883&oq=Club+HUb+UK&aqs=chrome..69i57j69i64j69i61l2.1455j0j4&sourceid=chrome&ie=UTF-8#'>Google</a>.</p>";

			$promo_message .= "<p>** Why not share our details with your friends and family and watch the Club Hub community grow **</p>";

			$promo_message .= "<p>We really appreciate your feedback and support. Please follow us on our <a href='https://instagram.com/clubhubuk'>Instagram</a>, <a href='https://www.facebook.com/clubhubapp/'>Facebook</a> and <a href='https://twitter.com/clubhubuk'>Twitter</a> pages where you can enter our monthly competitions and keep up to date with all our latest news.</a>.</p>";

			$promo_headers = 'From: noreply  <noreply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n".
			'Reply-To: noreply@'.$_SERVER['HTTP_HOST']. "\r\n" .
			'X-Mailer: PHP/' . phpversion();
			add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));

			wp_mail(
				$email,
				"We'd love to know what you think!",
				$promo_message,
				$promo_headers
			);
		}
	}





	$previous_month_date = date("Y-m-d",strtotime("-1 month"));

	$query = "SELECT * FROM `wp_users` WHERE `user_registered` = '".$previous_month_date."'";

	$user_data = $wpdb->get_results( $query );
	$sender_list = array();
	if(count($user_data)> 0) {
		foreach ($user_data as $user_detail) {
			send_one_month_email($user_detail->user_email);
			array_push($sender_list, $user_detail->ID);
		}
	}
	echo "Email send to following userid: ";
	echo implode(", ", $sender_list);
	exit;

?>