<?php 
define('WP_USE_THEMES', false);
require('wp-load.php');

$today = date("Y-m-d");

$args = array( 
	'post_type' => 'estate_property',
	'nopaging' => true,
    'fields'    => 'ids'
);

$postids = get_posts($args);

if ( $postids ) { 
    foreach( $postids as $postID ) {
        $expiry = get_post_meta( $postID,'local-expires',true);
        $localman = get_post_meta( $postID,'prop_localman',true);
        if ( strtotime($expiry) ) {
            if ( $expiry < $today ) {
                update_post_meta( $postID,'prop_localman',false);
                update_post_meta( $postID,'local-expires',false);
                print_r( "Expiry: $expiry, Today: $today | ");
            }
        }
        if ( $localman != 1 && $expiry ) {
            print_r( "local: $localman | ");
            update_post_meta( $postID,'local-expires','');
        }
    }
}

?>