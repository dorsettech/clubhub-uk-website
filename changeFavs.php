<?php 
define('WP_USE_THEMES', false);
error_reporting(E_ALL); ini_set('display_errors', 1);
require('wp-load.php');

if($_GET['club'] != '' && $_GET['user'] != '' && $_GET['change'] != ''){
    
    $userID                         =   $_GET['user'];
	$user_option                    =   'favorites'.$userID;
	$curent_fav                     =   get_option($user_option);
    
	if(!empty($curent_fav)){
		if($_GET['change'] == 'add'){
			//add
			if(!in_array($_GET['club'],$curent_fav)){
				array_push($curent_fav,$_GET['club']);
				update_option($user_option,$curent_fav);
			}
		   //return '[{"result":"yes","error":"no"}]';

		} else {
			//remove
			if(in_array($_GET['club'],$curent_fav)){
				if(($key = array_search($_GET['club'], $curent_fav)) !== false) {
					unset($curent_fav[$key]);
				}
				update_option($user_option,$curent_fav);
			}
		  // return '[{"result":"yes","error":"no"}]';
		}
	}else{
		$default_options[] = $_GET['club'];
		//echo "<pre>";print_r($default_options);die;
		add_option($user_option,$default_options);
	
	}
    
} else {
	//return '[{"result":"no","error":"invalid request"}]';
}

$result = array();
$clubs = array();
$clubs[] = array(
    'results' => 'finished'
);
$result['result'] = $clubs;
$result['status'] = 'success';
if($_GET['callback']){
	echo $_GET['callback'].'('.json_encode( $result ).')';

} else {
	echo json_encode( $result,JSON_PRETTY_PRINT );

}

?>