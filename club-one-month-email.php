<?php 
	define('WP_USE_THEMES', false);
	require('wp-load.php');

	ini_set('memory_limit', '-1');
	ini_set('max_execution_time', 0);
	echo '<pre>';
	global $wpdb;

	if(!function_exists( 'send_one_month_club_email' )) {
		function send_one_month_club_email ($email) {

			$message = "<p><b><u>Have you enabled bookings yet on Club Hub?</u></b></p>";
					
			$message .= "<p>You can now <a href='https://clubhubuk.co.uk/user-dashboard/'><span style='padding: 5px; background: #FEFF00;'>\"Enable Free Trial\"</span></a> on your <a href='https://clubhubuk.co.uk/user-dashboard/'><b>\"My Clubs\"</b></a> page. This enables a Book Now Button where parents go straight through to your desired website link and you also receive the parents email address so you can contact them directly. </p>";

			$message .= "<p>After the free trial it is a one off fee of £2.50 per year per listing to <a href='https://clubhubuk.co.uk/user-dashboard/'>upgrade to the web link package</a> and we take <b>0% commission</b>.</p>";

			$message .= "<p>If you have any questions on the above or need any help, please do let us know.</p>";

			$message .= esc_html__("Kind regards,", 'wpestate'). "\r\n";
			$message .= esc_html__("Tessa Robinson", 'wpestate'). "\r\n";

			add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));
			$headers = 'From: noreply  <noreply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n".
				'Reply-To: noreply@'.$_SERVER['HTTP_HOST']. "\r\n" .
				'X-Mailer: PHP/' . phpversion();
			$e = wp_mail(
					$to,
					esc_html__( 'Have you enabled bookings yet on Club Hub?','wpestate'),
					htmlspecialchars_decode($message, ENT_QUOTES),
					$headers
			);
		}
	}





	$previous_month_date = date("Y-m-d",strtotime("-1 month"));

	$query = "SELECT * FROM `wp_posts` WHERE `post_type` = 'estate_property' AND `post_status` = 'publish' AND `post_date` BETWEEN '" . date('Y-m-d', strtotime('-30 days')) . "'
         AND '" . date('Y-m-d', strtotime('-29 days')) . "'";

	$results = $wpdb->get_results( $query );
	$sender_list = array();
	if(count($results)> 0) {
		foreach ($results as $res) {
			$user_detail = get_userdata($res->post_author);
			send_one_month_club_email($user_detail->user_email);
			array_push($sender_list, $user_detail->ID);
		}
	}
	echo "Email send to following userid: ";
	echo implode(", ", $sender_list);
	exit;

?>