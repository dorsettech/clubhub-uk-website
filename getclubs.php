<?php 

define('WP_USE_THEMES', false);
error_reporting(E_ALL); ini_set('display_errors', 0);
require('wp-load.php');
$listsize = 0;
//(meters) public static function vincentyGreatCircleDistance( $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000){
		
	
	###################### Get Distance  ###############################
	function getDistance( $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 3959){
			
		// convert from degrees to radians
		$latFrom = deg2rad($latitudeFrom);
		$lonFrom = deg2rad($longitudeFrom);
		$latTo = deg2rad($latitudeTo);
		$lonTo = deg2rad($longitudeTo);

		$lonDelta = $lonTo - $lonFrom;
		$a = pow(cos($latTo) * sin($lonDelta), 2) +
			pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
		$b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

		$angle = atan2(sqrt($a), $b);
		return number_format($angle * $earthRadius,1);
		//return 1;
	}
	###################### END Get Distance  ###############################

		if($_GET['page']){
			$pager = $_GET['page'];
		} else {
			$pager = 1;
		}

		$args = array( 
			'post_type' => 'estate_property',
			'status' 	=> 'publish',
			'showposts' => -1,
			'order' 	=> 'DESC',
			'orderby'   => 'meta_value',
			'paged' 	=>  $pager

		);

		//category
		if($_GET['ver'] != 2){
			if($_GET['interest'] && $_GET['interest'] != 'all'){
				$cats = array(
					'tax_query' => array(
						array(
							'taxonomy' => 'property_category',
							'field'    => 'slug',
							'terms'    => $_GET['interest']
						),
					),
				);
			
				$args = array_merge($cats, $args);
			}
		} else { //multi-category
			if($_GET['interest'] && $_GET['interest'] != 'all' && $_GET['interest'] != ''){
				$fullreq = explode('|', $_GET['interest']);
				$cats = array(
					'tax_query' => array(
						array(
							'taxonomy' => 'property_category',
							'field'    => 'slug',
							'terms'    => $fullreq
						),
					),
				);
			
				$args = array_merge($cats, $args);
			}
		}

		//age
		if(isset($_GET['age']) || isset($_GET['lat']) || isset($_GET['days'])){
			if($_GET['days'] != '' && $_GET['days'] != 'all'){
				$a = explode(',',$_GET['days']);
				foreach($a as $v){
					$days_of1['key']      = 'dayheld';
					$days_of1['value']    = $v;
					$days_of1['compare']  = 'LIKE';
					$meta_query[] = $days_of1;
				}
			} 
			if($_GET['age']){
				$minage_array=array();
				$maxage_array=array();
				if( isset($_GET['age']) && $_GET['age'] != '' ){
						$childage = explode(',',$_GET['age']);
						sort($childage);
						$minAgeval = (int) intval($childage[0]);
						$maxAgeVal = (int) intval(end($childage));
						$minage_array['key']      = 'minimum_age';
						$minage_array['value']    = $minAgeval;
						$minage_array['type']     = 'numeric';
						$minage_array['compare']  = '<='; 
						$meta_query[]            = $minage_array;
						$maxage_array['key']      = 'maximum_age';
						$maxage_array['value']    = $maxAgeVal;
						$maxage_array['type']     = 'numeric';
						$maxage_array['compare']  = '>='; 
						$meta_query[]            = $maxage_array;
					} 
				
			}

			//if(isset($_GET['order']) && $_GET['order'] != '' && $_Get['order'] != 0){
				if($_GET['lat'] && $_GET['interest'] != 'activity-boxes' && $_GET['interest'] != 'online-classes'){
			
					$lat = substr($_GET['lat'], 0, 9);
					$lng = substr($_GET['long'], 0, 9);

					$distance = 50;
					// earth's radius in miles = ~3959
					$radius = 3959;

					// latitude boundaries
					$maxlat = substr(($lat + rad2deg($distance / $radius)), 0, 9);
					$minlat = substr(($lat - rad2deg($distance / $radius)), 0, 9);

					// longitude boundaries (longitude gets smaller when latitude increases)
					$maxlng = substr(($lng + rad2deg($distance / $radius / cos(deg2rad($lat)))), 0, 9);
					$minlng = substr(($lng - rad2deg($distance / $radius / cos(deg2rad($lat)))), 0, 9);
					$lat_lng_meta_query =
						array (
								'relation' => 'AND',
								array(
									'key' => 'property_latitude',
									'value' =>  array($minlat, $maxlat),
									'compare' => 'BETWEEN',
									'type' => 'DECIMAL'
								),
								array(
									'key' => 'property_longitude',
									'value' =>  array($minlng, $maxlng),
									'compare' => 'BETWEEN',
									'type' => 'DECIMAL'
								)
						);
					$meta_query[] = $lat_lng_meta_query;
				}
			//}

			$ages = array(
				'meta_query' => $meta_query,
				'geo_query' => $geo_query
			);
			$args = array_merge($ages, $args);
		}

		if(!isset($args['tax_query'])) {
			$args['showposts'] = 4247;
		}
		//echo "<pre>";print_r($args);die;
		//echo "<pre>";print_r($args);die;

		$query = new WP_Query( $args );
		
		$tday = date("Y-m-d");
		$result = array();
		$clubs = array();

			if($query->have_posts()){ 
				while( $query->have_posts() ) : $query->the_post();
			
				//thumbnails
					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
					if(count($thumb) > 0){
						$urlthumb = $thumb['0'];
					} else {
						$urlthumb = 'images/fallbackthumb.jpg';
					}
					$thumbbig = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
					if(count($thumbbig) > 0){
						$urlthumbbig = $thumbbig['0'];
					} else {
						$urlthumbbig = 'images/fallbackbig.jpg';
					}
				
					$time = $dates = $dateraw = $timeraw = '';
					$dateraw = get_post_meta($post->ID,'dayheld',true);
					$timeraw = get_post_meta($post->ID,'timheld',true);
					if($dateraw != ''){
						$dates = $dateraw;
					}
					if($timeraw != ''){
						$time = $timeraw;
					}
				
					$bestemail = '';
					$emailraw = get_post_meta($post->ID,'bestemail',true);
					if($emailraw != ''){
						$bestemail = $emailraw;
					}
				
					//price
					$clubprice = 'POA';
					$priceper = '';
					$price_ = 0;
					$priceraw = get_post_meta($post->ID,'property_price',true);
					if($priceraw != ''){
						$clubprice = '£'.number_format($priceraw, 2, '.', '');
						$price_ = number_format($priceraw, 2, '.', '');
						$priceper = get_post_meta($post->ID,'property_price_per',true);
						if($priceper != ''){
							$clubprice = $clubprice.' '.$priceper;
						}
					}

					//phone
					$clubphone = '';
					$phoneraw = get_post_meta($post->ID,'clubphone',true);
					if($phoneraw != ''){
						$clubphone = $phoneraw;
					}

					//favourites
					$isfav = 'no';
					$favtext = 'Add To Favourites';
					if($_GET['guid']){
						$userID                         =   $_GET['guid'];
						$user_option                    =   'favorites'.$userID;
						$curent_fav                     =   get_option($user_option);
						if ( !is_array($curent_fav) || empty($curent_fav) ) {
							$curent_fav = array();
						}
						//print_r($curent_fav);
						if (in_array($post->ID, $curent_fav)) {
							$isfav = 'yes';
							$favtext = 'Remove From Favourites';
						}
					}
					//age
					$agerange = 'N/A';
					$ageminraw = get_post_meta($post->ID,'minimum_age',true);
					$agemaxraw = get_post_meta($post->ID,'maximum_age',true);
					if($ageminraw != ''){
						$agerange = $ageminraw;
						if($agemaxraw != ''){
							$agerange .= ' - '.$agemaxraw;
						} else {
							$agerange .= '+';
						}
					}

					if($_GET['lat']){
						$lat = get_post_meta($post->ID,'property_latitude',true);
						$long = get_post_meta($post->ID,'property_longitude',true);
						$distance = getDistance($lat, $long, $_GET['lat'], $_GET['long']);
					} else {
						$lat = null;
						$long = null;
						$distance = null;
					}


					//days active
					//echo 'date: '.get_the_date('Y-m-d',$post->ID);
					$start = strtotime(get_the_date('Y-m-d',$post->ID));
					//echo 'str1: '.$start;

					$today = strtotime(date('Y-m-d'));
					//echo 'today: '.date('Y-m-d');
					//echo 'str2:'.$today;

					$daysold = ($today - $start) / 86400;
					
					if($daysold > 1){
						$daysold .= ' days';
					} else {
						$daysold .= ' day';
					}

				//echo $daysold.' days';

				//category
				$term_list = wp_get_post_terms($post->ID, 'property_category', array("fields" => "names"));
				if(count($term_list)> 0){
					//print_r($term_list);die;
					$maincat = implode(',',$term_list);
				} else {
					$maincat = 'N/A';
				} 
				//type
				$term_list2 = wp_get_post_terms($post->ID, 'property_action_category', array("fields" => "names"));
				if(count($term_list2)> 0){
					$maintype = $term_list2[0];
				} else {
					$maintype = 'N/A';
				} 

				//$address1 = $town = $county = $post = '';
				$address1 = get_post_meta($post->ID,'property_address',true);
				$town = get_post_meta($post->ID,'property_state',true);
				$county = get_post_meta($post->ID,'property_county',true);
				$postcode = get_post_meta($post->ID,'property_zip',true);

					
				//calendar
				$calendarenable = 0;
				$calraw = get_post_meta($post->ID,'calendar-expires',true);
				if(count($calraw)> 0){
					if($tday < $calraw){
						$calendarenable = 1;
					} 
				}

				//local feature
				$localfeature = 0;
				$localraw = get_post_meta($post->ID,'prop_localman',true);
					//if($_GET['search_location'] != '') {
						if($localraw == 1 && $distance != null && $distance < 5){
							$localfeature = 1;
						} else {
							$localfeature = 0;
						}
					// }
					// else{
					// 	if($localraw == 1){ //$localraw == 1 || 
							
					// 		$localfeature = 1;
					// 	}else{
					// 		$localfeature = 0;
					// 	}
					// }

				$prop_weblink_url = get_post_meta($post->ID,'prop_weblink',true);
				$weblink_url ='';
				if($prop_weblink_url == 1) {
					$weblink_url = get_post_meta($post->ID,'website_url',true);
				}

			// Add a club entry
			
				//new age filter
				if($_GET['age'] != '' && intval($agemaxraw) >= intval($_GET['age']) && intval($ageminraw) <= intval($_GET['age'])){
					
				// if($_GET['age'] != ''){
					$clubs[] = array(
						'id' => $post->ID,
						'title' => get_the_title(),
						'content' => strip_tags(get_the_content(), '<br>'),
						//'excerpt' => get_the_excerpt(),
						'author' => get_the_author(),
						'thumbnail_images' => array( 
							'thumbnail' => $urlthumb,
							'large' => $urlthumbbig
						),
						'clubprice' => $clubprice,
						'clubage' => $daysold,
						'clubcat' => $maincat,
						'clubtype' => $maintype,
						'clubphone' => $clubphone,
						'time' => $time,
						'email' => $bestemail,
						'dates' => $dates,
						'agestat' =>$agestat,
						'agerange' => $agerange,
						'minage' => $ageminraw,
						'maxage' => $agemaxraw,
						'calendarenable' => $calendarenable,
						'cluburl' => get_permalink($post->ID),
						'lat' => $lat,
						'long' => $long,
						'distance' => $distance,
						'address1' => $address1,
						'town' => $town,
						'county' => $county,
						'post' => $postcode,
						'isFav' => $isfav,
						'localfeature' => $localfeature,
						'favText' => $favtext,
						'weblink'	=> $weblink_url,
						'price_'    => $price_
					);
					$listsize = $listsize+1;
			}
			
			endwhile;

				//order by distance
			/* $sdist = array();
				foreach ($clubs as $key => $row)
				{
					$sdist[$key] = $row['distance'];
				}
				array_multisort($sdist, SORT_ASC, $clubs);*/

				array_multisort(array_column($clubs, 'localfeature'),  SORT_DESC, array_column($clubs, 'distance'), SORT_ASC, $clubs);
				//echo '<pre>'; print_r($clubs); exit;
				$price = array_column($clubs, 'price_');
				
				if(isset($_GET['order']) && $_GET['order'] == 2){
					array_multisort($price, SORT_ASC, $clubs);
				}else if(isset($_GET['order']) && $_GET['order'] == 1){
					array_multisort($price, SORT_DESC, $clubs);
				}else{
					//array_multisort($price, SORT_DESC, $clubs);
				}
			

				/*$sfeature = array();
				foreach ($clubs as $key => $row)
				{
					$sfeature[$key] = $row['localfeature'];
				}
				array_multisort($sfeature, SORT_DESC, $clubs);*/
				
				
				$result['posts'] = $clubs;
				$result['numfound'] = $listsize;
				$result['numresults'] = $listsize;
				$result['numpages'] = 1;
				$result['status'] = 'ok';

				wp_reset_query();
			
			//echo json_encode( $clubs );

			} else {
				$result['posts'] = array();
				$result['numfound'] = 0;
				$result['numresults'] = 0;
				$result['numpages'] = 0;
				$result['status'] = 'ok';
			} 


//echo "<pre>";print_r($result);die;

if($_GET['callback']){
	echo $_GET['callback'].'('.json_encode( $result ).')';

} else {
	echo json_encode( $result,JSON_PRETTY_PRINT );
}


?>