<?php
/*
Plugin Name: DT GeoPosts
Plugin URI: http://fyaconiello.github.com/wp-geo-posts/
Description: Extends WP_Query with lat, long and radius, and Distance Sort via WP_GeoQuery
Version: 1.0
Author: Dorset Tech
Author URI: https://dorset.tech
License: GPL2
*/
/*
Copyright 2022 Dorset Tech  (email : david@dorset.tech)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as 
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


if(!class_exists('WP_Geo_Posts'))
{
	class WP_Geo_Posts
	{
		/**
		 * Construct the plugin object
		 */
		public function __construct()
		{
			// register actions
			add_action('init', array(&$this, 'init'));
		} // END public function __construct


		/**
		 * hook into WP's init action hook
		 */
		public function init()
		{
			// Import WP_GeoQuery class
			require_once(sprintf("%s/query.php", dirname(__FILE__)));
		} // END public static function activate

		/**
		 * Activate the plugin
		 */
		public static function activate()
		{
			// Do nothing
		} // END public static function activate
		
		/**
		 * Deactivate the plugin
		 */		
		public static function deactivate()
		{
			// Do nothing
		} // END public static function deactivate
	} // END class WP_Geo_Posts
} // END if(!class_exists('WP_Geo_Posts'))

if(class_exists('WP_Geo_Posts'))
{
	// Installation and uninstallation hooks
	register_activation_hook(__FILE__, array('WP_Geo_Posts', 'activate'));
	register_deactivation_hook(__FILE__, array('WP_Geo_Posts', 'deactivate'));
	
	// instantiate the plugin class
	$wp_geo_posts_plugin = new WP_Geo_Posts();
}
