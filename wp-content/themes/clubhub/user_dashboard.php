<?php
// Template Name: User Dashboard
// Wp Estate Pack
if ( !is_user_logged_in() ) {
    wp_redirect(  esc_html( home_url() ) );exit();
} 

if ( !wpestate_check_user_level()){
   wp_redirect(  esc_html( home_url() ) );exit(); 
}

$current_user = wp_get_current_user();
$userID                         =   $current_user->ID;
$user_login                     =   $current_user->user_login;
$user_pack                      =   get_the_author_meta( 'package_id' , $userID );
$user_registered                =   get_the_author_meta( 'user_registered' , $userID );
$user_package_activation        =   get_the_author_meta( 'package_activation' , $userID );   
$paid_submission_status         =   esc_html ( get_option('wp_estate_paid_submission','') );
$price_submission               =   floatval( get_option('wp_estate_price_submission','') );
$submission_curency_status      =   esc_html( get_option('wp_estate_submission_curency','') );
$edit_link                      =   wpestate_get_dasboard_edit_listing();
$floor_link                     =   '';
$processor_link                 =   wpestate_get_procesor_link();
 $th_separator                  =   get_option('wp_estate_prices_th_separator','');
if( isset( $_GET['delete_id'] ) ) {
    if( !is_numeric($_GET['delete_id'] ) ){
        exit('you don\'t have the right to delete this');
    }else{
        $delete_id= intval ( $_GET['delete_id']);
        $the_post= get_post( $delete_id); 
        if( $current_user->ID != $the_post->post_author ) {
            exit('you don\'t have the right to delete this');;
        }else{
            // delete attchaments
            $arguments = array(
                'numberposts'   => -1,
                'post_type'     => 'attachment',
                'post_parent'   => $delete_id,
                'post_status'   => null,
                'exclude'       => get_post_thumbnail_id(),
                'orderby'       => 'menu_order',
                'order'         => 'ASC'
            );
            $post_attachments = get_posts($arguments);
            
            foreach ($post_attachments as $attachment) {
                wp_delete_post($attachment->ID);                      
            }
            wp_delete_post( $delete_id ); 
        }  
    }
}  
?>
<style type="text/css">
    span.clone_club{
        border-right: 1px solid #e8ebf0;
        width: 45px;
        height: 45px;
        float: left;
        padding: 10px 0px 0px 10px;
        cursor: pointer;
    }
    .clone_club i {
        font-size: 17px;
        padding: 5px;
        color: #bdc4d2;
        cursor: pointer;
        width: 22px;
        height: 23px;
        margin-right: 7px;
    }
</style>
<?php  
get_header();
$options=wpestate_page_details($post->ID);
?> 
    
<div class="row is_dashboard">
    <?php
    if( wpestate_check_if_admin_page($post->ID) ){
        if ( is_user_logged_in() ) {   
            get_template_part('templates/user_menu'); 
        }  
    }
    ?> 
    <style type="text/css">
        #popupalert {
            width: 50%;
            text-align: center;
            margin-left: auto;
            margin-right: auto;
            background-color: rgba(232, 30, 124, 1);
            padding: 10px 20px;
            color: white;
        }
        #popupalert h2, #popupalert p strong {
            color: #f4d919 !important;
        }
        #popupalert h4{
            font-size: 1.5em;
        }
        #popupalert h2 {
            margin-top: 15px;
            line-height: 2em;
            font-size: 2em;
        }
        #popupalert p {
            font-size: 1.8em;
        }
        @media (max-width: 1220px){
            #popupalert {
                width: 95%;
            }
        }

    </style>
    <div class="dashboard-margin">
        
        <div class="dashboard-header">
            <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
                <h1 class="entry-title listings-title-dash"><?php the_title(); ?></h1>
            <?php } ?>
        </div>    
        <?php echo do_shortcode('[dt-dashboard-toggle]'); ?>
        <?php if( isset($_GET['pop']) ){
            //echo '<div id="popupalert"><h2>Upgrade Your Listing:</h2><h4>The Only Way for Parents to Message you through Club Hub.</h4><p><strong>Only £12 per year per listing</strong></p><p><strong style="font-size:0.9em;">(If you have more than 5 listings please get in touch for huge discounts)</strong></p></div>';
            //echo '<div id="popupalert" style="width: 59%;"><h2>Nominations are now open for the Club Hub Awards 2019!</h2><h4><a target="_blank" href="/club-hub-event-2019/)">Click Here Now to find out how YOU can be nominated</a></h4></div>';
 echo '<div id="popupalert" style=""><div id="poprightpink" style="width: 78%;margin-left: 10%;display:inline-block;vertical-align: top;padding: 25px 25px;">
 <h4>
	Have you enabled bookings yet? You can now <span style="background: #f4d919;padding: 5px;color: #000;font-size: 16px;">Enable Free Trial</span> or <span style="background: #28a52b;padding: 4px;font-size: 16px;color: #fff;">Upgrade</span> to the £2.50 web link package. This enables parents to go straight through to your desired website link. We don\'t take any commission just a one off fee of £2.50 per year!
 </h4>
 </div></div>';
//Don\'t forget to upgrade to our amazing Web Link Package to enable parents to get in touch with you easily. Only £2.50 per Listing per year! Click the Green Upgrade Button Now!
        }?>
        
        
        
        <div class="row admin-list-wrapper flex_wrapper_list">    
        <?php
        $prop_no      =   intval( get_option('wp_estate_prop_no', '') );
        $paged        = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
                'post_type'        =>  'estate_property',
                'author'           =>  $current_user->ID,
                'paged'             => $paged,
                'posts_per_page'    => $prop_no,
                'post_status'      =>  array( 'any' )
            );


        $prop_selection = new WP_Query($args);
        if( !$prop_selection->have_posts() ){
            print '<h4 class="no_list_yet">'.esc_html__( 'You don\'t have any clubs yet!','wpestate').'</h4>';
        }

        while ($prop_selection->have_posts()): $prop_selection->the_post();          
            get_template_part('templates/dashboard_listing_unit'); 
        endwhile;
        
        kriesi_pagination($prop_selection->max_num_pages, $range =2 , false);
        ?>    
        </div>
    </div>
</div>  

<?php 
wp_reset_query();
get_footer(); 
?>
