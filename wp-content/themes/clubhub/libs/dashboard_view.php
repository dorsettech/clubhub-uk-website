<?php
/*
    *   New View for /user-dashboard/
    *   Add view toggle
    *   View 1 Default with images
    *   View 2 Alphabetical text list
    *
    *
*/

function dt_dashboard_toggle() {
    ob_start();
    
    ?>
    <script>
    jQuery(function() {
        
        function changeView(view) {
            jQuery('.user_dashboard_listed').toggleClass('hidden');
            jQuery('.user_dashboard_listed:nth-child(6)').toggleClass('hidden');
            jQuery('.user_dashboard_listed:nth-child(6)').toggleClass('weblink');
            jQuery('.blog_listing_image').toggleClass('hidden');
            jQuery('.flexdashbaord').toggleClass('col-md-6');
            jQuery('.flexdashbaord').toggleClass('col-sm-6');
            jQuery('.flexdashbaord').toggleClass('text-view');
            if (this.view == "text") {
                
            } else {
                
            }
        }
        
        jQuery('#imageToggle').click(function() {
            if (jQuery('#textToggle').hasClass('toggle-on') && (jQuery('#imageToggle').hasClass('toggle-on') == false)) {
                jQuery('#imageToggle').toggleClass('toggle-on');
                jQuery('#textToggle').toggleClass('toggle-on');
                changeView("image");
            }
        });
        jQuery('#textToggle').click(function() {
            if (jQuery('#imageToggle').hasClass('toggle-on') && (jQuery('#textToggle').hasClass('toggle-on') == false)) {
                jQuery('#textToggle').toggleClass('toggle-on');
                jQuery('#imageToggle').toggleClass('toggle-on');
                changeView("text");
            }
        });
    });
    </script>

        <div class="vc_row">
            <div class="col-md-12"><br><!--
                <h4 class="toggle-title">Toggle Views [WIP]</h4>-->
            </div>
        </div>

        <div class="vc_row">
            <div class="col-sm-3 col-xs-6">
                <button id="imageToggle" class="toggle-button toggle-on">Image View</button>
            </div>
            <div class="col-sm-3 col-xs-6">
                <button id="textToggle" class="toggle-button">Text View</button>
            </div>
        </div>
    <?php
    
    return ob_get_clean();
}
add_shortcode('dt-dashboard-toggle', 'dt_dashboard_toggle');