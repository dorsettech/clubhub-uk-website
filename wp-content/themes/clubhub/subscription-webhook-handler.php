<?php
// Template Name: Suspended Webhook
// Wp Estate Pack

// if(isset($_POST)) {


	$file_path = get_template_directory().'/log.txt';
	$myfile    = fopen($file_path, "a") or die("Unable to open file!");

	$req 	   = file_get_contents('php://input');

	function validate_subscription_for_club($subscription_id) {
		$file_path = get_template_directory().'/log.txt';
		$myfile    = fopen($file_path, "a") or die("Unable to open file!");

		$filepath  = get_template_directory().'/ad_recurring_log.txt';
		$fpath     = fopen($filepath, "a") or die("Unable to open file!");

		$paypal_status                  =   esc_html( get_option('wp_estate_paypal_api','') );

        $host                           =   'https://api.sandbox.paypal.com';  

        if($paypal_status=='live'){
            $host='https://api.paypal.com';
        }

        $url                =   $host.'/v1/oauth2/token'; 
        $postArgs           =   'grant_type=client_credentials';
        $token              =   wpestate_get_access_token($url,$postArgs);

        $url = $host.'/v1/billing/subscriptions/'.$subscription_id;


        $dump_req = '';
        $json = '';

        $subscription_response_json = wpestate_make_post_call($url, $json,$token,0);

        $plan_after_one_month = strtotime($subscription_response_json['start_time'] . '+ 1 month');
        // $plan_start_time = strtotime($subscription_response_json['start_time']);

        $today = strtotime(date('Y-m-d'));

        /* HANDLE CLUB EXPIRATION METAKEY*/
		$pack_data = manage_subscription_type( $subscription_id );
		/* END HANDLE CLUB EXPIRATION METAKEY*/
        if($subscription_response_json['status'] == 'ACTIVE' && $today > $plan_after_one_month) {

        	$args = array(
					'post_type'      =>  'estate_property',
					'post_status'    =>  array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
					'meta_query' => array(
						array(
						// 'key' => 'club_subscription_id',
						'key' => $pack_data['active_subscription'].'_subscription_id',
						'value' => $subscription_id,
						'compare' => '=',
						)
					)
				);

				$filtered_prop = new WP_Query($args);
				if(sizeof($filtered_prop->posts) > 0) {
					foreach ($filtered_prop->posts as $property) {
						
						update_post_meta( $property->ID, $pack_data['active_subscription'].'_active_subscription', 1 );
						update_post_meta($property->ID, $pack_data['prop_pack_key'], 1);
						update_post_meta($property->ID, $pack_data['exp_key'], $pack_data['exp_value']);
						$stringData = $property->ID . $pack_data['prop_pack_key'].' subscription activated '. date('Y-m-d'). "\n\n";
						fwrite($myfile, $stringData);
					}
				}

				/* Advertise subscription recurring */
				$ad_id = custom_get_ad_data( 'subscription_id', $subscription_id )[0];
				if ( !empty($ad_id) && $ad_id->subscription_id == $subscription_id) {
					$update_ad_data = array(
			            'ad_limit' => strtotime( $subscription_response_json['billing_info']['next_billing_time'] )
			        );
					custom_update_necessary_column_data( $ad_id->id, $update_ad_data);
					$log_data = $ad_id->id . " Next Dt: ". $subscription_response_json['billing_info']['next_billing_time']. ' subscription activated '. date('Y-m-d'). "\n\n";
					fwrite($fpath, $log_data);
					fclose($fpath);
				}
				/* END Advertise subscription recurring */

        } else {
        	$args = array(
				'post_type'      =>  'estate_property',
				'post_status'    =>  array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
				'meta_query' => array(
					array(
					// 'key' => 'club_subscription_id',
					'key' => $pack_data['active_subscription'].'_subscription_id',
					'value' => $subscription_id,
					'compare' => '=',
					)
				)
			);

			$filtered_prop = new WP_Query($args);
			if(sizeof($filtered_prop->posts) > 0) {
				foreach ($filtered_prop->posts as $property) {
					update_post_meta( $property->ID, $pack_data['active_subscription'].'_active_subscription', 0 );
					update_post_meta($property->ID, $pack_data['prop_pack_key'], 0);
					update_post_meta($property->ID, $pack_data['exp_key'], '');
					$stringData = $property->ID . $pack_data['prop_pack_key'].' subscription cancelled/suspended '. date('Y-m-d'). "\n\n";
					fwrite($myfile, $stringData);
				}
			}

			/* Advertise subscription recurring */
			$ad_id = custom_get_ad_data( 'subscription_id', $subscription_id )[0];
			if ( !empty($ad_id) && $ad_id->subscription_id == $subscription_id) {
				$update_ad_data = array(
					'ad_limit'		   => strtotime( date('Y-m-d') ),
		            'subscription_obj' => json_encode($subscription_obj)
		        );
				custom_update_necessary_column_data( $ad_id->id, $update_ad_data);
				$log_data = $ad_id->id . " advertise subscription cancelled/suspended: ". $subscription_id. ' - '. date('Y-m-d'). "\n\n";
				fwrite($fpath, $log_data);
				fclose($fpath);
			}
			/* END Advertise subscription recurring */
        }
	}




	// $req = file_get_contents(get_template_directory().'/req.txt');

	//Decode JSON

	$request_data = json_decode($req,true);



	if(isset($request_data) && isset($request_data['id']) && $request_data['event_type'] != '') {


		//backup goes here?
		//if followup switch case does not work or have another status then handle it with another API call
		//got subscription id here?
		validate_subscription_for_club($request_data['resource']['id']);



		//removed the switch case handler

		// switch ($request_data['event_type']) {
		// 	//commented out subsscription 
		// 	// case 'BILLING.SUBSCRIPTION.SUSPENDED':
		// 	// 	$subscription_id = $request_data['resource']['id'];

		// 	// 	$args = array(
		// 	// 		'post_type'      =>  'estate_property',
		// 	// 		'post_status'    =>  array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
		// 	// 		'meta_query' => array(
		// 	// 			array(
		// 	// 			'key' => 'club_subscription_id',
		// 	// 			'value' => $subscription_id,
		// 	// 			'compare' => '=',
		// 	// 			)
		// 	// 		)
		// 	// 	);
		// 	// 	$filtered_prop = new WP_Query($args);
		// 	// 	if(sizeof($filtered_prop->posts) > 0) {
		// 	// 		foreach ($filtered_prop->posts as $property) {

		// 	// 			update_post_meta( $property->ID, 'club_active_subscription', 0 );

		// 	// 			//empty the subscription id 
		// 	// 			//as we are suspending the subscription and it can not turn back to active
		// 	// 			// update_post_meta( $property->ID, 'club_subscription_id', '' );

		// 	// 			//additional field to update for the package
		// 	// 			update_post_meta($property->ID, 'prop_weblink', 0);
		// 	// 			update_post_meta($property->ID, 'weblink-expires', '');

		// 	// 			// exit;
		// 	// 			$stringData = $property->ID . ' subscription suspended '. date('Y-m-d'). "\n\n";
		// 	// 			fwrite($myfile, $stringData);
		// 	// 		}
		// 	// 	}

		// 	// 	break;
		// 	case 'BILLING.SUBSCRIPTION.ACTIVATED':
		// 		$subscription_id = $request_data['resource']['id'];

		// 		$args = array(
		// 			'post_type'      =>  'estate_property',
		// 			'post_status'    =>  array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
		// 			'meta_query' => array(
		// 				array(
		// 				'key' => 'club_subscription_id',
		// 				'value' => $subscription_id,
		// 				'compare' => '=',
		// 				)
		// 			)
		// 		);

		// 		$filtered_prop = new WP_Query($args);
		// 		if(sizeof($filtered_prop->posts) > 0) {
		// 			foreach ($filtered_prop->posts as $property) {

		// 				update_post_meta( $property->ID, 'club_active_subscription', 1 );

		// 				//empty the subscription id 
		// 				//as we are suspending the subscription and it can not turn back to active
		// 				// update_post_meta( $property->ID, 'club_subscription_id', '' );

		// 				//additional field to update for the package
		// 				update_post_meta($property->ID, 'prop_weblink', 1);
		// 				update_post_meta($property->ID, 'weblink-expires', date('Y-m-d',strtotime("+1 year")));

						

		// 				// exit;
		// 				$stringData = $property->ID . ' subscription activated '. date('Y-m-d'). "\n\n";
		// 				fwrite($myfile, $stringData);
		// 			}
		// 		}
		// 	case 'BILLING.SUBSCRIPTION.RE-ACTIVATED':
		// 		//internally re-activated will again fire the BILLING.SUBSCRIPTION.ACTIVATED
		// 		//but still just in case would be better to have it here
		// 		$subscription_id = $request_data['resource']['id'];

		// 		$args = array(
		// 			'post_type'      =>  'estate_property',
		// 			'post_status'    =>  array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
		// 			'meta_query' => array(
		// 				array(
		// 				'key' => 'club_subscription_id',
		// 				'value' => $subscription_id,
		// 				'compare' => '=',
		// 				)
		// 			)
		// 		);

		// 		$filtered_prop = new WP_Query($args);
		// 		if(sizeof($filtered_prop->posts) > 0) {
		// 			foreach ($filtered_prop->posts as $property) {

		// 				update_post_meta( $property->ID, 'club_active_subscription', 1 );

		// 				//empty the subscription id 
		// 				//as we are suspending the subscription and it can not turn back to active
		// 				// update_post_meta( $property->ID, 'club_subscription_id', '' );

		// 				//additional field to update for the package
		// 				update_post_meta($property->ID, 'prop_weblink', 1);
		// 				update_post_meta($property->ID, 'weblink-expires', date('Y-m-d',strtotime("+1 year")));

		// 				// exit;
		// 				$stringData = $property->ID . ' subscription re-activated '. date('Y-m-d'). "\n\n";
		// 				fwrite($myfile, $stringData);
		// 			}
		// 		}
		// 	case 'BILLING.SUBSCRIPTION.CANCELLED':
		// 		$subscription_id = $request_data['resource']['id'];

		// 		$args = array(
		// 			'post_type'      =>  'estate_property',
		// 			'post_status'    =>  array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
		// 			'meta_query' => array(
		// 				array(
		// 				'key' => 'club_subscription_id',
		// 				'value' => $subscription_id,
		// 				'compare' => '=',
		// 				)
		// 			)
		// 		);
		// 		$filtered_prop = new WP_Query($args);
		// 		if(sizeof($filtered_prop->posts) > 0) {
		// 			foreach ($filtered_prop->posts as $property) {

		// 				update_post_meta( $property->ID, 'club_active_subscription', 0 );

		// 				//empty the subscription id 
		// 				//as we are suspending the subscription and it can not turn back to active
		// 				// update_post_meta( $property->ID, 'club_subscription_id', '' );

		// 				//additional field to update for the package
		// 				update_post_meta($property->ID, 'prop_weblink', 0);
		// 				update_post_meta($property->ID, 'weblink-expires', '');
		// 				update_post_meta($property->ID, 'club_subscription_id', '');
		// 				update_post_meta($property->ID, 'club_plan_id', '');
		// 				update_post_meta($property->ID, 'club_product_id', '');
		// 				// exit;
		// 				$stringData = $property->ID . ' subscription cancelled by customer from other resource'. date('Y-m-d'). "\n\n";
		// 				fwrite($myfile, $stringData);
		// 			}
		// 		}

		// 		break;
		// 	//for future cases
		// 	// add new cases here
		// 	default:
		// 		# code...
		// 		break;
		// }
	}

	// fwrite($myfile, $stringData);
	fclose($myfile);
	
// }
// $home_link=esc_html( home_url() );
// wp_redirect($home_link);
// exit();