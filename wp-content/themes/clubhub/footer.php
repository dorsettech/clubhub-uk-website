</div><!-- end content_wrapper started in header or full_width_row from prop list -->

<?php 
$footer_background          =   get_option('wp_estate_footer_background','');
$repeat_footer_back_status  =   get_option('wp_estate_repeat_footer_back','');
$footer_style               =   '';
$footer_back_class          =   '';

if ($footer_background!=''){
    $footer_style='style=" background-image: url('.$footer_background.') "';
}

if( $repeat_footer_back_status=='repeat' ){
    $footer_back_class = ' footer_back_repeat ';
}else if( $repeat_footer_back_status=='repeat x' ){
    $footer_back_class = ' footer_back_repeat_x ';
}else if( $repeat_footer_back_status=='repeat y' ){
    $footer_back_class = ' footer_back_repeat_y ';
}else if( $repeat_footer_back_status=='no repeat' ){
    $footer_back_class = ' footer_back_repeat_no ';
}
 

if( !is_search() && !is_category() && !is_tax() &&  !is_tag() &&  !is_archive() && wpestate_check_if_admin_page($post->ID) ){
    // do nothing for now  
  
} else if(!is_search() && !is_category() && !is_tax() &&  !is_tag() &&  !is_archive() && basename(get_page_template($post->ID)) == 'property_list_half.php'){
    // do nothing for now   
  
} else if( ( is_category() || is_tax() ) &&  get_option('wp_estate_property_list_type','')==2){
    // do nothing for now
  
} else if(  is_page_template('advanced_search_results.php') &&  get_option('wp_estate_property_list_type_adv','')==2){
    // do nothing for now
 
}else{  
?>


<footer id="colophon" >
	
	<div class="row newsletterTopper">
		<div class="row content_wrapper">
			<div class="col-md-2"><img src="/wp-content/uploads/ch-theo.jpg"/></div>
			<div class="col-md-2"><img src="/wp-content/uploads/BSNA22-Winner-Business.jpg"/></div>
			<div class="col-md-2"><img src="/wp-content/uploads/2022 Finalist Logo BBWA.png"/></div>
			<div class="col-md-2"><img src="/wp-content/uploads/ch-queen.jpg"/></div>
			<div class="col-md-2"><img src="/wp-content/uploads/ch-wow.jpg"/></div>
			<div class="col-md-2"><img src="/wp-content/uploads/ch-guardian.jpg"/></div>
		</div>
	</div>
	
	<div class="row newsletterWrapper">
		<div class="row content_wrapper">
			<div class="col-md-6 join">
				<h3>Join our Newsletter</h3>
				<p>Join thousands of other parents and grandparents who have subscribed to Club Hub Uk’s mailing list.</p>
			</div>
			<div class="col-md-6"><?php echo do_shortcode("[sibwp_form id='1']"); ?></div>
		</div>
	</div>
    <div id="footer-widget-area" class="row">
        <?php  get_sidebar('footer');?>
    </div><!-- #footer-widget-area -->
	<div class="footer_menu">
		<div class="content_wrapper">
			<?php      
            wp_nav_menu( array(
                'theme_location'    => 'footer_menu',
                'depth'             => 1                           
            ));  
            ?>
		</div>
	</div>
    <div class="sub_footer">  
        <div class="sub_footer_content">
            <span class="copyright">&copy; Copyright 2022 CLUB HUB UK, All Rights Reserved. <span class="pipe">|</span> <a href="https://dorset.tech" target="_blank">Web design by Dorset Tech</a>
            </span>

            <div class="subfooter_menu">
                <ul>
					<li><a href="/site-pages/">Site Pages</a></li>
					<span class="pipe">|</span>
					<li><a href="/terms-conditions/">Terms &amp; Conditions</a></li>
					<span class="pipe">|</span>
					<li><a href="/privacy-policy/">Privacy Policy</a></li>
				</ul>
            </div>  
        </div>  
    </div>
</footer><!-- #colophon -->


<?php } // end property_list_half?>
<?php get_template_part('templates/footer_buttons');?>
<?php wp_get_schedules(); ?>
<?php wp_footer();  ?>

<?php
    $ga = esc_html(get_option('wp_estate_google_analytics_code', ''));
    if ($ga != '') {  ?>

        <script>
            //<![CDATA[
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', '<?php echo $ga; ?>', '<?php     echo $_SERVER['SERVER_NAME']; ?>');
          ga('send', 'pageview');
        //]]>
        </script>
<?php
    }
?>


</div> <!-- end class container -->

<?php get_template_part('templates/social_share');?>

</div> <!-- end website wrapper -->
<!--<div class="hidden">-->
<div class="pre_loader_wrap"><div class="pre_loader"></div></div>
</body>
</html>