
jQuery(document).ready(function($){

	$('.bsaProSubmit').on('click', function(){
		// $(this).prop('disabled', true);
		jQuery("body").css({
	        "position": "relative",
	    });
	    jQuery(".pre_loader_wrap").css({
	        "display": "flex",
	    });
	    jQuery("body").addClass("has_loader");
	});

	$('body').on('click', '#bsa-Pro-PayPal-Payment .bsaProImgSubmit', function(e){

		var qry_vars = getUrlVars();

		$.ajax({
			type: 	"POST",
			url: 	custom_ajaxurl.ajaxurl,
			dataType:"json",
			data: 	{
				action: "init_ad_recurring_plan",
				ad_order_id: qry_vars.oid,
			},
			success: function( response ){
				console.log(response.url);
				if ( response.url ) {
					window.location.href = response.url;
				}
			}

		});

	});

	function getUrlVars() {
	    var vars = [], hash;
	    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	    for(var i = 0; i < hashes.length; i++) {
	        hash = hashes[i].split('=');
	        vars.push(hash[0]);
	        vars[hash[0]] = hash[1];
	    }
	    return vars;
	}

});

