<?php
// Template Name: Paypal Processor Recursive
// Wp Estate Pack // custom over -- V

$paypal_status              =   esc_html( get_option('wp_estate_paypal_api','') );
$host                       =   'https://api.sandbox.paypal.com';
if($paypal_status == 'live'){
	$host='https://api.paypal.com';
}

$current_user = wp_get_current_user();   
$clientId                       =   esc_html( get_option('wp_estate_paypal_client_id','') );
$clientSecret                   =   esc_html( get_option('wp_estate_paypal_client_secret','') );
$headers                        =   'From: My Name <myname@example.com>' . "\r\n";
$attachments                    =   '';
$allowed_html   =   array();
if (isset($_GET['token']) ){
	$token     =   sanitize_text_field ( wp_kses ( $_GET['token'],$allowed_html) );

	// get transfer data
	$saved_transaction_data = get_option('paypal_recusrive_transfer_option');

	// if($saved_transaction_data != '') {
		//get perticulat user subscription status
		$rec_trans_data = $saved_transaction_data[$current_user->ID];
		//check subscription status of the club

		if(isset($rec_trans_data['property_id']) && $rec_trans_data['property_id'] != '0') {
			$property_id = $rec_trans_data['property_id'];
			$token = $rec_trans_data['paypal_token'];
			$paypal_subscription_id = $rec_trans_data['paypal_subscription_id'];
			$paypal_plan_id = $rec_trans_data['paypal_plan_id'];
			// if(isset($_GET['subscription_id']) && $_GET['subscription_id'] != '') {

				$get_subscription_id = $_GET['subscription_id'];
				// if($paypal_subscription_id == $get_subscription_id) {

					$url = $host.'/v1/billing/subscriptions/'.$paypal_subscription_id;

					$dump_req = '';
					$subscription_response_json = wpestate_make_post_call($url, $json,$token,0);

					if(isset($subscription_response_json['status']) && $subscription_response_json['status'] == 'ACTIVE') {
						// echo 'YAY success payment';
						
						//update meta fields here

						/* HANDLE CLUB EXPIRATION METAKEY*/
						$pack_data = manage_subscription_type( $paypal_subscription_id );
						/* END HANDLE CLUB EXPIRATION METAKEY*/

						//the core field to check if the recursive subscription is enabled or not
						update_post_meta($property_id, $pack_data['active_subscription'].'_active_subscription', 1);

						

						//additional field to update for the package
						update_post_meta($property_id, $pack_data['prop_pack_key'], 1);
						update_post_meta($property_id, $pack_data['exp_key'], $pack_data['exp_value']);


						//remove that user's data from the paypal_recusrive_transfer_option
						$saved_transaction_data[$current_user->ID ]  =   array();
						update_option ('paypal_recusrive_transfer_option',$saved_transaction_data);

						//done take back user to dashboard //list of club
						$redirect = wpestate_get_dashboard_link();
						wp_redirect($redirect);
						exit();
					}
				// }
			// }
		}

		
	// }

} else {
	//direct hit with no param or wrong param
	$home_link=esc_html( home_url() );
	wp_redirect($home_link);
	exit();
}




// a:1:{i:8216;a:4:{s:12:"paypal_token";s:97:"A21AAE-QIgRrv1eICltVF0eLtktr8aFpUa87u5uTuw5Bb_4wW2qeA7JFw-FTzHPeVOkxDdyVTKCkg4v5jgC-E-y6O6hELRfOA";s:11:"property_id";i:79222;s:14:"paypal_plan_id";s:26:"P-2Y997609DS729303HLWS25OI";s:22:"paypal_subscription_id";s:14:"I-C2YWBL4CPWMM";}}