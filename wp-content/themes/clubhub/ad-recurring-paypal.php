<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

/**
 * Enqueue script
 */
add_action( 'wp_enqueue_scripts', 'clubhub_enq_script' );
function clubhub_enq_script() {
    wp_register_script( 'custom', get_stylesheet_directory_uri().'/js/custom.js?aaa='.time(), false );
    wp_enqueue_script( 'custom' );
    wp_localize_script( 'custom', 'custom_ajaxurl',
        array( 
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
        )
    );
}

/**
 * intial ajax request when submit via paypal
 * create product, plan and create subscription
 */
// add_action('wp_ajax_init_ad_recurring_plan', 'init_ad_recurring_plan' );
// add_action('wp_ajax_nopriv_init_ad_recurring_plan', 'init_ad_recurring_plan' );
function init_ad_recurring_plan( $advert_id ) {

    global $wpdb;
    
	$ad_order_id  = (int)$advert_id;
    $ad_limit     = bsa_ad($ad_order_id, 'ad_limit');
    $day_interval = $ad_limit - time();
    $day_interval = number_format($day_interval / 24 / 60 / 60);
    $interval_count = $day_interval;

    $ad_price                 = (float) bsa_ad($ad_order_id, 'cost');
    $current_user             = wp_get_current_user();
    $is_existing              = custom_get_ad_data( 'id', $ad_order_id );
    $existing_subscription_id = !empty( $is_existing->subscription_id ) ? $is_existing->subscription_id : "";
    $existing_ad_prod_id      = !empty( $is_existing->subscription_product_id ) ? $is_existing->subscription_product_id : "";
    $existing_ad_plan_id      = !empty( $is_existing->paypal_plan_id ) ? $is_existing->paypal_plan_id : "";

    // cust_printr($existing_subscription_id);
	
    $curency_code             = esc_html(get_option('bsa_pro_plugin_currency_code', ''));
    $bsaTrans                 = 'bsa_pro_plugin_trans_';
    $host                     = 'https://api.paypal.com';
	if ( strtolower(get_option($bsaTrans."payment_paypal_title")) == 'test' ) {
    	$host = 'https://api.sandbox.paypal.com';
	}

	/* generate token from paypal */
	$url 	  = $host . '/v1/oauth2/token';
	$postArgs = 'grant_type=client_credentials';
	$token 	  = wpestate_get_access_token($url, $postArgs);
	
    if ( empty($existing_subscription_id) ) {

		$paypal_product_id = clubhub_validate_create_recurring_ad_product( $ad_order_id, $token, $host, $existing_ad_prod_id );
		// echo 'pp product: '.$paypal_product_id."<br>\n";

		$paypal_plan_id = clubhub_validate_ad_plan_product($ad_order_id, $token, $host, $paypal_product_id, $interval_count, $existing_ad_plan_id );
	    // echo 'plan id :::'. $paypal_plan_id."<br>\n";
	
	    $paypal_subscription_data = clubhub_create_ad_subscription($ad_order_id, $token, $host, $paypal_plan_id, $current_user, $existing_subscription_id);

	    // echo "pp_subs_data :::<br>\n";
	    // print_r($paypal_subscription_data);

	    if ($paypal_subscription_data['subscription_id'] != '' && $paypal_subscription_data['is_new'] == 1) {
	        $subscription_response_json = $paypal_subscription_data['subscription_obj'];
	        foreach ($subscription_response_json['links'] as $link) {
	            if ($link['rel'] == 'approve') {
	                $init_subscription_process = $link['href'];
	            }
	        }

	        //logic inherited
	        $executor['paypal_token'] 			= $token;
	        $executor['ad_order_id'] 			= $ad_order_id;
	        $executor['paypal_plan_id'] 		= $paypal_plan_id;
	        $executor['paypal_subscription_id'] = $subscription_response_json['id'];
	        $save_data[$current_user->ID] 		= $executor;

	        update_option('paypal_recusrive_transfer_option', $save_data);

            /* update in database in this table wp_bsa_pro_ads */
            $update_arr = array(
                'paypal_plan_id'            => $paypal_plan_id,
                'subscription_product_id'   => $paypal_product_id,
                'subscription_id'           => $subscription_response_json['id'],
                'subscription_obj'          => json_encode( $paypal_subscription_data )
            );
            // cust_printr($paypal_subscription_data);
            // exit;
            custom_update_necessary_column_data( $ad_order_id, $update_arr );

            /* keep track as log just in case  */
            $fpath     = get_template_directory().'/ad_recurring_log.txt';
            $fpath     = fopen($fpath, "a") or die("Unable to open file!");

            $log_data = $ad_order_id . " subs_id: ". $update_arr['subscription_id']. ' new subscription created '. date('Y-m-d'). "\n\n";
            fwrite($fpath, $log_data);
            /* end keep track as log just in case  */


            $init_data = array( 'url' => $init_subscription_process );
            header("Location: ".$init_subscription_process);
	        echo json_encode($init_data);
	        die();
	    }

	}

}

function clubhub_validate_create_recurring_ad_product($ad_order_id, $token, $host, $existing_ad_prod_id = '') {

    if ($existing_ad_prod_id == '') {

        //new product means first time integgration
        $url = $host . '/v1/catalogs/products';
        $product = array(
            "name" => "Advertising Title" . '_' . $ad_order_id,
            "type" => "SERVICE",
            "category" => "SERVICES"
        );
        $product_json = json_encode($product);

        $json_prod_resp = wpestate_make_post_call($url, $product_json, $token);

        if (isset($json_prod_resp['id'])) {
            //i.e. plan is created
            //storing paypal product id just in case
            $update_arr = array( 'subscription_product_id' => $json_prod_resp['id'] );
            custom_update_necessary_column_data( $ad_order_id, $update_arr );
            return $json_prod_resp['id'];
        }
    } else {
        return $existing_ad_prod_id;
    }

}


function clubhub_validate_ad_plan_product($ad_order_id, $token, $host, $paypal_product_id, $interval_count = 1, $existing_ad_plan_id) {

    if ($existing_ad_plan_id == '') {
        $ad_price        = (float) bsa_ad($ad_order_id, 'cost');
        $package_pricing = $ad_price;
        $currency_code   = esc_html(get_option('wp_estate_submission_curency', ''));
        //this product doesn't have any plan associated
        
        $url            = $host . '/v1/billing/plans';
        $dash_link      = wpestate_get_dashboard_link();
        $processor_link = wpestate_get_procesor_link();

        $product_plan   = array(
            'product_id'    => $paypal_product_id,
            'name'          => "Advertising plan for ".$ad_order_id,
            'status'        => 'ACTIVE',
            'billing_cycles'=> array(
                array(
                    'frequency' => array(
                        'interval_unit'     => 'DAY',
                        'interval_count'    => $interval_count,
                    ),
                    "tenure_type"    => "REGULAR",
                    "sequence"       => 1,
                    "total_cycles"   => 0,
                    'pricing_scheme' => array(
                        'fixed_price'       => array(
                            "value"         => $package_pricing,
                            "currency_code" => $currency_code,
                        ),
                    ),
                ),
            ),
            'payment_preferences' => array(
                'auto_bill_outstanding'     => 'true',
                'setup_fee'                 => array(
                    'value'                 => 0,
                    'currency_code'         => $currency_code,
                ),
                "setup_fee_failure_action" => "CONTINUE",
                "payment_failure_threshold" => 2
            ),
        );

        $product_plan_json = json_encode($product_plan);

        $json_plan_resp = wpestate_make_post_call($url, $product_plan_json, $token);
        // print_r($json_plan_resp);

        if (isset($json_plan_resp['id'])) {
            //i.e. plan is created
            //storing paypal product id just in case
            $update_arr = array( 'paypal_plan_id' => $json_plan_resp['id'] );
            custom_update_necessary_column_data( $ad_order_id, $update_arr );
            return $json_plan_resp['id'];
        }
    } else {
        $status = clubhub_change_ad_plan_status($host, $token, $existing_ad_plan_id, $ad_order_id, 'activate', 'ACTIVE');
        return $existing_ad_plan_id;
    }
}


function clubhub_create_ad_subscription($ad_order_id, $token, $host, $plan_id, $current_user, $existing_subscription_id) {
    
    if ($existing_subscription_id == '') {
        $url = $host . '/v1/billing/subscriptions';
        $dash_link = wpestate_get_dashboard_link();
        $recursive_processor_link = wpestate_get_recursive_procesor_link();

        $return_url = site_url('/')."place-an-advert/?oid=".$ad_order_id."&bsa_pro_payment=notify&bsa_pro_notice=success";
        $cancel_url = site_url('/')."place-an-advert/?oid=".$ad_order_id."&bsa_pro_notice=failed";

        $subscription = array(
            "plan_id" => $plan_id,
            "quantity" => "1",
            // "start_time"=> $is_delay_dt,
            "subscriber" => array(
                "name" => array(
                    "given_name" => $current_user->user_login
                ),
                "email_address" => $current_user->user_email,
            ),
            "application_context" => array(
                "locale" => "en-US",
                "shipping_preference" => "SET_PROVIDED_ADDRESS",
                "user_action" => "SUBSCRIBE_NOW",
                "payment_method" => array(
                    "payer_selected" => "PAYPAL",
                    "payee_preferred" => "IMMEDIATE_PAYMENT_REQUIRED"
                ),
                //localhost url will give error here
                // "return_url" => $recursive_processor_link,
                // "cancel_url" => $dash_link,
                "return_url" => $return_url,
                "cancel_url" => $cancel_url,
            )
        );

        // if delay start subscription
        $is_delay_dt = is_delay_ad_start( $ad_order_id );
        if ( !empty($is_delay_dt) ) {
            $future_start = array( 'start_time' => $is_delay_dt);
            $subscription = array_merge( $future_start, $subscription);
        }

        $json = json_encode($subscription);
        // echo 'JSON: '.$json ."to hit";
        // echo 'URL: '.$url ."to hit";

        $json_resp = wpestate_make_post_call($url, $json, $token);
        if (isset($json_resp['id'])) {
            if ($json_resp['id'] != '') {
                update_option('ad_subscription_id_'.$ad_order_id, $json_resp['id']);
                
            }
            return array(
                'subscription_id' => $json_resp['id'],
                'is_new' => 1,
                'subscription_obj' => $json_resp
            );
        }
    } else {
        //renew scenario here
        $url = $host . '/v1/billing/subscriptions/' . $existing_subscription_id . '/activate';

        $json = '';
        $subscription_response = wpestate_make_post_call($url, $json, $token, 1, 1);
        if ($subscription_response == 204) {
            

            //additional field to update for the package
            update_option('club_active_subscription_'.$ad_order_id, 1);
            update_option('ad_weblink_'.$ad_order_id, 1);
            update_option('ad-weblink-expires_'.$ad_order_id, date('Y-m-d', strtotime("+1 year")));

            return array(
                'subscription_id' => $existing_subscription_id,
                'is_new' => 0,
                'subscription_obj' => ''
            );
        }
    }
}

function clubhub_change_ad_plan_status($host, $token, $plan_id, $property_id, $set_status, $check_status) {

    $url = $host . '/v1/billing/plans/' . $plan_id;

    // $url = $host.'/v1/billing/subscriptions/'.$paypal_subscription_id.'/cancel';
    $json = '';
    //this will return nothing if success
    $plan_data = wpestate_make_post_call($url, $json, $token, 0);
    // print_r($plan_data);

    if (isset($plan_data['status']) && $plan_data['status'] != $check_status) {
        $url = $host . '/v1/billing/plans/' . $plan_id . '/' . $set_status;
        // echo $url;
        $plan_status_response = wpestate_make_post_call($url, $json, $token, 1, 1);
        if ($plan_status_response == 204) {
            return $plan_id;
        } else {
            return 0;
        }
    } else {
        //in same state as requested
        return $plan_id;
    }
    // print_r($plan_data);
    // exit;
}


function custom_get_ad_data( $column_name, $column_value ) {

    global $wpdb;
    $ad_data = $wpdb->get_results($wpdb->prepare("SELECT * FROM ".$wpdb->prefix."bsa_pro_ads WHERE $column_name = %s", $column_value));
    return $ad_data;

}

function custom_update_necessary_column_data( $ad_id, $update_necessary_column_arr ) {
    global $wpdb;
    /* update in database in this table wp_bsa_pro_ads */
    $wpdb->update( $wpdb->prefix.'bsa_pro_ads',
        
        $update_necessary_column_arr,
        
        array( 'id' => $ad_id )
    );
}


/* start delay advertisement */
function is_delay_ad_start( $ad_id ) {
    global $wpdb;
    $is_delay_date = '';
    $is_delay_dt = $wpdb->get_var($wpdb->prepare("SELECT start_time FROM ".$wpdb->prefix."bsa_pro_cron WHERE item_id = %s", $ad_id));
    if ( !empty($is_delay_dt) ) {
        $start_date  = date( 'Y-m-d\TH:m:i\Z', $is_delay_dt );
        $is_delay_date = $start_date;
    }

    return $is_delay_date;
}

function cust_printr( $data ) {
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}