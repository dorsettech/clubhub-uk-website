<?php
// Template Name: User Dashboard Subscriptions
// Wp Estate Pack
if ( !is_user_logged_in() ) {   
    wp_redirect(  esc_html( home_url() ) );exit();
} 
if ( !wpestate_check_user_level()){
   wp_redirect(  esc_html( home_url() ) );exit(); 
}

global $current_user;
$current_user = wp_get_current_user();    
$paid_submission_status         =   esc_html ( get_option('wp_estate_paid_submission','') );
$price_submission               =   floatval( get_option('wp_estate_price_submission','') );
$submission_curency_status      =   esc_html( get_option('wp_estate_submission_curency','') );
$userID                         =   $current_user->ID;
$user_option                    =   'favorites'.$userID;
$curent_fav                     =   get_option($user_option);
$show_remove_fav                =   1;   
$show_compare                   =   1;
$show_compare_only              =   'no';
$currency                       =   esc_html( get_option('wp_estate_currency_symbol', '') );
$where_currency                 =   esc_html( get_option('wp_estate_where_currency_symbol', '') );
get_header();
$options                        =   wpestate_page_details($post->ID);
?> 

<div class="row is_dashboard">
    <?php
    if( wpestate_check_if_admin_page($post->ID) ){
        if ( is_user_logged_in() ) {   
            get_template_part('templates/user_menu'); 
        }  
    }
    ?> 
    
    <div class=" dashboard-margin">
        <div class="dashboard-header">
            <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
                <h1 class="entry-title entry-title-profile"><?php the_title(); ?></h1>
            <?php } ?>
        </div>
        
        <div class="row">    
        
        <div class="content-admin-dashboard">
            <?php the_content('Continue Reading'); ?>
        </div>    
            
        <?php
        $paid_submission_status = esc_html ( get_option('wp_estate_paid_submission','') );  
        if ($paid_submission_status == 'membership'){ 
            $userID =   $current_user->ID;
            $unlimited_lists    = get_the_author_meta('package_listings', $userID);
            $date               = strtotime ( get_user_meta($userID, 'package_activation',true) );
            $year=60*60*24*365;
            $expired = $date+$year;
            $today = strtotime(date("Y-m-d H:i:s"));
        ?>
        
        <div class="">
            <div class="user_dashboard_panel">
            <?php if($unlimited_lists >= 0 || $expired < $today ) { ?>
            <h4 class="user_dashboard_panel_title"><?php esc_html_e('Upgrade your Package','wpestate');?></h4>
                <div class="col-md-8">
                    <?php wpestate_display_packages(); ?>
                    <input checked="true" type="checkbox" name="pack_recuring" id="pack_recuring" value="1" /> 
                    <label for="pack_recurring"><?php esc_html_e('make payment recurring ','wpestate');?></label><br>
                    <label>Cancel at anytime via your subscriptions page on your PayPal account</label>
                </div>
                <?php
                    $enable_paypal_status= esc_html ( get_option('wp_estate_enable_paypal','') );
                    $enable_stripe_status= esc_html ( get_option('wp_estate_enable_stripe','') );
                    
                    print '<div class="col-md-4">';
                    if($enable_paypal_status==='yes'){
                        print '<div id="pick_pack">'.esc_html__( 'pay','wpestate').'</div>';
                    }
                    if($enable_stripe_status==='yes'){
                        wpestate_show_stripe_form_membership();
                    }
                    print '</div>';
               
                ?>
                
            </div>
            <div class="ch_paypal_loading" style="display: none;">
                <img src="<?php echo get_template_directory_uri() . '/img/loader.gif'?>" style="margin: auto;display: block;height: 75px;width: 200px;object-fit: cover;margin-bottom: 45px;">
            </div>
            <?php
            $currency  =   esc_html( get_option('wp_estate_submission_curency', '') );
        $args = array(
            'post_type'         => 'membership_package',
            'posts_per_page'    => -1,
            'meta_query'        =>  array(
                                        array(
                                        'key' => 'pack_visible',
                                        'value' => 'yes',
                                        'compare' => '=',
                                    )
                                )
        );
        
        $pack_selection = new WP_Query($args);
           
            print '<div class="pack-wrapper">';
                while($pack_selection->have_posts() ){
                    $pack_selection->the_post();
                    get_template_part('templates/dashboard_pack_unit'); 
                }
                
            print '</div>';
            } else { ?>
                <div class="max-upgrade">
                    <h2>You Have the Best Available Package</h2>
                </div>
            <?php }
        } ?>
            
        </div>
            
        <?php
        //featured news
//        print '<div class="col-md-4 newspostfeaturebutton"> 
//                            <div class="user_dashboard_panel pack_unit_list">
//                            <a href="/featured-news-item/"><h4 class="user_dashboard_panel_title">Featured News Item</h4><p>for a week</p></a>    
//                            <a href="/featured-news-item/"><p id="packPrice">£10</p>
//                            <div class="pack-listing-period"><img class="alignnone wp-image-1934 size-medium" src="/wp-content/uploads/packages-news-300x182.png" width="300" height="182"></div>
//                            <div class="pack-listing-period">
//                            News Item Featured on the Site for 1 week and shared on social media 
//                        </div></a>
//                        <div class="ult_price_link" style="background:#de81b8;color:#000000;">
//                        <a href="/featured-news-item/" target="" class="ult_price_action_button ult-responsive" style="background:#f5bbd1;color:#000000; font-weight:normal;">Book Now</a>
//                    </div> 
//                            </div>
//                    </div>';
                

                //end featured news

        
                
        ?>    
</div>
        <script>
        jQuery(document).ready(function($){
            $('#pack_select').change(function(){
                let GetPackVal = $(this).val();
                if(GetPackVal && GetPackVal == '137320'){
                    $('#pack_recuring').prop("checked",true);
                    // console.log(GetPackVal);
                }else{
                    $('#pack_recuring').prop("checked",true);
                }
                
            })
        });
        </script>
<?php 
wp_reset_query();
get_footer(); ?>