<?php 

global $post;
$adv_search_what            =   get_option('wp_estate_adv_search_what','');
$show_adv_search_visible    =   get_option('wp_estate_show_adv_search_visible','');
$close_class                =   '';
$allowed_html = array();

if($show_adv_search_visible=='no'){
    $close_class='adv-search-1-close';
}

if(isset( $post->ID)){
    $post_id = $post->ID;
}else{
    $post_id = '';
}

$extended_search    =   get_option('wp_estate_show_adv_search_extended','');
$extended_class     =   '';

if ( $extended_search =='yes' ){
    $extended_class='adv_extended_class';
    if($show_adv_search_visible=='no'){
        $close_class='adv-search-1-close-extended';
    }      
}
$header_type                =  '';
if(isset($post->ID)){    
    $header_type                =   get_post_meta ( $post->ID, 'header_type', true);
}
$global_header_type         =   get_option('wp_estate_header_type','');

$google_map_lower_class='';
 if (!$header_type==0){  // is not global settings
    if ($header_type==5){ 
        $google_map_lower_class='adv_lower_class';
    }
}else{    // we don't have particular settings - applt global header          
    if($global_header_type==4){
        $google_map_lower_class='adv_lower_class';
    }
} // end if header
    

    
    
?>

 <div class="adv-2-header">
     Find Your Kids Activity
</div>  

<div class="adv-2-wrapper"> 
</div>  


<div class="adv-search-2 <?php echo $google_map_lower_class.' '.$close_class.' '.$extended_class;?>" id="adv-search-1" data-postid="<?php echo $post_id; ?>"> 

       
    <form  method="get"  id="main_search" action="<?php print $adv_submit; ?>" >
        <?php
        if (function_exists('icl_translate') ){
            print do_action( 'wpml_add_language_form_field' );
        }
        ?>
        <div class="col-md-12 map_icon">       
            <?php 
			$selArgs = wpestate_get_select_arguments();
			//$selArgs['selected'] = 'online-classes-and-activity-boxes';
			
            $show_adv_search_general            =   get_option('wp_estate_wpestate_autocomplete','');
            $wpestate_internal_search           =   '';
            if($show_adv_search_general=='no'){
                $wpestate_internal_search='_autointernal';
                print '<input type="hidden" id="stype" name="stype" value="tax">';
            }
            ?>
            <input type="text"    id="search_location<?php echo $wpestate_internal_search;?>"      class="form-control" name="search_location" placeholder="<?php esc_html_e('Location / Post Code','wpestate');?>" value="" >              
            <input type="hidden" id="advanced_city"      class="form-control" name="advanced_city" data-value=""   value="<?php if(isset( $_GET['advanced_city'] )){echo wp_kses( esc_attr($_GET['advanced_city']),$allowed_html);}?>" >              
            <input type="hidden" id="advanced_area"      class="form-control" name="advanced_area"   data-value="" value="<?php if(isset( $_GET['advanced_area'] )){echo wp_kses ( esc_attr($_GET['advanced_area']),$allowed_html);}?>" >      
            <input type="hidden" id="advanced_country"   class="form-control" name="advanced_country"   data-value="" value="<?php if(isset( $_GET['advanced_country'] )){echo wp_kses ( esc_attr($_GET['advanced_country']),$allowed_html);}?>" >              
            <input type="hidden" id="property_admin_area" name="property_admin_area" value="">
            <input id="advanced_lat" name="advanced_lat" class="form-control" type="hidden" value="">
            <input id="advanced_lng" name="advanced_lng" class="form-control" type="hidden" value="">
        </div>
        
        <div class="col-md-12  dropdown has-child-icon has_calendar calendar_icon">
            <!--<input type="text" id="search_age" class="form-control search_age" name="search_age" placeholder="Child's Age" value="" autocomplete="off">-->
			<div class=" form-control search_age">
                <div data-toggle="dropdown" id="search_age" class="filter_menu_trigger" data-value=""> <span class="selected-agerange" style="padding-left: 30px;">Child's Age eg: 3 and 5</span> <span class="caret caret_filter"></span> </div>
				<span class="child_interest_close">X</span>
                <input required type="hidden" name="search_age" id="search_age" value="">
                <ul class="dropdown-menu filter_menu age-range-dropdown" role="menu" aria-labelledby="search_age"> <?php echo wpestate_get_agerange_select_list(); ?>
                </ul>
            </div>
        </div>
        
        <div class="col-md-12  dropdown has-child-icon">
            <div class=" form-control child_interest">
                <div data-toggle="dropdown" id="search_child_age" class="filter_menu_trigger" data-value="all"> <span class="selected-cat">All Types</span> <span class="caret caret_filter"></span> </div><span class="child_interest_close">X</span>
                <input type="hidden" name="category" id="category" value="all">
                <ul class="dropdown-menu filter_menu cat-dropdown" role="menu" aria-labelledby="category"> <?php echo wpestate_get_category_select_list($selArgs); ?>
                </ul>
            </div> 
        </div>
        
                
        <div class="col-md-12">
        <input name="submit" type="submit" class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="advanced_submit_2" value="<?php esc_html_e('Search','wpestate');?>">
        </div>
              
        <div id="results">
            <?php esc_html_e('We found ','wpestate')?> <span id="results_no">0</span> <?php esc_html_e('results.','wpestate'); ?>  
            <span id="showinpage"> <?php esc_html_e('Show the results now ?','wpestate');?> </span>
        </div>
        <script>
            jQuery(function($) {
                $('.age-range-dropdown, .cat-dropdown').click( function () {
                    $('.child_interest_close').click();
                    $(this).unbind('click');
                })
            });
        </script>
    </form>  

</div> 
<?php get_template_part('libs/internal_autocomplete_wpestate'); ?>