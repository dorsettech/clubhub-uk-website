<?php
global $feature_list_array;
global $edit_id;
global $moving_array;
global $edit_link_calendar;
?>
<div class="club_benefits_main">
    <div class="user_dashboard_panel1">
        <h4 class="user_dashboard_panel_title"><?php esc_html_e('Activity Benefits','wpestate');?></h4>
        <div class="col-md-12" id="profile_message_amenity"></div>                
        <?php
        foreach($feature_list_array as $key => $value){
            $post_var_name =   str_replace(' ','_', trim($value) );
            $post_var_name =   wpestate_limit45(sanitize_title( $post_var_name ));
            $post_var_name =   sanitize_key($post_var_name);

            $value_label=$value;
            if (function_exists('icl_translate') ){
                $value_label    =   icl_translate('wpestate','wp_estate_property_custom_amm_'.$value, $value ) ;                                      
            }

            print ' <div class="col-md-3"><p>
                   <input type="hidden"    name="'.$post_var_name.'" value="" style="display:block;">
                   <input type="checkbox"   id="'.$post_var_name.'" name="'.$post_var_name.'" value="1" ';

            if (esc_html(get_post_meta($edit_id, $post_var_name, true)) == 1) {
                print' checked="checked" ';
            }else{
                if(is_array($moving_array) ){                      
                    if( in_array($post_var_name,$moving_array) ){
                        print' checked="checked" ';
                    }
                }
            }
            print' /><label for="'.$post_var_name.'">'.$value_label.'</label></p></div>';  
        }
        ?>
       <!-- Membership Form  -->
    <?php if (!is_user_logged_in()) :?>
       <div class="col-md-12 membershipPackPlan">
            <h4 class="user_dashboard_panel_title"><?php esc_html_e('Select Package','wpestate');?></h4>
            <?php wpestate_display_packages(); ?>
            <input checked type="checkbox" name="pack_recuring" id="pack_recuring" value="1" /> 
            <label for="pack_recuring"><?php esc_html_e('make payment recurring ','wpestate');?></label>
        </div>
        <?php endif; ?>
       <!-- END Membership Form  -->
        <div class="col-md-12 terms">
            <p>
                <label>Terms and Conditions</label>
            </p>
            <p><input id="over18" type="checkbox" name="over18" <?php if(!empty($over18)) { echo 'checked';} ?> value="1"/>* I can confirm that I am at least 18 years of age.</p>
            <p><input id="dbscheck" type="checkbox" name="dbscheck" <?php if(!empty($dbscheck)){ echo 'checked';} ?> value="1"/>* Workers are covered by a valid DBS check (if applicable).</p>
            <p><input id="plinsurance" type="checkbox" name="plinsurance" <?php if(!empty($plinsurance)){ echo 'checked';} ?> value="1"/>* Activities are covered by valid liability cover.</p>
            <p><input id="tandcs" type="checkbox" name="tandcs" <?php if(!empty($tandcs)){ echo 'checked';} ?> value="1"/>* I accept the Club Hub <a href="/terms-conditions/">Terms and Conditions</a></p>
        </div>
        <div class="col-md-12" style="display: inline-block;">  
			<div class="col-md-12" id="profile_message-bottom"></div>
            <input type="hidden" name="listing_edit" id="listing_edit" value="<?php echo $edit_id;?>">
            <button type="button" class="g-recaptcha wpb_btn-info wpb_btn-small wpestate_vc_button vc_button" id="recaptcha_club_information" data-sitekey='6LcmXiwaAAAAADSGRVwsFlDt3lgtoGOxnIxV5kIy' data-callback='onSubmit' data-action='submit'><?php esc_html_e('Save', 'wpestate') ?></button>
            <input type="button" class="hidden wpb_btn-info wpb_btn-small wpestate_vc_button vc_button" id="save_club_information" value="<?php esc_html_e('Save', 'wpestate') ?>"/>
            <?php
                    $tday = date("Y-m-d");
                    $calraw = ( !empty($post_id) ) ? get_post_meta($post_id,'calendar-expires',true) : '';
                    if ( !empty($calraw) ) {
                        if($tday < $calraw){ /* ?>
                            <a href="<?php print $edit_link_calendar;?>"    class="<?php print $activecalendar;?>">    <?php esc_html_e('Calendar','wpestate');?></a>     
                        <?php */
                        ?>        <a id="calelink" href="<?php echo  $edit_link_calendar;?>" class="next_submit_page"><?php esc_html_e('Go to Calendar settings (*make sure you click save first).','wpestate');?></a>
                         <?php } 
                    } ?>
                   
        </div>
        <script>
        onSubmit = function (token) {
            jQuery(function($) {
                console.log(token);
                $("#save_club_information").click();
            });
        }
        </script>
        <script>
            jQuery(function($) {
                if(!$('#over18:checked').length || !$('#dbscheck:checked').length || !$('#plinsurance:checked').length || !$('#tandcs:checked').length){
                    $('#save_club_information, #recaptcha_club_information').prop('disabled',true);
                }

                $('#over18,#dbscheck,#plinsurance,#tandcs').change(function(){
                    if(!$('#over18:checked').length || !$('#dbscheck:checked').length || !$('#plinsurance:checked').length || !$('#tandcs:checked').length){
                        $('#save_club_information, #recaptcha_club_information').prop('disabled',true);
                    } else {
                        $('#save_club_information, #recaptcha_club_information').prop('disabled',false);
                    }
                });
            });
        </script>
        <style type="text/css">
            #save_club_information[disabled], #recaptcha_club_information[disabled] {
                background: grey !important;
                color: white !important;
            }
            .membershipPackPlan {
                margin-top: 2em;
                margin-bottom: 1em;
            }
            .membershipPackPlan .user_dashboard_panel_title {
                padding-left: 0;
            }
            .membershipPackPlan select#pack_select {
                width: 30%;
                padding: 15px 15px;
                color: #747c83;
                border: 1px solid #f0f0f0;
                font-size: 15px;
            }
            .membershipPackPlan input#pack_recuring {
                vertical-align: middle;
                margin-left: 1em;
            }
        </style>
    </div>
    </div>
    </form>
</div>
</div>