<div class="col-md-12">
    <div class="user_dashboard_panel1">
    <h4 class="user_dashboard_panel_title">Listing Details</h4>

    <div class="col-md-12" id="profile_message_detail"></div>
    <div class="col-md-4"><p>
        <label for="minimum_age">Minimum Age (age for months must be added with a decimal point. For Example 6 months = 0.6)</label>
            <input required type="text" class="form-control" placeholder="4" id="minimum_age" size="40" name="minimum_age" value="">
    </p></div>
    <div class="col-md-4"><p>
        <label for="maximum_age">Maximum Age</label>
        <input required type="text" class="form-control" placeholder="16" id="maximum_age" size="40" name="maximum_age" value="">
    </p></div>
    <div class="col-md-4"><p>
        <label for="dayheld">Day(s)</label>
        <input required type="text" class="form-control" placeholder="Mondays and Fridays Term-time" id="dayheld" size="40" name="dayheld" value="">
    </p></div>
    <div class="col-md-4"><p>
        <label for="timheld">Time(s)</label>
        <input required type="text" class="form-control" placeholder="4pm Mondays, 6pm Fridays" id="timheld" size="40" name="timheld" value="">
    </p></div>
    <div class="col-md-4"><p>
        <label for="clubphone">Phone Number</label>
        <input required type="text" class="form-control" placeholder="01234 567890" id="clubphone" size="40" name="clubphone" value="">
    </p></div>
    <div class="col-md-12" style="display: inline-block;">  
    </div>
</div>  
 
<div id="new_post2">
    <div class="user_dashboard_panel1">
    <h4 class="user_dashboard_panel_title">Listing Media</h4>
<!--    <div class="col-md-12" id="profile_message"></div>    -->
    <div class="col-md-12">
        <div id="upload-container">                 
            <div id="aaiu-upload-container" style="position: relative;">                 
                <div id="aaiu-upload-imagelist">
                    <ul id="aaiu-ul-list" class="aaiu-upload-list"></ul>
                </div>

                <div id="imagelist">
                  
                </div>

                <div id="aaiu-uploader" class=" wpb_btn-small wpestate_vc_button  vc_button" style="position: relative; z-index: 1;">Click Here to Select Image *</div>
                <input type="hidden" name="attachid" id="attachid" value="">
                <input type="hidden" name="attachthumb" id="attachthumb" value="">
                <p class="full_form full_form_image"><strong>2MB Filesize limit per image.</strong></p>
                <p class="full_form full_form_image">*Click on the image to select featured. </p>
            <div id="html5_1bpg3ef96c9o157l57eqgh4jk3_container" class="moxie-shim moxie-shim-html5" style="position: absolute; top: 5px; left: 0px; width: 200px; height: 44px; overflow: hidden; z-index: 0;"><input id="html5_1bpg3ef96c9o157l57eqgh4jk3" type="file" style="font-size: 999px; opacity: 0; position: absolute; top: 0px; left: 0px; width: 100%; height: 100%;" multiple="" accept="image/jpeg,.jpeg,.jpg,image/gif,.gif,image/png,.png,application/pdf,.pdf"></div></div>  
        </div>
    </div>

    <div class="col-md-4">
        <p>
            <label for="embed_video_type">Video from (optional)</label>
            <select id="embed_video_type" name="embed_video_type" class="select-submit2">
                <option value="vimeo">vimeo</option><option value="youtube">youtube</option>            </select>
        </p>
    </div>
    
    
    <div class="col-md-4">
        <p>     
           <label for="embed_video_id">Video id: </label>
           <input type="text" id="embed_video_id" class="form-control" name="embed_video_id" size="40" value="">
        </p>
    </div>
    <div class="col-sm-12">
        <p class="full_form full_form_image">For example: For https://www.youtube.com/embed/4lUn6GZdFCc the Video ID is 4lUn6GZdFCc</p>
    </div>
</div>
</div>   
</div>