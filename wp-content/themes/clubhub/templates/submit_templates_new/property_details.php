<?php
global $unit;
global $edit_id;
global $property_size;
global $property_lot_size;
global $property_rooms;
global $property_bedrooms;
global $property_bathrooms;
global $custom_fields;    
global $custom_fields_array;
global $edit_link_location;

$measure_sys = esc_html ( get_option('wp_estate_measure_sys','') ); 
?> 

<div class="col-md-12">
    <div class="user_dashboard_panel1">
    <h4 class="user_dashboard_panel_title"><?php esc_html_e('Listing Details','wpestate');?></h4>
    <div class="col-md-12" id="profile_message_detail"></div>
    <?php 
	
    $i=0;
	$count = 0;    
    if ( !empty($custom_fields) ) {
        while($i < 4 ) {
            $placeholder = '""';
            if($i == 0){
                $placeholder = '"4" required';
            } else if ( $i == 1) {
                $placeholder = '"16" required';
            } else if ( $i == 2 ) {
                $placeholder = '"Mondays and Fridays Term-time" required';
            } else if ( $i == 3 ) {
                $placeholder = '"4pm Mondays, 6pm Fridays" required';
            }
            
            $name  =   $custom_fields[$i][0];
            $label =   $custom_fields[$i][1];
            $type  =   'long text'; //$custom_fields[$i][2];
            $slug  =   str_replace(' ','_',$name);

            $slug         =   wpestate_limit45(sanitize_title( $name ));
            $slug         =   sanitize_key($slug);

            $i++;
			
            if (function_exists('icl_translate') ) {
                $label     =   icl_translate('wpestate','wp_estate_property_custom_front_'.$label, $label ) ;
            } 

            print '<div class="col-md-4"><p><label for="'.$slug.'">'.$label.'</label>';

            if ($type=='long text')
			{
                 print '<textarea type="text" class="form-control input-textarea" placeholder='.$placeholder.'  id="'.$slug.'"  size="0" name="'.$slug.'" rows="3" cols="42">'.$custom_fields_array[$slug].'</textarea>';
            }
			else
			{
                 print '<input type="text" class="form-control" placeholder="'.$placeholder.'" id="'.$slug.'" size="40" name="'.$slug.'" value="'.$custom_fields_array[$slug].'">';
            }
            print '</p>  </div>';
			$count++;
			if($count == 3)
			{
				echo '<div class="clearfix"></div>';
				$count =0;
			}

            if ($type=='date'){
                print '<script type="text/javascript">
                    //<![CDATA[
                    jQuery(document).ready(function(){
                        '.wpestate_date_picker_translation($slug).'
                    });
                    //]]>
                    </script>';
            }
        }
		// adding custom field here for days monday to sunday checkboxes by gd
		if($edit_id > 0) {
			$clubdays = get_post_meta($edit_id,'clubdays',true);
		}
		print '<div class="col-md-6"><p><label>*Choose Day(s)</label></p>';
		$monsundaysoptions = getMonSunDaysOptions();
		print '<ul class="monsunoptions-wrapper" id="monsunoptions-wrapper">';
        $isChecked = '';
		foreach($monsundaysoptions as $monsunkey => $monsunoption) {
			if ( isset($clubdays) ) { $isChecked = in_array($monsunkey,$clubdays) ? 'checked' : '';}
			print '<li><input type="checkbox" name="clubdays[]" value="'.$monsunkey.'" id="'.$monsunkey.'" '.$isChecked.' /> <label for="'.$monsunkey.'">'.$monsunoption.'</label></li>';
		}
		print '</ul>';
        print '</div>';
    }
    ?>
    <div class="col-md-12" style="display: inline-block;">  
        <input type="hidden" name="" id="listing_edit" value="<?php echo $edit_id;?>">
       <!--  <input type="submit" class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="edit_prop_details" value="<?php //esc_html_e('Save', 'wpestate') ?>" /> -->
        <!-- <a href="<?php //echo  $edit_link_location;?>" class="next_submit_page"><?php //esc_html_e('Go to Location settings (*make sure you click save first).','wpestate');?></a>
   -->
    </div>
</div>  


<div class="contact-section">
    <h4 class="user_dashboard_panel_title"><?php esc_html_e('Contact Details','wpestate');?></h4>
    <?php
        for($j=4;$j<sizeof($custom_fields); $j++) {
            $placeholder = '""';
            if($j == 4){
                $placeholder = '"01234 567890"';
            } else if($j == 5){
                $placeholder = '"john@gmail.com" required';
            }
            else if($j == 6){
                $placeholder = '"www.facebook.com"';
            }
            else if($j == 7){
                $placeholder = '"twitter.com"';
            }
            else if($j == 8){
                $placeholder = '"www.instagram.com"';
            }
            $type ='';
            $name ='';
            $label ='';
            $slug ='';

            $name  =   $custom_fields[$j][0];
            $label =   $custom_fields[$j][1];
            $type  =   'long text'; //$custom_fields[$j][2];
            $slug  =   str_replace(' ','_',$name);
            if ($j == 9) {
                $label = 'Web Link Package - ' . $label;
            }
            $slug         =   wpestate_limit45(sanitize_title( $name ));
            $slug         =   sanitize_key($slug);

            print '<div class="col-md-4"><p><label for="'.$slug.'">'.$label.'</label>';
            
            if ($type=='long text')
            {
                 print '<textarea type="text" class="form-control input-textarea" placeholder='.$placeholder.' id="'.$slug.'"  size="0" name="'.$slug.'" rows="3" cols="42">'.$custom_fields_array[$slug].'</textarea>';
            }
            else
            {
                 print '<input type="text" class="form-control" placeholder="'.$placeholder.'" id="'.$slug.'" size="40" name="'.$slug.'" value="'.$custom_fields_array[$slug].'">';
            }
            print '</p>  </div>';
        }
    ?>
    <div class="col-md-12">
        <p>Don’t forget to Upgrade to our Web Link Package to enable bookings - Free Trial Available</p>
    </div>
</div>
