<?php
global $feature_list_array;
global $edit_id;
global $moving_array;
global $edit_link_calendar;
?>
<div class="club_benefits_main">
    <div class="user_dashboard_panel1">
        <h4 class="user_dashboard_panel_title"><?php esc_html_e('Listing Benefits','wpestate');?></h4>
        <div class="col-md-12" id="profile_message_amenity"></div>                
        <?php
        foreach($feature_list_array as $key => $value){
            $post_var_name =   str_replace(' ','_', trim($value) );
            $post_var_name =   wpestate_limit45(sanitize_title( $post_var_name ));
            $post_var_name =   sanitize_key($post_var_name);

            $value_label=$value;
            if (function_exists('icl_translate') ){
                $value_label    =   icl_translate('wpestate','wp_estate_property_custom_amm_'.$value, $value ) ;                                      
            }

            print ' <div class="col-md-3"><p>
                   <input type="hidden"    name="'.$post_var_name.'" value="" style="display:block;">
                   <input type="checkbox"   id="'.$post_var_name.'" name="'.$post_var_name.'" value="1" ';

            if (esc_html(get_post_meta($edit_id, $post_var_name, true)) == 1) {
                print' checked="checked" ';
            }else{
                if(is_array($moving_array) ){                      
                    if( in_array($post_var_name,$moving_array) ){
                        print' checked="checked" ';
                    }
                }
            }
            print' /><label for="'.$post_var_name.'">'.$value_label.'</label></p></div>';  
        }
        ?>    
        <div class="col-md-12" style="display: inline-block;">  
			<div class="col-md-12" id="profile_message-bottom"></div>
            <input type="hidden" name="listing_edit" id="listing_edit" value="<?php echo $edit_id;?>">
            <button type="button" class="g-recaptcha wpb_btn-info wpb_btn-small wpestate_vc_button vc_button" id="recaptcha_club_information" data-sitekey='6LcmXiwaAAAAADSGRVwsFlDt3lgtoGOxnIxV5kIy' data-callback='onSubmit' data-action='submit'><?php esc_html_e('Save', 'wpestate') ?></button>
            <input type="button" class="hidden wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="save_club_information" value="<?php esc_html_e('Save', 'wpestate') ?>" />
            <?php
                    $tday = date("Y-m-d");
                    $calraw = get_post_meta($edit_id,'calendar-expires',true);
                    if(isset($calraw)){
                        if($tday < $calraw){ /* ?>
                            <a href="<?php print $edit_link_calendar;?>"    class="<?php print $activecalendar;?>">    <?php esc_html_e('Calendar','wpestate');?></a>     
                        <?php */
                        ?>        <a id="calelink" href="<?php echo  $edit_link_calendar;?>" class="next_submit_page"><?php esc_html_e('Go to Calendar settings (*make sure you click save first).','wpestate');?></a>

                         <?php } 
                    } ?>
        </div>
    </div>
    </form>
    <script>
        onSubmit = function (token) {
            jQuery(function($) {
                console.log(token);
                $("#save_club_information").click();
            });
        }
    </script>
