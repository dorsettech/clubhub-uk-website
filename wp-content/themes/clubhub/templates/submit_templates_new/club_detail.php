<?php
global $current_user;
global $submit_title;
global $submit_description;
global $property_price; 
global $property_price_per; 
global $property_label; 
global $prop_action_category;
global $prop_action_category_selected;
global $prop_category_selected;
global $property_city;
global $property_area;
global $guestnumber;
global $property_country;
global $property_description;
global $property_admin_area;
global $over18;
global $dbscheck;
global $plinsurance;
global $tandcs;

// Locations page
global $property_latitude;
global $property_longitude;
global $google_camera_angle;
global $property_address;
global $property_zip;
global $property_latitude;
global $property_longitude;
global $google_view_check;
global $google_camera_angle;
global $property_area;
global $property_city;
global $property_country;
global $edit_id;
global $property_county;
global $property_state;
global $edit_link_amenities;

// BENEFITS page
global $feature_list_array;
global $edit_id;
global $moving_array;
global $edit_link_calendar;

// Price Page
global $property_price;
global $property_label;
global $cleaning_fee;
global $city_fee;
global $property_price_week;
global $property_price_month;
global $edit_id;
global $cleaning_fee_per_day;
global $city_fee_per_day;
global $min_days_booking;
global $extra_price_per_guest;
global $price_per_guest_from_one;
global $overload_guest;
global $price_per_weekeend;
global $checkin_change_over;
global $checkin_checkout_change_over;
global $edit_link_images;  
$week_days=array(
    '0'=>esc_html__('All','wpestate'),
    '1'=>esc_html__('Monday','wpestate'), 
    '2'=>esc_html__('Tuesday','wpestate'),
    '3'=>esc_html__('Wednesday','wpestate'),
    '4'=>esc_html__('Thursday','wpestate'),
    '5'=>esc_html__('Friday','wpestate'),
    '6'=>esc_html__('Saturday','wpestate'),
    '7'=>esc_html__('Sunday','wpestate')
);
$wp_estate_currency_symbol = esc_html( get_option('wp_estate_currency_symbol', '') );

// Images page
global $action;
global $edit_id;
global $embed_video_id;
global $option_video;
global $edit_link_details;
$images='';
$thumbid='';
$attachid='';
$arguments = array(
      'numberposts'     => -1,
      'post_type'       => 'attachment',
      'post_parent'     => $edit_id,
      'post_status'     => null,
      'exclude'         => get_post_thumbnail_id(),
      'orderby'         => 'menu_order',
      'order'           => 'ASC'
  );
$post_attachments = get_posts($arguments);
$post_thumbnail_id = $thumbid = get_post_thumbnail_id( $edit_id );  
/*foreach ($post_attachments as $attachment) {
    $preview =  wp_get_attachment_image_src($attachment->ID, 'wpestate_property_listings');    
    
    if($preview[0]!=''){
        $images .=  '<div class="uploaded_images" data-imageid="'.$attachment->ID.'"><img src="'.$preview[0].'" alt="thumb" /><i class="fa fa-trash-o"></i>';
        if($post_thumbnail_id == $attachment->ID){
            $images .='<i class="fa thumber fa-star"></i>';
        }
    }else{
        $images .=  '<div class="uploaded_images" data-imageid="'.$attachment->ID.'"><img src="'.get_template_directory_uri().'/img/pdf.png" alt="thumb" /><i class="fa fa-trash-o"></i>';
        if($post_thumbnail_id == $attachment->ID){
            $images .='<i class="fa thumber fa-star"></i>';
        }
    }
    
    
    $images .='</div>';
    $attachid.= ','.$attachment->ID;
}*/

// Detail page
global $unit;
global $edit_id;
global $property_size;
global $property_lot_size;
global $property_rooms;
global $property_bedrooms;
global $property_bathrooms;
global $custom_fields;    
global $custom_fields_array;
global $edit_link_location;
$measure_sys = esc_html ( get_option('wp_estate_measure_sys','') ); 
?>

<?php if ( is_user_logged_in() ) {    ?>
<form id="new_post" name="new_post" method="post" action="" enctype="multipart/form-data" class="add-estate">
        <?php
        if (function_exists('icl_translate') ){
            print do_action( 'wpml_add_language_form_field' );
        }
        ?>
<?php }else{ ?>
<div id="new_post"  class="add-estate">
<?php } ?>
        
<div class="col-md-12">
    <!-- START :: DESCRIPTION -->
    <div class="user_dashboard_panel">
        <h4 class="user_dashboard_panel_title"><?php  esc_html_e('Description','wpestate');?></h4>
        <div class="alert alert-danger" id="phase1_err"></div>
        
        <div class="col-md-4">           
            <p>
               <label for="title"><?php esc_html_e('*Title (mandatory)','wpestate'); ?> </label>
               <input type="text" id="title" class="form-control" placeholder="Club Name test" value="<?php print $submit_title; ?>" size="20" name="wpestate_title" />
            </p>
        </div>
        
        <div class="col-md-4">
            <p>
                <label for="prop_action_category"> <?php esc_html_e('*Type (mandatory)','wpestate'); $prop_action_category;?></label>
                <?php 
                $args=array(
                        'class'       => 'select-submit2',
                        'hide_empty'  => false,
                        'selected'    => $prop_action_category_selected,
                        'name'        => 'prop_action_category',
                        'id'          => 'prop_action_category_submit',
                        'orderby'     => 'NAME',
                        'order'       => 'ASC',
                        'show_option_none'   => esc_html__( 'None','wpestate'),
                        'taxonomy'    => 'property_action_category',
                        'hierarchical'=> true
                    );

                   wp_dropdown_categories( $args );  ?>
            </p>       
        </div>

        <div class="col-md-4">
            <p>
                <label for="prop_category"><?php esc_html_e('*Category (mandatory)','wpestate');?></label>
                <?php 
                    $args=array(
                            'class'       => 'select-submit2',
                            'hide_empty'  => false,
                            'selected'    => $prop_category_selected,
                            'name'        => 'prop_category',
                            'id'          => 'prop_category_submit',
                            'orderby'     => 'NAME',
                            'order'       => 'ASC',
                            'show_option_none'   => '',
                            'taxonomy'    => 'property_category',
                            'hierarchical'=> true
                        );
                    wp_dropdown_categories( $args ); ?>

            </p>
        </div>
            
        <div class="col-md-4">
            <p>
                <label for="guest_no"><?php esc_html_e('*Maximum Number of Participants','wpestate');?></label>
                <select id="guest_no" name="guest_no">
                    <?php 
                    for($i=0; $i<50; $i++) {
                        print '<option value="'.$i.'" ';
                            if ( $guestnumber==$i){
                                print ' selected="selected" ';
                            }
                        print '>'.$i.'</option>';
                    } ?>
					<option value="50">50+</option>
                </select>    
            </p>
        </div>
        
        <div class="col-md-4">    
            <p>
                <?php
                $show_adv_search_general            =   get_option('wp_estate_wpestate_autocomplete','');
                $wpestate_internal_search           =   '';
                if($show_adv_search_general=='no'){
                    $wpestate_internal_search='_autointernal';
                }
                ?>
                
                <label for="property_city_front"><?php esc_html_e('*City (mandatory)','wpestate');?></label>
                <input type="text"   id="property_city_front<?php echo $wpestate_internal_search;?>" name="property_city_front" placeholder="<?php esc_html_e('Type the city name','wpestate');?>" value="<?php echo $property_city;?>" class="advanced_select  form-control">
                <?php  if($show_adv_search_general!='no'){ ?>
                <input type="hidden" id="property_country" name="property_country" value="<?php echo $property_country;?>">
                <?php } ?>
                <input type="hidden" id="property_city" name="property_city"  value="<?php echo $property_city;?>" >
                <input type="hidden" id="property_admin_area" name="property_admin_area" value="<?php echo $property_admin_area;?>">
            </p>
        </div>    
        <div class="col-md-8" style="clear:both;"> 
            <label for="property_description"><?php esc_html_e('Club Description','wpestate');?></label>
            <textarea  rows="4" id="property_description" name="property_description"  class="advanced_select  form-control" placeholder="<?php esc_html_e('Describe your club','wpestate');?>"><?php echo $property_description; ?></textarea>
        </div>
        <div class="col-md-12">
            <p>
                <label>Terms and Conditions</label>
            </p>
            <p><input type="checkbox" name="over18" <?php if($over18 == 1){ echo 'checked';} ?> value="1"/> I can confirm that I am at least 18 years of age.</p>
            <p><input type="checkbox" name="dbscheck" <?php if($dbscheck == 1){ echo 'checked';} ?> value="1"/> Workers are covered by a valid DBS check (if applicable).</p>
            <p><input type="checkbox" name="plinsurance" <?php if($plinsurance == 1){ echo 'checked';} ?> value="1"/> Activities are covered by valid liability cover.</p>
            <p><input type="checkbox" name="tandcs" <?php if($tandcs == 1){ echo 'checked';} ?> value="1"/> I accept the Club Hub <a href="/terms-conditions/">Terms and Conditions</a></p>
        </div>    
        <?php
        if ( !is_user_logged_in() ) { 
            print '<input type="hidden" name="pointblank" value="1">';  
        }else{
            print '<input type="hidden" name="pointblank" value="0">';   
        }
        ?>    
        <div class="col-md-12" style="display: inline-block;">   
            <?php if ( is_user_logged_in() ) {    ?>
                <input type="submit"  class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button"  disabled  id="form_submit_1" value="<?php esc_html_e('Continue', 'wpestate') ?>" />
            <?php }else{ ?>
                <input type="submit"  class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button externalsubmit"  disabled  id="form_submit_1" value="<?php esc_html_e('Continue', 'wpestate') ?>" />       
            <?php } ?>    
        </div>    
    </div>
    <!-- END :: DESCRIPTION -->
                    
    <!-- START :: Price Panel -->
    <div class="user_dashboard_panel price_panel ">
        <h4 class="user_dashboard_panel_title"><?php esc_html_e('Activity Pricing','wpestate');?></h4>
        <div class="col-md-12" id="profile_message"></div>          
        <div class="col-md-3">
            <p>           
          <!--       <label  class="label_adjust" for="property_price"> <?php esc_html_e('Price per session in ','wpestate');print $wp_estate_currency_symbol.' '; esc_html_e('(only numbers)','wpestate'); ?> </label> -->
                <label  class="label_adjust" for="property_price"> <?php esc_html_e('Price e.g. 3.50 (numbers only please)*','wpestate'); ?></label>
                <input type="text" id="property_price" class="form-control" placeholder="3.50" size="40" name="property_price" value="<?php print number_format($property_price,2); ?>">
            </p>
        </div>
        <div class="col-md-3">
            <p>           
          <!--       <label  class="label_adjust" for="property_price"> <?php esc_html_e('Price per session in ','wpestate');print $wp_estate_currency_symbol.' '; esc_html_e('(only numbers)','wpestate'); ?> </label> -->
                <label  class="label_adjust" for="property_price_per"> <?php esc_html_e('Price per period e.g. per month or per session','wpestate'); ?></label>
                <input type="text" id="property_price_per" class="form-control" placeholder="per session" size="40" name="property_price_per" value="<?php if($property_price_per != ''){ print $property_price_per; } else {
                    print 'per session';
                } ?>">
            </p>
        </div>    
        <div class="col-md-3 ">
            <p>
                <label for="cleaning_fee" ><?php esc_html_e('Booking Fee in ','wpestate');print $wp_estate_currency_symbol.' '; esc_html_e('(only numbers)','wpestate'); ?></label>
                <input type="text" id="cleaning_fee" size="40" placeholder="0.50" class="form-control"  name="cleaning_fee" value="<?php print $cleaning_fee; ?>">
            </p> 
        </div>    
        <div class="col-md-3 check_adjust">
            <p>
                <input style="float:left;" type="checkbox" class="form-control"  value="1" id="cleaning_fee_per_day" name="cleaning_fee_per_day" <?php print $cleaning_fee_per_day; ?> >
                <label style="float:left;" for="cleaning_fee"><?php esc_html_e('Booking Fee applies per session:','wpestate');?></label>
            </p> 
        </div>
        <div class="col-md-3 hidden">
            <p>
                <label for="min_days_booking"> <?php esc_html_e('Minimum sessions of booking (only numbers) ','wpestate'); ?>  </label>
                <input type="text" id="min_days_booking" class="form-control" size="40" name="min_days_booking" value="1">
            </p>
        </div>    
        <div class="col-md-4 hidden">
            <p>
                <label for="checkin_change_over"><?php esc_html_e('Allow only bookings starting with the check in on:','wpestate');?></label>
                <select id="checkin_change_over" name="checkin_change_over" class="select-submit2">
                   <?php 
                    foreach($week_days as $key=>$value){
                        print '   <option value="'.$key.'"';
                        if( $key==$checkin_change_over){
                            print ' selected="selected" ';
                        }
                        print '>'.$value.'</option>';
                    }
                   ?>
                    
                </select>
            </p> 
        </div>    
        <div class="col-md-4 hidden">
            <p>
                <label for="checkin_checkout_change_over"><?php esc_html_e('Allow only bookings with the check in/check out on: ','wpestate');?></label>
                <select id="checkin_checkout_change_over" name="checkin_checkout_change_over" class="select-submit2">
                   <?php 
                    foreach($week_days as $key=>$value){
                       print '   <option value="'.$key.'"';
                        if( $key==$checkin_checkout_change_over){
                            print ' selected="selected" ';
                        }
                        print '>'.$value.'</option>';
                    }
                   ?>
                </select>
            </p> 
        </div>   
        <div class="col-md-12 hidden">
            <p>
                <input style="float:left;" type="checkbox" class="form-control" value="1"  id="price_per_guest_from_one" name="price_per_guest_from_one" <?php print $price_per_guest_from_one; ?> >
                <label style="float:left;" for="price_per_guest_from_one"><?php esc_html_e('Pay by the no of children','wpestate');?></label>            
            </p> 
        </div>    
        <div class="col-md-12" style="display: inline-block;">  
            <input type="hidden" name="" id="listing_edit" value="<?php echo $edit_id;?>">
            <input type="submit" class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="edit_prop_price" value="<?php esc_html_e('Save', 'wpestate') ?>" />
            <a href="<?php echo  $edit_link_images;?>" class="next_submit_page"><?php esc_html_e('Go to Media settings (*make sure you click save first).','wpestate');?></a>  
        </div>    
        <?php wpestate_show_custom_details($edit_id,1);?>    
    </div>
    <!-- END :: Price Panel -->

    <!-- START :: IMAGE Panel -->
    <div class="col-md-12" id="new_post2">
        <div class="user_dashboard_panel">
        <h4 class="user_dashboard_panel_title"><?php  esc_html_e('Listing Media','wpestate');?></h4>
<!--        <div class="col-md-12" id="profile_message"></div>     -->
        
        <div class="col-md-12">
            <div id="upload-container">                 
                <div id="aaiu-upload-container">                 
                    <div id="aaiu-upload-imagelist">
                        <ul id="aaiu-ul-list" class="aaiu-upload-list"></ul>
                    </div>

                    <div id="imagelist">
                    <?php 
                        if($images!=''){
                            print $images;
                        }
                    ?>  
                    </div>

                    <div id="aaiu-uploader"  class=" wpb_btn-small wpestate_vc_button  vc_button"><?php esc_html_e('Click Here to Select Image *','wpestate');?></div>
                    <input type="hidden" name="attachid" id="attachid" value="<?php echo $attachid;?>">
                    <input type="hidden" name="attachthumb" id="attachthumb" value="<?php echo $thumbid;?>">
                <p class="full_form full_form_image"><strong>2MB Filesize limit per image.</strong></p>
                    <p class="full_form full_form_image"><?php esc_html_e('*Click on the image to select featured. ','wpestate');?></p>
                </div>  
            </div>
        </div>

        <div class="col-md-4">
            <p>
                <label for="embed_video_type"><?php esc_html_e('Video from (optional)','wpestate');?></label>
                <select id="embed_video_type" name="embed_video_type" class="select-submit2">
                    <?php print $option_video;?>
                </select>
            </p>
        </div>
            
        <div class="col-md-4">
            <p>     
               <label for="embed_video_id"><?php esc_html_e('Video id: ','wpestate');?></label>
               <input type="text" id="embed_video_id" class="form-control"  name="embed_video_id" size="40" value="<?php print $embed_video_id;?>">
            </p>
        </div>

        <p class="full_form full_form_image"><?php esc_html_e('For example: For https://www.youtube.com/embed/4lUn6GZdFCc the Video ID is 4lUn6GZdFCc','wpestate');?></p>
            
        <div class="col-md-12" style="display: inline-block;"> 
            <input type="hidden" name="" id="listing_edit" value="<?php echo $edit_id;?>">
            <input type="submit" class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="edit_prop_image" value="<?php esc_html_e('Save', 'wpestate') ?>" />
            <a href="<?php echo  $edit_link_details;?>" class="next_submit_page"><?php esc_html_e('Go to Details settings (*make sure you click save first).','wpestate');?></a>  
        </div>       
    </div>    
    <!-- END :: IMAGE Panel -->

    <!-- START :: DETAIL Panel -->
    <div class="user_dashboard_panel">
        <h4 class="user_dashboard_panel_title"><?php esc_html_e('Listing Details','wpestate');?></h4>

        <div class="col-md-12" id="profile_message"></div>
        <?php     
        $i=0;
        if( !empty($custom_fields)){  
            while($i< count($custom_fields) ){
                $placeholder = '';
                if($i == 0){
                    $placeholder = '4';
                } else if($i == 1){
                    $placeholder = '16';
                } else if($i == 2){
                    $placeholder = 'Mondays and Fridays Term-time';
                } else if($i == 3){
                    $placeholder = '4pm Mondays, 6pm Fridays';
                } else if($i == 4){
                    $placeholder = '01234 567890';
                }
                
                $name  =   $custom_fields[$i][0];
                $label =   $custom_fields[$i][1];
                $type  =   $custom_fields[$i][2];
                $slug  =   str_replace(' ','_',$name);

                $slug         =   wpestate_limit45(sanitize_title( $name ));
                $slug         =   sanitize_key($slug);

                $i++;

                if (function_exists('icl_translate') ){
                    $label     =   icl_translate('wpestate','wp_estate_property_custom_front_'.$label, $label ) ;
                }   

                print '<div class="col-md-4"><p><label for="'.$slug.'">'.$label.'</label>';

                if ($type=='long text'){
                     print '<textarea type="text" class="form-control"  id="'.$slug.'"  size="0" name="'.$slug.'" rows="3" cols="42">'.$custom_fields_array[$slug].'</textarea>';
                }else{
                     print '<input type="text" class="form-control" placeholder="'.$placeholder.'" id="'.$slug.'" size="40" name="'.$slug.'" value="'.$custom_fields_array[$slug].'">';
                }
                print '</p>  </div>';

                if ($type=='date'){
                    print '<script type="text/javascript">
                        //<![CDATA[
                        jQuery(document).ready(function(){
                            '.wpestate_date_picker_translation($slug).'
                        });
                        //]]>
                        </script>';
                }
            }
        }
        ?>
        <div class="col-md-12" style="display: inline-block;">  
            <input type="hidden" name="" id="listing_edit" value="<?php echo $edit_id;?>">
            <input type="submit" class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="edit_prop_details" value="<?php esc_html_e('Save', 'wpestate') ?>" />
            <a href="<?php echo  $edit_link_location;?>" class="next_submit_page"><?php esc_html_e('Go to Location settings (*make sure you click save first).','wpestate');?></a>
      
        </div>
    </div>    
    <!-- END :: DETAIL Panel -->

    <!-- START :: LOCATION Panel -->    
    <div class="user_dashboard_panel">
        <h4 class="user_dashboard_panel_title"><?php  esc_html_e('Activity Location','wpestate');?></h4>
        <div class="col-md-12" id="profile_message"></div>
        <div class="col-md-4"> 
            <p>
                <label for="property_address"><?php esc_html_e('Street Address','wpestate');?></label>
                <input type="text" id="property_address" placeholder="4 Test Street" class="form-control" size="40" name="property_address" value="<?php print $property_address;?>">
           </p>
        </div>

        <div class="col-md-3 ">
            <p>
                <label for="property_state"><?php esc_html_e('Town','wpestate');?></label>
                <input type="text" id="property_state" placeholder="Example Town" class="form-control" size="40" name="property_state" value="<?php print $property_state?>">
            </p>
        </div>
       
        <div class="col-md-3">
            <p>
                <label for="property_county"><?php esc_html_e('County','wpestate');?></label>
                <input type="text" id="property_county" placeholder="Devon" class="form-control" size="40" name="property_county" value="<?php print $property_county?>">
            </p>
        </div>

        <div class="col-md-2">
            <p>
                <label for="property_zip"><?php esc_html_e('Post Code','wpestate');?></label>
                <input type="text" id="property_zip" placeholder="AB12 3CD" class="form-control" size="40" name="property_zip" value="<?php print $property_zip;?>">
            </p>
        </div>
           
        <div class="col-md-6">
            <div id="google_capture"  class="wpb_btn-small wpestate_vc_button  vc_button"><?php esc_html_e('Click Here to Place Pin with Address','wpestate');?></div>
        </div>
        
        <div class="col-md-12">
            <div id="googleMapsubmit" ></div>   
        </div>
        
        <div class="col-md-3 hidden">
            <p>            
                 <label for="property_latitude"><?php esc_html_e('Latitude (for Google Maps Pin Position)','wpestate'); ?></label>
                 <input type="text" id="property_latitude" class="form-control" style="margin-right:20px;" size="40" name="property_latitude" value="<?php print $property_latitude; ?>">
            </p>
        </div>
        
        <div class="col-md-3 hidden">
            <p>    
                <label for="property_longitude"><?php esc_html_e('Longitude (for Google Maps Pin Position)','wpestate');?></label>
                <input type="text" id="property_longitude" class="form-control" style="margin-right:20px;" size="40" name="property_longitude" value="<?php print $property_longitude;?>">
            </p>
        </div>
        
        <div class="col-md-3 hidden">
            <p>
                <label for="google_camera_angle"><?php esc_html_e('Street View - Camera Angle (value from 0 to 360)','wpestate');?></label>
                <input type="text" id="google_camera_angle" class="form-control" style="margin-right:0px;" size="5" name="google_camera_angle" value="<?php print $google_camera_angle;?>">
            </p>
        </div>
        
        <input type="hidden" id="property_city_submit" value="<?php echo $property_city?>">
        <input id="property_country" type="hidden" value="<?php echo $property_country;?>">
        
        <div class="col-md-12" style="display: inline-block;">  
            <input type="hidden" name="" id="listing_edit" value="<?php echo $edit_id;?>">
            <input type="submit" class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="edit_prop_locations" value="<?php esc_html_e('Save', 'wpestate') ?>" />
            <a href="<?php echo  $edit_link_amenities;?>" class="next_submit_page"><?php esc_html_e('Go to Benefits settings (*make sure you click save first).','wpestate');?></a>
        </div>
    </div> 
    <!-- END :: LOCATION Panel -->

    <!-- START :: Benefits Panel -->   
    <div class="user_dashboard_panel">
        <h4 class="user_dashboard_panel_title"><?php esc_html_e('Activity Benefits','wpestate');?></h4>
        <div class="col-md-12" id="profile_message"></div>                
        <?php
        foreach($feature_list_array as $key => $value){
            $post_var_name =   str_replace(' ','_', trim($value) );
            $post_var_name =   wpestate_limit45(sanitize_title( $post_var_name ));
            $post_var_name =   sanitize_key($post_var_name);

            $value_label=$value;
            if (function_exists('icl_translate') ){
                $value_label    =   icl_translate('wpestate','wp_estate_property_custom_amm_'.$value, $value ) ;                                      
            }

            print ' <div class="col-md-3"><p>
                   <input type="hidden"    name="'.$post_var_name.'" value="" style="display:block;">
                   <input type="checkbox"   id="'.$post_var_name.'" name="'.$post_var_name.'" value="1" ';

            if (esc_html(get_post_meta($edit_id, $post_var_name, true)) == 1) {
                print' checked="checked" ';
            }else{
                if(is_array($moving_array) ){                      
                    if( in_array($post_var_name,$moving_array) ){
                        print' checked="checked" ';
                    }
                }
            }
            print' /><label for="'.$post_var_name.'">'.$value_label.'</label></p></div>';  
        }
        ?>    
        <div class="col-md-12" style="display: inline-block;">  
            <input type="hidden" name="" id="listing_edit" value="<?php echo $edit_id;?>">
            <input type="submit" class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="edit_prop_ammenities" value="<?php esc_html_e('Save', 'wpestate') ?>" />
            <?php
                    $tday = date("Y-m-d");
                    $calraw = get_post_meta($post_id,'calendar-expires',true);
                    if ( isset($calraw) ) {
                        if($tday < $calraw){ /* ?>
                            <a href="<?php print $edit_link_calendar;?>"    class="<?php print $activecalendar;?>">    <?php esc_html_e('Calendar','wpestate');?></a>     
                        <?php */
                        ?>        <a id="calelink" href="<?php echo  $edit_link_calendar;?>" class="next_submit_page"><?php esc_html_e('Go to Calendar settings (*make sure you click save first).','wpestate');?></a>

                         <?php } 
                    } ?>
        </div>
    </div> 
    <!-- END :: Benefits Panel -->
</div>    
    <input type="hidden" id="security-login-submit" name="security-login-s  ubmit" value="<?php echo estate_create_onetime_nonce( 'submit_front_ajax_nonce' );?>">
<?php 
print ' <input type="hidden" name="estatenonce" value="'.sh_create_onetime_nonce( 'thisestate' ).'"/>';
wp_nonce_field('submit_new_estate','new_estate'); 
function sh_create_onetime_nonce($action = -1) {
    $time = time();
    $nonce = wp_create_nonce($time.$action);
    return $nonce . '-' . $time;
}
?>    
<?php if ( is_user_logged_in() ) {    ?>
</form>  
<?php }else{ 
    echo '<span class="next_submit_page_first_step">'.esc_html__('You must Login / Register in the modal form that shows after you press the Continue button or else your data will be lost. ','wpestate').'</span>';?>
</form>    
<?php } ?>
<div class="modal fade" id="owner_price_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog custom_price_dialog">
        <div class="modal-content">

            <div class="modal-header"> 
              <button type="button" id="close_custom_price_internal" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h2 class="modal-title_big"><?php esc_html_e('Custom Price','wpestate');?></h2>
              <h4 class="modal-title" id="myModalLabel"><?php esc_html_e('Set custom price for selected period','wpestate');?></h4>
            </div>

            <div class="modal-body">
                
                <div id="booking_form_request_mess_modal"></div>    
             
                    <div class="col-md-6">
                        <label for="start_date_owner_book"><?php esc_html_e('Start Date','wpestate');?></label>
                        <input type="text" id="start_date_owner_book" size="40" name="booking_from_date" class="form-control" value="">
                    </div>

                    <div class="col-md-6">
                        <label for="end_date_owner_book"><?php  esc_html_e('End Date','wpestate');?></label>
                        <input type="text" id="end_date_owner_book" size="40" name="booking_to_date" class="form-control" value="">
                    </div>
                        
              
                    <input type="hidden" id="property_id" name="property_id" value="" />
                    <input name="prop_id" type="hidden"  id="agent_property_id" value="">
               
                    <div class="col-md-6">
                        <label for="coment"><?php echo esc_html__( 'New Price in ','wpestate').' '.$wp_estate_currency_symbol;?></label>
                        <input type="text" id="new_custom_price" size="40" name="new_custom_price" class="form-control" value="">
                    </div>    
                
                
                
                <div class="col-md-6">
                    <label for="period_min_days_booking"><?php echo esc_html__( 'Minimum days of booking','wpestate');?></label>
                    <input type="text" id="period_min_days_booking" size="40" name="period_min_days_booking" class="form-control" value="1">
                </div> 
                
                <div class="col-md-6">
                    <label for="period_extra_price_per_guest"><?php echo esc_html__( 'Extra Price per guest per day in','wpestate').' '.$wp_estate_currency_symbol;?></label>
                    <input type="text" id="period_extra_price_per_guest" size="40" name="period_extra_price_per_guest" class="form-control" value="0">
                </div> 
               
                <div class="col-md-6">
                    <label for="period_price_per_weekeend"><?php echo esc_html__( 'Price per weekend in ','wpestate').' '.$wp_estate_currency_symbol;?></label>
                    <input type="text" id="period_price_per_weekeend" size="40" name="period_price_per_weekeend" class="form-control" value="">
                </div>
                
                <div class="col-md-6">
                    <label for="period_checkin_change_over"><?php echo esc_html__( 'Allow only bookings starting with the check in on changeover days','wpestate');?></label>
                    <select id="period_checkin_change_over" name="period_checkin_change_over" class="select-submit2">
                        <?php 
                        foreach($week_days as $key=>$value){
                            print '   <option value="'.$key.'">'.$value.'</option>';
                        }
                        ?>
                    </select>
                </div>
                
                <div class="col-md-6">
                    <label for="period_checkin_checkout_change_over"><?php echo esc_html__( 'Allow only bookings with the check in/check out (changeover) days/nights','wpestate');?></label>
                    <select id="period_checkin_checkout_change_over" name="period_checkin_checkout_change_over" class="select-submit2">
                        <?php 
                        foreach($week_days as $key=>$value){
                            print '<option value="'.$key.'" >'.$value.'</option>';
                        }
                        ?>
                    </select>
                </div>
                
                
                <button type="submit" id="set_price_dates" class="wpb_button  wpb_btn-info  wpb_regularsize   wpestate_vc_button  vc_button"><?php esc_html_e('Set price for period','wpestate');?></button>

            </div><!-- /.modal-body -->

        
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<?php
    function wpestate_get_calendar_price($edit_id,$property_price,$custom_price_array,$mega_details,$initial = true, $echo = true) {
        global $wpdb, $m, $monthnum, $year, $wp_locale, $posts;
        $daywithpost =array();
        // week_begins = 0 stands for Sunday


        $time_now  = current_time('timestamp');
        $now=date('Y-m-d');
        $date = new DateTime();

        $thismonth = gmdate('m', $time_now);
        $thisyear  = gmdate('Y', $time_now);

        $unixmonth = mktime(0, 0 , 0, $thismonth, 1, $thisyear);
        $last_day = date('t', $unixmonth);

        $month_no=1;
            while ($month_no<12){

                wpestate_draw_month_price($edit_id,$property_price,$month_no,$custom_price_array,$mega_details, $unixmonth, $daywithpost,$thismonth,$thisyear,$last_day);

                $date->modify( 'first day of next month' );
                $thismonth=$date->format( 'm' );
                $thisyear  = $date->format( 'Y' );
                $unixmonth = mktime(0, 0 , 0, $thismonth, 1, $thisyear);
                $month_no++;
            }
    }  
    function  wpestate_draw_month_price($edit_id,$property_price,$month_no,$custom_price_array,$mega_details, $unixmonth, $daywithpost,$thismonth,$thisyear,$last_day){
        global $wpdb, $m, $monthnum, $year, $wp_locale, $posts;
       
        if(!is_array($mega_details)){
            $mega_details=array();
        }
        
        $week_begins = intval(get_option('start_of_week'));
        $initial=true;
        $echo=true;

        $table_style='';
        if( $month_no>2 ){
            $table_style='style="display:none;"';
        }

        $calendar_output = '<div class="booking-calendar-wrapper-in-price booking-price col-md-6" data-mno="'.$month_no.'" '.$table_style.'>
            <div class="month-title"> '. date_i18n("F", mktime(0, 0, 0, $thismonth, 10)).' '.$thisyear.' </div>
            <table class="wp-calendar booking-calendar">
        <thead>
        <tr>';

        $myweek = array();

        for ( $wdcount=0; $wdcount<=6; $wdcount++ ) {
            $myweek[] = $wp_locale->get_weekday(($wdcount+$week_begins)%7);
        }

        foreach ( $myweek as $wd ) {
            $day_name = (true == $initial) ? $wp_locale->get_weekday_initial($wd) : $wp_locale->get_weekday_abbrev($wd);
            $wd = esc_attr($wd);
            $calendar_output .= "\n\t\t<th scope=\"col\" title=\"$wd\">$day_name</th>";
        }

        $calendar_output .= '
        </tr>
        </thead>

        <tfoot>
        <tr>';

        $calendar_output .= '
        </tr>
        </tfoot>
        <tbody>
        <tr>';

        // See how much we should pad in the beginning
        $pad = calendar_week_mod(date('w', $unixmonth)-$week_begins);
        if ( 0 != $pad )
                $calendar_output .= "\n\t\t".'<td colspan="'. esc_attr($pad) .'" class="pad">&nbsp;</td>';

        $daysinmonth = intval(date('t', $unixmonth));
        for ( $day = 1; $day <= $daysinmonth; ++$day ) {
            $timestamp = strtotime( $day.'-'.$thismonth.'-'.$thisyear).' | ';
            $timestamp_java = strtotime( $day.'-'.$thismonth.'-'.$thisyear);
            if ( isset($newrow) && $newrow ){
                $calendar_output .= "\n\t</tr>\n\t<tr>\n\t\t";
            }

            $newrow = false;
            $has_past_class='has_future';
            $is_reserved=0;


            $calendar_output .= '<td class="calendar-free '.$has_past_class.'" data-curent-date="'.$timestamp_java.'">';      
            $calendar_output .= '<span class="day-label">'.$day.'</span>';
            
            
            $property_price_week            =   floatval   ( get_post_meta($edit_id, 'price_per_weekeend', true) );
            $weekday = date('N', $timestamp_java); // 1-7
            
            if($weekday ==6 || $weekday==7){
                // WEEKEND days
                if(( array_key_exists ($timestamp_java,$mega_details) && floatval( $mega_details[$timestamp_java]['period_price_per_weekeend']) !=0) ){
                    // we have custom price per weekend
                    $calendar_output .= '<span class="custom_set_price weekend_set_price">'.wpestate_show_price_custom ( $mega_details[$timestamp_java]['period_price_per_weekeend'] ).'</span>'; 
                }else if( $property_price_week!=0 ){
                    // we have general price per weekend
                    $calendar_output .= '<span class="custom_set_price weekend_set_price">'.wpestate_show_price_custom ( $property_price_week ).'</span>'; 
                }else{
                    // no weekedn price
                    $calendar_output .= '<span class="price-day">';
                    if( array_key_exists  ($timestamp_java,$custom_price_array) ){
                        $calendar_output .= wpestate_show_price_custom ( $custom_price_array[$timestamp_java]);
                    }else{
                        $calendar_output .= wpestate_show_price_custom ( $property_price );
                    }
                   
                    $calendar_output .= '</span>'; 
                }
            }else{
               // days during the week 
                if( array_key_exists  ($timestamp_java,$custom_price_array) ){
                    // custom price
                    $calendar_output .= '<span class="custom_set_price">'.wpestate_show_price_custom ( $custom_price_array[$timestamp_java] ).'</span>'; 
                }else{
                    // default price
                    $calendar_output .= '<span class="price-day">'.wpestate_show_price_custom ( $property_price ).'</span>'; 
                }
                
            }
            
            $calendar_output .='</td>';
            if ( 6 == calendar_week_mod(date('w', mktime(0, 0 , 0, $thismonth, $day, $thisyear))-$week_begins) )
                $newrow = true;
            }

            $pad = 7 - calendar_week_mod(date('w', mktime(0, 0 , 0, $thismonth, $day, $thisyear))-$week_begins);
            if ( $pad != 0 && $pad != 7 ){
                $calendar_output .= "\n\t\t".'<td class="pad" colspan="'. esc_attr($pad) .'">&nbsp;</td>';
            }
            $calendar_output .= "\n\t</tr>\n\t</tbody>\n\t</table></div>";

            if ( $echo ){
                echo apply_filters( 'get_calendar',  $calendar_output );
            }else{
                return apply_filters( 'get_calendar',  $calendar_output );
            }
                
    }
?>