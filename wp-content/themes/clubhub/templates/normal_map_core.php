<?php
global $options;
global $prop_selection;
global $property_list_type_status;
global $full_page;
global $term;
global $taxonmy;
global $book_from;
global $book_to;
global $listing_type;
global $property_unit_slider;

$records_found = get_query_var('records_found');
$is_search_archive = get_query_var('is_search_archive');

$filtered_size = sizeof($prop_selection->posts);
$property_unit_slider = esc_html ( get_option('wp_estate_prop_list_slider','') );
$listing_type   =   get_option('wp_estate_listing_unit_type','');
$page_tax       =   '';

if( $options['content_class'] == "col-md-12" ) {
    $full_page=1;
}

ob_start(); 

if ( isset($_GET['advanced_lat']) && $prop_selection->found_posts > 0 ) {
    while ( $prop_selection->have_posts() ) { $prop_selection->the_post(); 
        get_template_part('templates/property_unit');
    }
} 

if ($filtered_size > 0 && !isset($_GET['advanced_lat'] )) {
    while ( $prop_selection->have_posts() ) { $prop_selection->the_post(); 
        get_template_part('templates/property_unit');
    }
}

$templates = ob_get_contents();
ob_end_clean(); 
wp_reset_query(); 
wp_reset_postdata();
?>

<div class="row content-fixed">
    <?php get_template_part('templates/breadcrumbs'); ?>
    <div class=" <?php print $options['content_class'];?>  ">
        
    <?php if( !is_tax() ) {
    while (have_posts()) : the_post();  
        if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) == 'yes') { 
            if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) == 'yes') { 
                if ( is_page_template('advanced_search_results.php') ) { ?>
                    <h1 class="entry-title title_list_prop"><?php the_title(); 
                        if ($filtered_size == 0) {
                           print ': '.esc_html__( 'No results found','wpestate');
                        } else {
                           print ': <span class="post-count">'.$prop_selection->found_posts .'</span> '.esc_html__( 'results','wpestate').' displayed nearest to you';
                        }?>
                    </h1>
                <?php } else { ?>
                    <h1 class="entry-title title_list_prop"><?php the_title();?></h1>   
                <?php }               
            }
        } ?>
    <div class="single-content"><?php the_content();?></div>
    <?php endwhile;  
    } else {
        $term_data  =   get_term_by('slug', $term, $taxonmy);
        $place_id   =   $term_data->term_id;
        $term_meta  =   get_option( "taxonomy_$place_id");

        if ( isset($term_meta['pagetax']) ) {
           $page_tax=$term_meta['pagetax'];           
        }

        if ( $page_tax != '' ) {
            $content_post = get_post($page_tax);
            $content = $content_post->post_content;
            $content = apply_filters('the_content', $content);
            echo $content;
        } ?>

        <h1 class="entry-title title_prop"> 
            <?php esc_html_e('Clubs listed in ','wpestate');
            single_cat_title(); ?>
        </h1>
    <?php }
        
    if ( $property_list_type_status == 2 ){
        get_template_part('templates/advanced_search_map_list');
    } ?>

    <!--Filters starts here-->     
    <?php get_template_part('templates/property_list_filters'); ?> 
    <!--Filters Ends here-->   
    <?php get_template_part('templates/compare_list'); ?>
        
	<div class="row">
		<div class="listing_filters_head col-md-5 col-xs-12">
		<?php if(isset($_GET['advanced_lat']) && $_GET['advanced_lat'] != '') { ?> 
			<input class="form-control" type="hidden" value="<?php echo $_GET['advanced_lat'] ?>" name="advanced_lat" id="filter_advanced_lat" />
			<input class="form-control" type="hidden" value="<?php echo $_GET['advanced_lng'] ?>" name="advanced_lng" id="filter_advanced_lng" />
		<?php } ?>

	   <?php if ( isset($_GET['category']) && is_array($_GET['category']) && (isset($_GET['category'][0]) && $_GET['category'][0] != '') ) {
            $args = wpestate_get_select_arguments();
            $allowed_html_list = array( 
                'li' => array(
                    'data-value'        => array(),
                    'role'              => array(),
                    'data-parentcity'   => array(),
                    'data-value2'       => array(),
                ),
            );
            $categ_select_list  =   wpestate_get_category_select_list($args); ?>
	
            <input type="hidden" value="<?php echo implode(",",$_GET['category']) ?>" name="curr_cat" id="curr_cat" />
            
            <input required id="filter_search_age" name="filter_search_age" class="form-control" type="hidden" value="<?php echo isset($_GET['search_age']) && $_GET['search_age'] != '' ? $_GET['search_age'] : '' ?>">
            
            <div class="col-xs-12 col-sm-6">
                <div class="dropdown form_control listing_filter_select" >
                    <div data-toggle="dropdown" id="a_filter_categ" class="filter_menu_trigger" data-value="<?php echo '' ?>"> <?php echo 'Filter by Type' ?> <span class="caret caret_filter"></span> </div>           
                    <ul class="dropdown-menu filter_menu" role="menu" aria-labelledby="a_filter_categ">
                        <?php print wp_kses( $categ_select_list, $allowed_html_list ); ?>
                    </ul>        
                </div>      
            </div>
        <?php } ?>
			<div class="col-xs-12 col-sm-6 filter-by-price-wrapper" style="">
				<div class="dropdown form_control listing_filter_select filter-by-price" >
                    <div data-toggle="dropdown" id="filter-by-price" class="filter_menu_trigger" data-value="<?php echo '' ?>"> <?php echo 'Sort by' ?> <span class="caret caret_filter"></span> </div>           
                    <ul  class="dropdown-menu filter_menu filter-by-price-menu" id="filter-by-price-menu" role="menu" aria-labelledby="filter-by-price">
						<li role="presentation" data-value="2">Price Low to High</li>
						<li role="presentation" data-value="1">Price High to Low</li>
						<li role="presentation" data-value="0">Distance</li>
                    </ul>        
                </div>  
			</div>
			<div id="remove-filter-by-distance" class="col-md-2 filter-by-price-wrapper" style="margin-left: 5px;">
				Filter by: <br/>
				<span><a href="javascript:void(0)" class="filter_menu_trigger filter-by-price" id="filter-by-distance" data-value="">Distance</a><span title="remove filter" id="remove-filter-by-distance" style="display:none;">X</span></span>
			</div>
		</div>
		<div class="col-xs-12 col-md-7 filter-by-days" style="margin-bottom: 20px;">
			<ul style="justify-content:space-between;display:flex;">
				<li data-value="monday">    <input type="checkbox" value="monday"       class="monsundays_checkbox" name="monsundays[]" /> Mon  </li>
				<li data-value="tuesday">   <input type="checkbox" value="tuesday"      class="monsundays_checkbox" name="monsundays[]" /> Tues </li>
				<li data-value="wednesday"> <input type="checkbox" value="wednesday"    class="monsundays_checkbox" name="monsundays[]" /> Wed  </li>
				<li data-value="thursday">  <input type="checkbox" value="thursday"     class="monsundays_checkbox" name="monsundays[]" /> Thurs</li>
				<li data-value="friday">    <input type="checkbox" value="friday"       class="monsundays_checkbox" name="monsundays[]" /> Fri  </li>
				<li data-value="saturday">  <input type="checkbox" value="saturday"     class="monsundays_checkbox" name="monsundays[]" /> Sat  </li>
				<li data-value="sunday">    <input type="checkbox" value="sunday"       class="monsundays_checkbox" name="monsundays[]" /> Sun  </li>
			</ul>
		</div>
	</div>

    <!-- Listings starts here -->                   
    <?php get_template_part('templates/spiner'); ?> 
    <div id="listing_ajax_container" class="row"> 
        <?php
        print $templates;
        ?>
    </div>
    <!-- Listings Ends  here --> 
        <div class="original-pagination-container">
            <?php kriesi_pagination_ajax($prop_selection->max_num_pages, $range =2, $prop_selection->query['paged'], 'pagination_ajax'); ?>       
        </div>
    </div><!-- end 8col container-->
    
<?php include(locate_template('sidebar.php')); ?>
</div>