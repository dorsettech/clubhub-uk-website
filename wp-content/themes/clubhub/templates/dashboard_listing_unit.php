<?php
global $edit_link;
global $token;
global $processor_link;
global $paid_submission_status;
global $submission_curency_status;
global $price_submission;
global $floor_link;
global $show_remove_fav;
global $curent_fav;
global $th_separator;
global $user_pack;
$extra= array(
        'class'         =>  'lazyload img-responsive',    
        );

$post_id                    =   get_the_ID();
$preview                    =   wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'wpestate_property_listings',$extra);
$edit_link                  =   esc_url_raw ( add_query_arg( 'listing_edit', $post_id, $edit_link) ) ;
$edit_link                  =   esc_url_raw ( add_query_arg( 'action', 'description', $edit_link) ) ;               
$floor_link                 =   esc_url_raw ( add_query_arg( 'floor_edit', esc_url($post_id), $floor_link) ) ;
$post_status                =   get_post_status($post_id);

$property_address           =   esc_html ( get_post_meta($post_id, 'property_address', true) );
$property_city              =   get_the_term_list($post_id, 'property_city', '', ', ', '') ;
$property_category          =   get_the_term_list($post_id, 'property_category', '', ', ', '');
$property_action_category   =   get_the_term_list($post_id, 'property_action_category', '', ', ', '');
$price_label                =   esc_html ( get_post_meta($post_id, 'property_label', true) );
$price                      =   get_post_meta($post->ID, 'property_price', true) ;
$currency                   =   esc_html( get_option('wp_estate_submission_curency', '') );
$currency_title             =   esc_html( get_option('wp_estate_currency_symbol', '') );
$where_currency             =   esc_html( get_option('wp_estate_where_currency_symbol', '') );
$status                     =   '';
$link                       =   '';
$pay_status                 =   '';
$is_pay_status              =   '';
$paid_submission_status     =   esc_html ( get_option('wp_estate_paid_submission','') );
$price_submission           =   floatval( get_option('wp_estate_price_submission','') );
$price_featured_submission  =   floatval( get_option('wp_estate_price_featured_submission','') );
$price_calendar_submission  =   floatval( get_option('wp_estate_price_calendar_submission','') );
$price_weblink_submission  =   floatval( get_option('wp_estate_price_weblink_submission','') );
$price_local_submission  =   floatval( get_option('wp_estate_price_local_submission','') );
$price_social_submission  =   floatval( get_option('wp_estate_price_social_submission','') );
$tday = date("Y-m-d");


$club_active_subscription = get_post_meta($post_id, 'club_active_subscription', 0) ;
$paypal_subscription_id = get_post_meta($post_id, 'club_subscription_id',true);
$paypal_plan_id = get_post_meta($post_id, 'club_plan_id',true);
$paypal_product_id = get_post_meta($post_id, 'club_product_id',true);

if ($price != 0) {
    
   //$price =   number_format($price,2,'.',$th_separator);
    
   if ($where_currency == 'before') {
       $price_title =   $currency_title . ' ' . $price;
       $price       =   $currency . ' ' . $price;
   } else {
       $price_title = $price . ' ' . $currency_title;
       $price       = $price . ' ' . $currency;
     
   }
}else{
    $price='';
    $price_title='';
}

$fav_mes        =   esc_html__( 'add to favourites','wpestate');
if($curent_fav){
    if ( in_array ($post->ID,$curent_fav) ){
    $favorite_class =   'icon-fav-on';   
    $fav_mes        =   esc_html__( 'remove from favourites','wpestate');
    } 
}

if($post_status=='expired'){ 
    $status='<span class="label label-danger">'.esc_html__( 'Expired','wpestate').'</span>';
}else if($post_status=='publish'){ 
    $link= esc_url ( get_permalink() );
    $status='<span class="label label-success">'.esc_html__( 'Published','wpestate').'</span>';
}else if($post_status=='disabled'){ 
    $link= '';
    $status='<span class="label label-disabled">'.esc_html__( 'Disabled','wpestate').'</span>';
}else{
    $link='';
    $status='<span class="label label-info">'.esc_html__( 'Waiting for approval','wpestate').'</span>';
}

$plan_trial = 0;
if ($paid_submission_status=='per listing'){
    $pay_status    = get_post_meta(get_the_ID(), 'pay_status', true);
    if($pay_status=='paid'){
        $is_pay_status.='<span class="label label-success">'.esc_html__( 'Paid','wpestate').'</span>';
    }
    if($pay_status=='not paid'){
        // $is_pay_status.='<span class="label label-info">'.esc_html__( 'Not Paid','wpestate').'</span>';

        
        $is_active_subscription =  get_post_meta($post_id,'club_active_subscription',true);

            if($is_active_subscription == 1) {

                $existing_subscription_id = get_post_meta($prop_id, 'club_subscription_id', true);

                if($existing_subscription_id != '') {

                    $paypal_status                  =   esc_html( get_option('wp_estate_paypal_api','') );

                    $host                           =   'https://api.sandbox.paypal.com';  

                    if($paypal_status=='live'){
                        $host='https://api.paypal.com';
                    }

                    $url                =   $host.'/v1/oauth2/token'; 
                    $postArgs           =   'grant_type=client_credentials';
                    $token              =   wpestate_get_access_token($url,$postArgs);

                    $url = $host.'/v1/billing/subscriptions/'.$existing_subscription_id;


                    $dump_req = '';
                    $json = '';

                    $subscription_response_json = wpestate_make_post_call($url, $json,$token,0);

                    $plan_after_one_month = strtotime($subscription_response_json['start_time'] . '+ 1 month');
                    // $plan_start_time = strtotime($subscription_response_json['start_time']);

                    $today = strtotime(date('Y-m-d'));

                    if($subscription_response_json['status'] == 'ACTIVE' && $today > $plan_after_one_month) {
                        $plan_trial = 1;

                    }
                }

            }

        $is_weblink = get_post_meta($post_id,'prop_weblink',true);
        if($plan_trial == 1) {
            $is_pay_status.='<span class="label label-info">'.esc_html__( 'Trial','wpestate').'</span>';
        } elseif($is_weblink) {
            $is_pay_status.='<span class="label label-info">'.esc_html__( 'Paid','wpestate').'</span>';
        } else {
            $is_pay_status.='<span class="label label-info">'.esc_html__( 'Not Paid','wpestate').'</span>';
        }
    }
}
$featured  = intval  ( get_post_meta($post->ID, 'prop_featured', true) );
$localBoost  = intval  ( get_post_meta($post->ID, 'prop_localman', true) );

$free_feat_list_expiration= intval ( get_option('wp_estate_free_feat_list_expiration','') );
$pfx_date = strtotime ( get_the_date("Y-m-d",  $post->ID ) );
$expiration_date=$pfx_date+$free_feat_list_expiration*24*60*60;

?>




<div class="col-md-6 col-sm-6 col-xs-12 flexdashbaord wrapcontainer<?php echo $post->ID; ?>">
    <div class="dasboard-prop-listing">
    
        <div class="blog_listing_image dashboard_imagine">
            <?php


            if ( $featured==1 || $localBoost == 1 ) {
                print '<span class="label label-primary featured_div">'.esc_html__( 'featured','wpestate').'</span>';
            }
            if (has_post_thumbnail($post_id)){
            ?>
            <!--    <a href="<?php print $link; ?>"><img  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php  print $preview[0]; ?>" class="b-lazy img-responsive " alt="slider-thumb" /></a> -->
            <a href="<?php print $link; ?>"><img src="<?php  print $preview[0]; ?>" class="b-lazy img-responsive " alt="slider-thumb" /></a>
            
            <?php 
            } else{ 
                $thumb_prop_default =  get_template_directory_uri().'/img/defaultimage_prop.jpg';?>
                <!-- <img data-src="<?php // echo $thumb_prop_default;?>"  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" class="b-lazy img-responsive wp-post-image " alt="no thumb" />  -->       
                <img src="<?php echo $thumb_prop_default;?>"   class="b-lazy img-responsive wp-post-image " alt="no thumb" />         
            <?php    
            }
            ?>
        </div>
        <?php if ( !isset($show_remove_fav) || $show_remove_fav == '' || $show_remove_fav == '0') { ?>
			<div class="user_dashboard_status">
				<?php print $status.$is_pay_status;?>      
			</div>
		<?php } ?>
		
         <div class="prop-info">
           

            <h4 class="listing_title">
                <a href="<?php print $link; ?>">
                <?php
                $title=get_the_title();
                echo $title;
//                echo mb_substr( html_entity_decode( $title ), 0, 28); 
//                if(strlen($title)>28){
//                    echo '...';   
//                }
                ?>
                </a> 
            </h4>

            <div class="user_dashboard_listed">
                <?php print esc_html__( 'Price','wpestate').': <span class="price_label"> '. $price_title.' '.$price_label.'</span>';
                if ( $paid_submission_status=='membership' && $user_pack=='') {
                    echo ' | ' ; esc_html_e('expires on ','wpestate');echo date("Y-m-d",$expiration_date);
                } 
                ?>
            </div>

            <div class="user_dashboard_listed">
                 <?php esc_html_e('Club Type:','wpestate');?>  
                 <?php print $property_action_category; ?> 
                                      
            </div>
            <div class="user_dashboard_listed">
                 <?php esc_html_e('Listed in:','wpestate');?>  
                 <?php print $property_category;?>                     
            </div>

            <div class="user_dashboard_listed">
                 <?php print esc_html__( 'City','wpestate').': ';?>            
                 <?php print get_the_term_list($post_id, 'property_city', '', ', ', '');?>
                         
            </div>
			<?php if ( !isset($show_remove_fav) || $show_remove_fav == '' || $show_remove_fav == '0') { ?>
            <div class="user_dashboard_listed">
                <?php

                $packagelink = false; //calendar package

                    $linkraw = get_post_meta($post_id,'weblink-expires',true);
                    if(isset($linkraw)){
                        if($tday < $linkraw){ 
                            $packagelink = true; //calendar package
                        } 
                    }

                    $is_weblink = get_post_meta($post_id,'prop_weblink',true);
                    $paypal_action_btn = 0;


                    if($is_weblink == 1) {
                        //weblink is enable
                        //determine how it is enable
                        if($plan_trial == 1) {
                            //enabled through the paypal
                            //show cancel button
                            print'<input type="button" class="free_trail listing_free_trial_cancel" name="free_trail_cancel" value="Cancel Subscription" data-listingid="'.$post->ID.'">';
                        }
                    } elseif($is_weblink != 1 && $plan_trial !=1 ) {
                            //show paypal enable button
                            print'<input type="button" class="free_trail listing_free_trial" name="free_trail" value="Enable Web Link Package - Free Trial" data-listingid="'.$post->ID.'"><span class="submit-price submit-price-featured"></span>';
                        
                    }



                ?>
            </div>
			<?php } ?>
            <?php 
            if ( isset($show_remove_fav) && $show_remove_fav==1 ) {
                print '<div class="info-container-payments favorite-wrapper"><span class="icon-fav icon-fav-on-remove" data-postid="'.$post->ID.'"> '.$fav_mes.'</span></div>';
            } else{ 
            ?>
         
             
                <div class="info-container">
                    <a  data-original-title="<?php esc_html_e('Edit club','wpestate');?>"   class="dashboad-tooltip" href="<?php  print $edit_link;?>"><i class="fa fa-pencil editprop"></i></a>
                    <a  data-original-title="<?php esc_html_e('Delete club','wpestate');?>" class="dashboad-tooltip" onclick="return confirm(' <?php echo esc_html__( 'Are you sure you wish to delete ','wpestate').get_the_title(); ?>?')" href="<?php print esc_url ( add_query_arg( 'delete_id', $post_id,esc_url($_SERVER['REQUEST_URI']) ) );?>"><i class="fa fa-times deleteprop"></i></a>  
                    <?php
                    if( $post_status == 'expired' ){ 
                        print'<span data-original-title="'.esc_html__( 'Resend for approval','wpestate').'" class="dashboad-tooltip resend_pending" data-listingid="'.$post_id.'"><i class="fa fa-arrow-up"></i></span>';   
                    }
                    
//                    if($paid_submission_status=='membership'){
//                        if ( intval(get_post_meta($post_id, 'prop_featured', true))==1){
//                             print '<span class="label label-success is_featured">'.esc_html__( 'Club is featured','wpestate').'</span>';       
//                        }
//                        else{
//                            print ' <span  data-original-title="'.esc_html__( 'Set as featured','wpestate').'" class="dashboad-tooltip make_featured" data-postid="'.$post_id.'" ><i class="fa fa-star favprop"></i></span>';
//                        }
//                    }
                    
                    
                         print '<span class="activate_payments">'.esc_html__( 'Upgrade','wpestate').'</span>';
                    
                    ?>
                    
                    <?php
                    if( $post_status == 'publish' ){ 
                        print ' <span  data-original-title="'.esc_html__( 'Disable Listing','wpestate').'" class="dashboad-tooltip disable_listing" data-postid="'.$post_id.'" ><i class="fa fa-pause"></i></span>';
                    }elseif( $post_status == 'draft' || $post_status == 'pending' ){ 
                        print ' <span  data-original-title="'.esc_html__( 'Disable Listing','wpestate').'" id="disabled_draft" class="disabled_draft dashboad-tooltip disable_listing" data-postid="'.$post_id.'" ><i class="fa fa-pause"></i></span>';
                    }else if($post_status=='disabled') {
                        print ' <span  data-original-title="'.esc_html__( 'Enable Listing','wpestate').'" class="dashboad-tooltip disable_listing" data-postid="'.$post_id.'" ><i class="fa fa-play"></i></span>';
                  
                    }
                    //if($paid_submission_status=='per listing'){?>
                    <a data-original-title="<?php esc_html_e('Clone club','wpestate');?>"   class="dashboad-tooltip clone_club" id = <?php echo $post_id; ?> OnClick="display(this);"><i class="fa fa-clone" aria-hidden="true"></i></a>
                    <?php
                        //print '<span class="clone_club" id="'.$post_id.'" title="Clone club"><i class="fa fa-clone" aria-hidden="true"></i></span>';
                    ?>                
                </div>             
                <div class="info-container-payments" style="display:none;"> 
                <?php $pay_status = get_post_meta($post_id, 'pay_status', true);
                if ( $post_status !== 'expired' ) {

                    $enable_paypal_status= esc_html ( get_option('wp_estate_enable_paypal','') );
                    $enable_stripe_status= esc_html ( get_option('wp_estate_enable_stripe','') );

                    print'
                        <div class="listing_submit" style="display:none;">
                        <button type="button"  class="close close_payments" data-dismiss="modal" aria-hidden="true">x</button>';

                    $package['weblink-expires'] = [
                        'expired'   =>  true,
                        'class'     =>  "extra_weblink",
                        'label'     =>  "Web Link Package",
                        'price'     =>  number_format($price_weblink_submission,2),
                        'period'    =>  'Y,1'
                    ]; //link package
                    $package['prop_localman'] = [
                        'expired'   =>  true,
                        'class'     =>  "extra_local",
                        'label'     =>  "Local Boost Package",
                        'price'     =>  number_format($price_local_submission,2),
                        'period'    =>  'W,2'
                    ]; //local package
                    $package['prop_featured'] = [
                        'expired'   =>  true,
                        'class'     =>  "extra_featured",
                        'label'     =>  "Featured Package",
                        'price'     =>  number_format($price_featured_submission,2),
                        'period'    =>  'W,4'
                    ]; //featured package

                    $paypal_status  =   esc_html( get_option('wp_estate_paypal_api','') );
                    if($paypal_status == 'live'){
                        $paypal_url = "https://www.paypal.com/cgi-bin/webscr";
                    }else {
                        $paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
                    }  

                    print "<form action=".$paypal_url." method='post' class='paymentmethod$post->ID'>";
                    foreach ($package as $key => $pack) {
                        if ( $key !== 'prop_featured' ) {
                            $date = get_post_meta($post_id, $key, true);
                            if ( isset($date) ) {
                                if ( $tday < $date ) {
                                    $package[$key]['expired'] = false;
                                }
                            }
                        } elseif ( $key === 'weblink-expires' ) {
                            if ( $is_weblink == 1 ) {
                                $package[$key]['expired'] = false;
                            }
                        } else {
                            if ( $featured == 1 ) {
                                $package[$key]['expired'] = false;
                            }
                        }

                        if ( $package[$key]['expired']) {
                            print '
                                <input id="'.$key.$post->ID.'" type="radio" class="'.$package[$key]['class'].' extra_option_feature extraoption'.$post->ID.'" name="package" data-period="'.$package[$key]['period'].'"data-price="'.$package[$key]['price'].'" value="1" data-id="'.$post->ID.'" data-label="'.$package[$key]['label'].'"><label for="'.$key.$post->ID.'" class="submit-price"> '.$package[$key]['label']. ' £' .$package[$key]['price'].'</label><br><br>';
                        } else {
                            print '<div class="listing_submit_spacer" style="height:auto;"><span class="label label-success featured_label">'. $package[$key]['label'] .'</span></div>';
                        }
                    }

                    $stripe_class='';
                    if($enable_paypal_status==='yes'){
                        $stripe_class=' stripe_paypal ';
                        //print ' <div class="listing_submit_normal label label-danger" data-listingid="'.$post_id.'">'.esc_html__( 'pay','wpestate').'</div>';
                        ?>
                            <input type="hidden" name="charset" value="utf-8">
                            <input type="hidden" name="cmd" value="_xclick-subscriptions">
                            <input type="hidden" name="business" value="MZMEFAZQ9VQR2">
                            <input type="hidden" name="item_name" value="" class="producttitle">
                            <input type="hidden" name="a3" value="" class="amountpay">
                            <input type="hidden" name="p3" value="">
                            <input type="hidden" name="t3" value="">
                            <input type="hidden" name="src" value="1">
                            <input type="hidden" name="currency_code" value="GBP">
                            <input type="hidden" name="no_note" value="1">
                            <input type="hidden" name="return" value="<?=wpestate_get_procesor_link();?>">
<!--                            <input type="hidden" name="notify_url" value="https://clubhubuk.co.uk/?wp_paypal_ipn=1">-->
                            <input type="hidden" name="bn" value="WPPayPal_Subscribe_WPS_US">
                            <?php /* <input class="submit-paypal" style="display:none;width:100%" type="submit" border="0" name="submit" data-listingid="<?= $post_id; ?>"> */?>
                            <button class="submit-paypal" style="display:none;width:100%" type="button" border="0" name="submit" >Submit</button>
                        </form>
                        <?php 
                    }

                    if($enable_stripe_status==='yes'){
                        wpestate_show_stripe_form_per_listing($stripe_class,$post_id,$price_submission,$price_featured_submission);
                    }
                    print  '</p></div>'; 

                }else{
                    print '<div class="listing_submit style="display:none;"">
                    <button type="button"  class="close close_payments" data-dismiss="modal" aria-hidden="true">x</button>';


                    if ( $featured ==1 ){
                        print ' <div class="listing_submit_spacer" style="height:118px;"><span class="label label-success  featured_label">'.esc_html__( 'Club is featured','wpestate').'</span>   </div>';  
                    }else{
                        print'
                        <div class="listing_submit_spacer">
                            '.esc_html__( 'Featured Listing','wpestate').': <span class="submit-price submit-price-featured">'.$price_featured_submission.'</span><span class="submit-price"> '.$currency.'</span> </br>
                        </div>';

                        $stripe_class='';
                        if($enable_paypal_status==='yes'){
                            // - '.$price_featured_submission.' '.$currency.'
                            print'<span class="listing_upgrade label label-danger" data-listingid="'.$post_id.'">'.esc_html__( 'Set as Featured','wpestate').'</span>'; 
                        }
                        if($enable_stripe_status==='yes'){
                            wpestate_show_stripe_form_upgrade($stripe_class,$post_id,$price_submission,$price_featured_submission);
                        }
                    } 
                    print '</div>';
                }
            }?>

            </div>
        </div>
    </div>
<?php if ( strpos($_SERVER['REQUEST_URI'], 'favourite') === false ) { echo '</div>'; } ?>
 <script type="text/javascript">
        jQuery(".disabled_draft").unbind("click");
        jQuery(".disabled_draft").css("pointer-events", "none");
        jQuery('.disabled_draft').prop("disabled",true);
         function display(el) {
			jQuery(".pre_loader_wrap").css({'display': 'flex'});
            var club_id = jQuery(el).attr('id');
            //var club_id = this.id;
            var redirect = "<?php echo get_permalink( get_page_by_path( 'user-dashboard' ) ); ?>";
            jQuery.ajax({
                method : "POST",
                url : "<?php echo admin_url('admin-ajax.php'); ?>",
                data : {action: "clone_club", club_id: club_id},
                success : function(data){      
                    if(data > 0){
                        window.location.href = redirect;
                    }
                }
            });
        }        
 </script>