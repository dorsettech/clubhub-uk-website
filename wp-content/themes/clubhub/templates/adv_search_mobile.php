<?php
$adv_submit             =   wpestate_get_adv_search_link();

//  show cities or areas that are empty ?
$args = wpestate_get_select_arguments();

$action_select_list =   wpestate_get_action_select_list($args);
$categ_select_list  =   wpestate_get_category_select_list($args);
$select_city_list   =   wpestate_get_city_select_list($args); 
$select_area_list   =   wpestate_get_area_select_list($args);

$home_small_map_status              =   esc_html ( get_option('wp_estate_home_small_map','') );
$show_adv_search_map_close          =   esc_html ( get_option('wp_estate_show_adv_search_map_close','') );
$class                              =   'hidden';
$class_close                        =   '';
$guest_list             =   wpestate_get_guest_dropdown();
$allowed_html = '';
?>


<div id="adv-search-header-mobile"> 
    Find Your Kids Activity 
</div>   




<div class="adv-search-mobile"  id="adv-search-mobile"> 
   
    <form method="get"  id="form-search-mobile" action="<?php print $adv_submit; ?>" >
        <?php
        if (function_exists('icl_translate') ){
            print do_action( 'wpml_add_language_form_field' );
        }
        ?>
        <div class="col-md-4 map_icon">  
            <?php
            $show_adv_search_general            =   get_option('wp_estate_wpestate_autocomplete','');
            $wpestate_internal_search           =   '';
            if($show_adv_search_general=='no'){
                $wpestate_internal_search='_autointernal';
                print '<input type="hidden" id="stype" name="stype" value="tax">';
            }
            ?>
            
            <input type="text" id="search_location_mobile<?php echo $wpestate_internal_search;?>"      class="form-control" name="search_location" placeholder="Location / Post Code" value="" >              
            <input type="hidden" id="advanced_city_mobile"      class="form-control" name="advanced_city" data-value=""  value="<?php if(isset( $_GET['advanced_city'] )){echo wp_kses( esc_attr($_GET['advanced_city']),$allowed_html);}?>" >              
            <input type="hidden" id="advanced_area_mobile"      class="form-control" name="advanced_area"   data-value="" value="<?php if(isset( $_GET['advanced_area'] )){echo wp_kses ( esc_attr($_GET['advanced_area']),$allowed_html);}?>"  >              
            <input type="hidden" id="advanced_country_mobile"   class="form-control" name="advanced_country"   data-value="" value="<?php if(isset( $_GET['advanced_country'] )){echo wp_kses ( esc_attr($_GET['advanced_country']),$allowed_html);}?>" >
             <input type="hidden" id="property_admin_area_mobile" name="property_admin_area" value="">
             <input id="advanced_lat_mobile" name="advanced_lat" class="form-control" type="hidden" value="">
            <input id="advanced_lng_mobile" name="advanced_lng" class="form-control" type="hidden" value="">
        </div>
        
        <!--<div class="col-md-2 has_calendar calendar_icon">
            <input type="text" id="search_age" class="form-control search_child_age" name="search_age" placeholder="Child's Age" value="" autocomplete="off">
        </div>-->
		<div class="col-md-2  dropdown has-child-icon has_calendar calendar_icon">
            <!--<input type="text" id="search_age" class="form-control search_age" name="search_age" placeholder="Child's Age" value="" autocomplete="off">-->
			<div class=" form-control search_age">
                <div data-toggle="dropdown" id="search_age" class="filter_menu_trigger" data-value=""> <span class="selected-agerange" style="padding-left: 30px;">Child's Age eg: 3 and 5</span> <span class="caret caret_filter"></span> </div>
				<span class="child_interest_close">X</span>
                <input required type="hidden" name="search_age" id="search_age" value="">
                <ul class="dropdown-menu filter_menu age-range-dropdown" role="menu" aria-labelledby="search_age"> <?php echo wpestate_get_agerange_select_list(); ?>
                </ul>
            </div>
        </div>
        <div class="col-md-2  dropdown has-child-icon">
            <div class=" form-control child_interest">
                <div data-toggle="dropdown" id="search_child_age" class="filter_menu_trigger" data-value="all"> <span class="selected-cat">All Types</span> <span class="caret caret_filter"></span> </div> <span class="child_interest_close">X</span>          
                <input type="hidden" name="category" id="category" value="">
                <ul class="dropdown-menu filter_menu cat-dropdown" role="menu" aria-labelledby="category"> <?php echo wpestate_get_category_select_list(wpestate_get_select_arguments()); ?>
                </ul>
            </div> 
        </div>
                
        <div class="col-md-2">
            <input name="submit" type="submit" class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="advanced_submit_2_mobile" value="<?php esc_html_e('Search','wpestate');?>">
        </div>
    </form>
</div>  
<?php get_template_part('libs/internal_autocomplete_wpestate'); ?>