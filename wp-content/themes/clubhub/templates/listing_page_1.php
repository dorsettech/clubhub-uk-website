<?php
global $post;
global $current_user;
global $feature_list_array;
global $propid ;
global $post_attachments;
global $options;
global $where_currency;
global $property_description_text;     
global $property_details_text;
global $property_features_text;
global $property_adr_text;  
global $property_price_text;   
global $property_pictures_text;    
global $propid;
global $gmap_lat;  
global $gmap_long;
global $unit;
global $currency;
global $use_floor_plans;
global $favorite_text;
global $favorite_class;
global $property_action_terms_icon;
global $property_action;
global $property_category_terms_icon;
global $property_category;
global $guests; 
global $bedrooms;
global $bathrooms;
global $show_sim_two;

$price              =   floatval   ( get_post_meta($post->ID, 'property_price', true) );
$price_label        =   esc_html ( get_post_meta($post->ID, 'property_label', true) );  
$property_city      =   get_the_term_list($post->ID, 'property_city', '', ', ', '') ;
$property_area      =   get_the_term_list($post->ID, 'property_area', '', ', ', '');

$post_id=$post->ID; 
$tday = date("Y-m-d");

$guest_list= wpestate_get_guest_dropdown('noany');

$packagecalendar = false; //calendar package

$calraw = get_post_meta($post_id,'calendar-expires',true);
if($calraw != 0 && $calraw != '' && sizeof($calraw)> 0){
    if($tday < $calraw){ 
    $packagecalendar = true; //calendar package
    } 
}

if($post_id == '75548') {
    $packagecalendar = true;
}


?>
<?php
    $website_url   = esc_html(get_post_meta($post_id, 'website_url', true) );
	if($website_url != '') {
		$website_url = empty(parse_url($website_url)['scheme']) ? 'http://' . ltrim($website_url, '/') : $website_url;
	}
?>

   

<div class="row content-fixed-listing listing_type_1">
    <?php //get_template_part('templates/breadcrumbs'); ?>
    <div class=" 
        <?php
        if($options['sidebar_class']=='' || $options['sidebar_class']=='none' ){
            print ' col-md-4 '; 
        }else{
            print $options['sidebar_class'];
        }
        ?> 
        widget-area-sidebar listingsidebar2 listing_type_1 order-last" id="primary" >
        
            <div class="booking_form_request" id="booking_form_request">
                <div class="third-form-wrapper" style="border-top:0;margin-top: 0;">
                
                    <?php $property_phone   = esc_html(get_post_meta($post_id, 'clubphone', true) ); ?>
                    <?php $property_email   = esc_html(get_post_meta($post_id, 'bestemail', true) ); ?>
                    <?php $facebook_url   = esc_html(get_post_meta($post_id, 'facebook_url', true) ); ?>
                    <?php $twitter_url   = esc_html(get_post_meta($post_id, 'twitter_url', true) ); ?>
                    <?php $instagram_url   = esc_html(get_post_meta($post_id, 'instagram_url', true) ); ?>
                    <?php $showweblink   = esc_html(get_post_meta($post_id, 'prop_weblink', true) );?>
                    
                    <?php if($showweblink == 1 || $packagecalendar) { ?>
                        <div class="listing_detail websiteexlink col-md-12"><a id="extemaillink" class="button" href="<?= $website_url ?>">Book Now</a></div>
                    <?php } ?>
                          
                    <?php if($property_email != ''){ ?><div class="listing_detail listexlink col-md-12"><a class="property-email button" id="propertyEmail" onClick="return trackForGoogleGoal('<?php echo esc_attr(get_the_title()) ?>');" href="mailto:<?php echo $property_email; ?>?subject=Email Enquiry through Club Hub">Email</a></div><?php } ?>
                    
                    
				
                    <div class="reservation_buttons col-md-12">
						<div id="more_contact_details" class="col-md-6 contact_host button"  data-postid="<?php the_ID();?>">
				        <?php esc_html_e('More Contact Details','wpestate');?>
				        </div>
                    </div>
                    
                    <script>
                        jQuery(function($) {
                            $("#more_contact_details").click(function() {
                                $("#TableData").toggleClass("hidden");
                            });
                        });
                    </script>

                        <div class="contact-wrapper">
                            
                            <div id="TableData" class="hidden">
                                <div class="reservation_buttons col-md-12">
                                    <div id="add_favorites" class="button <?php print $favorite_class;?>" data-postid="<?php the_ID();?>">
                                        <?php echo $favorite_text;?>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top:5px;">
                                <?php if($property_phone != ''){ ?><div class="listing_detail listexlink col-md-12"><a class="property-phone button" onClick="return trackForGoogleGoal('<?php echo esc_attr(get_the_title()) ?>');" href="tel:<?php echo $property_phone ?>">Phone</a></div><?php } ?>
                                <div class="reservation_buttons">
                                    <div id="contact_host" class="col-md-6 contact_host button"  data-postid="<?php the_ID();?>">
                                    <?php esc_html_e('More Details Please','wpestate');?>
                                    </div>
                                </div>
                                <!-- start new form -->
                                <div id="extemailform" style="display:none;">
                                    <div id="booking_form_request_mess"></div>

                                    <input type="hidden" id="listing_edit" name="listing_edit" value="<?php echo $post_id;?>" />
                                    <input type="text" class="hidden" value="" id="formwebsitelink" name="formwebsitelink" data-link="<?php echo $website_url; ?>" />

                                    <div class="submit_booking_front_wrapper">
                                      <input type="text" placeholder="Enter your email address" id="emailu1" name="emailu1" value="" required/>
                                      <input type="text" placeholder="Confirm your email address" id="emailu2" name="emailu2" value="" required/>
                                      <input type="checkbox" id="perm" name="cotact_owner_permission" style="
            width: 18px !important;
            height: 18px !important;
            background: none !important;
            border: 3px #666 solid !important;
            padding: 0;
            margin-top: 3px;
            margin-right: 12px;
            " required><span class="enqperms">I give permission for the club owner to contact me.</span>
                                        <input type="submit" id="submit_booking_front" class="wpb_btn-small wpestate_vc_button submit_booking_front vc_button button" value="<?php esc_html_e('Continue to Book Now','wpestate');?>" />
                                        <?php wp_nonce_field( 'booking_ajax_nonce', 'security-register-booking_front' );?>
                                    </div>
                                </div>
                                <!-- end new form -->
                                    <div class="socialcon">
                                <?php if($facebook_url != ''){
                                     $facebook_url = empty(parse_url($facebook_url)['scheme']) ? 'http://' . ltrim($facebook_url, '/') : $facebook_url;
                                 ?><a id="facebook" onClick="return trackForGoogleGoal('<?php echo esc_attr(get_the_title()) ?>');" href="#booking_form_request" data-link="<?php echo $facebook_url; ?>"class="listesoc"><i class="clubthumblink fa fa-facebook fa-2"></i></a><?php } ?>
                                <?php if($twitter_url != ''){
                                     $twitter_url = empty(parse_url($twitter_url)['scheme']) ? 'http://' . ltrim($twitter_url, '/') : $twitter_url;
                                    ?><a id="twitter" onClick="return trackForGoogleGoal('<?php echo esc_attr(get_the_title()) ?>');" href="#booking_form_request" data-link="<?php echo $twitter_url; ?>" class="listesoc"><i class="clubthumblink fa fa-twitter fa-2"></i></a><?php } ?>
                                <?php if($instagram_url != ''){
                                     $instagram_url = empty(parse_url($instagram_url)['scheme']) ? 'http://' . ltrim($instagram_url, '/') : $instagram_url;
                                 ?><a id="instagram" onClick="return trackForGoogleGoal('<?php echo esc_attr(get_the_title()) ?>');" href="#booking_form_request" data-link="<?php echo $instagram_url; ?>" class="listesoc"><i class="clubthumblink fa fa-instagram fa-2"></i></a><?php } ?> 
                                    </div>
                                </div>
                            </div>
                        </div>
					<script type="text/javascript">
                        jQuery(function($) {
                            $('#extemaillink, #extemaillink_bottom').click(function(e){
                                e.preventDefault();
                                $(".pre_loader_wrap").prepend("<p>Thank you for using Club Hub. We are now directing you to the providers website for you to book.</p>");
                                $(".pre_loader_wrap").css({'display': 'flex'});
                                trackForGoogleGoal('<?php echo esc_attr(get_the_title()) ?>');
                                setTimeout(function() {
                                    window.location = $('#extemaillink').attr('href');
                                },1000);
                            });
                            $('#booking_form_request').on('click', '.expander', function(){
                                $('#TableData').toggle();
                            });
                        });
                    </script>
                   
                </div>
                
                <?php 
                if (has_post_thumbnail()){
                    $pinterest = wp_get_attachment_image_src(get_post_thumbnail_id(),'wpestate_property_full_map');
                }
                ?>  
                <?php $property_trust   = esc_html(get_post_meta($post_id, 'trustist_code', true) );?>
                <script src="<?php echo $property_trust; ?>"></script>
                <div ts-widget="" ts-border-radius="15px" ts-suppress-review-link="true"></div>
        </div>
        <div class="clubhubuk_advert">
            <?php
            $which = rand(1,2);
                if ($which == 1) {
                    echo do_shortcode( '[bsa_pro_ad_space id=1]');
                } else {
                ?>
                    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-5819193604272633"
                         crossorigin="anonymous"></script>
                    <!-- ADPRO Space -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-5819193604272633"
                         data-ad-slot="3943904546"
                         data-ad-format="auto"
                         data-full-width-responsive="true"></ins>
                    <script>
                         (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                <?php
                }
            ?>
        </div>
    </div>
    <div class=" <?php 
    if ( $options['content_class']=='col-md-12' || $options['content_class']=='none'){
        print 'col-md-8';
    }else{
       print  $options['content_class']; 
    }?> order-first">
    
        <?php get_template_part('templates/ajax_container'); ?>
        <?php
        while (have_posts()) : the_post();
            $image_id       =   get_post_thumbnail_id();
            $image_url      =   wp_get_attachment_image_src($image_id, 'wpestate_property_full_map');
            $full_img       =   wp_get_attachment_image_src($image_id, 'full');
            $image_url      =   $image_url[0];
            $full_img       =   $full_img [0];     
        ?>
        
     
    <div class="single-content listing-content">
        <h1 class="entry-title entry-prop"><?php the_title(); ?>        
            <span class="property_ratings">
                <?php 
                $args = array(
                    'number' => '15',
                    'post_id' => $post->ID, // use post_id, not post_ID
                );
                $comments   =   get_comments($args);
                $coments_no =   0;
                $stars_total=   0;

                foreach($comments as $comment) :
                    $coments_no++;
                    $rating= get_comment_meta( $comment->comment_ID , 'review_stars', true );
                    $stars_total+=$rating;
                endforeach;

                if($stars_total!= 0 && $coments_no!=0){
                    $list_rating= ceil($stars_total/$coments_no);
                    $counter=0; 
                    while($counter<5){
                        $counter++;
                        if( $counter<=$list_rating ){
                            print '<i class="fa fa-star"></i>';
                        }else{
                            print '<i class="fa fa-star-o"></i>'; 
                        }

                    }
                    print '<span class="rating_no">('.$coments_no.')</span>';
                }  
                ?>         
            </span> 
        </h1>
       
        <div class="listing_main_image_location">
            <?php //print  $property_city.', '.$property_area;
                print  $property_city;

             ?>        
        </div>   


        <div class="panel-wrapper imagebody_wrapper">
            <div class="panel-body imagebody imagebody_new">
                <?php  
                get_template_part('templates/property_pictures3');
                ?>
            </div> 
        </div>

        
        
        <div class="category_wrapper ">
            <div class="category_details_wrapper">
                <?php if( $property_action!='') {
                    echo $property_action; ?> <span class="property_header_separator">|</span>
                <?php } ?>
                
                <?php  if( $property_category!='') {
                    echo $property_category;?> <span class="property_header_separator">|</span> 
                <?php } ?> 
                    
                <?php print '<span class="no_link_details">Max Participants: '.$guests.'</span>';

                $minage = get_post_meta($post_id, 'minimum_age', true);
                $maxage = get_post_meta($post_id, 'maximum_age', true);
                $dayslist = get_post_meta($post_id, 'dayheld', true);
                $timelist = get_post_meta($post_id, 'timheld', true);

                
                /*
                .$bedrooms.' '.esc_html__( 'Bedrooms','wpestate').'</span>';?><span class="property_header_separator">|</span>
                <?php print '<span class="no_link_details">'.$bathrooms.' '.esc_html__( 'Baths','wpestate').'</span>'; */?>
            </div>
            <?php if($packagecalendar){
            echo '<a href="#listing_calendar" class="check_avalability">View Availability</a>';
            } ?>
        </div>
        
        
        
        <div id="listing_description"><div class="category_wrapper">
        <?php
            $content = get_the_content();
            $content = apply_filters('the_content', $content);
            $content = str_replace(']]>', ']]&gt;', $content);
            if($content!=''){   
                   
                $property_description_text =  get_option('wp_estate_property_description_text');
                if (function_exists('icl_translate') ){
                    $property_description_text     =   icl_translate('wpestate','wp_estate_property_description_text', esc_html( get_option('wp_estate_property_description_text') ) );
                }

                    
                print '<h4 class="panel-title-description">At a Glance</h4>';
                print '<div class="panel-body">';
                if($dayslist != ''){    
                print '<p style="font-family: \'kidsregular\';color: #000;font-size: 1.2em;"><strong>Days:</strong> '.$dayslist.'</p>';
                }
                if($timelist != ''){    
                print '<p style="font-family: \'kidsregular\';color: #000;font-size: 1.2em;"><strong>Times:</strong> '.$timelist.'</p>';
                }
                if($minage != ''){    
                print '<p style="font-family: \'kidsregular\';color: #000;font-size: 1.2em;"><strong>Age Range:</strong> '.$minage.' - '.$maxage.'</p>';
                }
                ?><p id="listing_price" style="font-family: 'kidsregular';color: #000;font-size: 1.2em;"><strong>Price:</strong> 
                <?php

                $priced = wpestate_show_price_range($post->ID,$currency,$where_currency,1); 
                if($priced != ''){
                    $pricelabel = get_post_meta($post_id,'property_price_per',true); 
                    echo $priced;
                    if($pricelabel != ''){
                        echo ' '.$pricelabel; 
                    } else {
                        echo ' per session';
                    }
                } else {
                    echo '£POA';
                    //echo '£0';
                }
            ?></p></div></div><div class="category_wrapper d-sm-none">
            <?php
                print '<h4 style="margin-top:15px;" class="panel-title-description d-sm-none">Contact Details</h4>';
                ?>
                <div style="text-align:center;">
                <div>
                    
			<div class="booking_form_request_bottom" id="booking_form_request_bottom">
					<div>
                        <?php if($showweblink == 1 || $packagecalendar){ ?>
                        <div class="websiteexlink"><a id="extemaillink_bottom" class="button" href="<?= $website_url ?>">Book Now</a></div>
                        <?php } ?>
                        <?php if($property_email != ''){ ?><div class="listexlink"><a class="property-email button" id="propertyEmail_bottom" onClick="return trackForGoogleGoal('<?php echo esc_attr(get_the_title()) ?>');" href="mailto:<?php echo $property_email; ?>?subject=Email Enquiry through Club Hub">Email</a></div><?php } ?>

						<?php $showweblink   = esc_html(get_post_meta($post_id, 'prop_weblink', true) );?>
                        </div>

						<?php $property_phone   = esc_html(get_post_meta($post_id, 'clubphone', true) ); ?>
						<?php $property_email   = esc_html(get_post_meta($post_id, 'bestemail', true) ); ?>
						<?php $facebook_url   = esc_html(get_post_meta($post_id, 'facebook_url', true) ); ?>
						<?php $twitter_url   = esc_html(get_post_meta($post_id, 'twitter_url', true) ); ?>
						<?php $instagram_url   = esc_html(get_post_meta($post_id, 'instagram_url', true) ); ?>
						<div id="more_contact_details-bot" class="col-md-6 contact_host button"  data-postid="<?php the_ID();?>">
                            <?php esc_html_e('More Contact Details','wpestate');?>
                            </div>
                        </div>

                        <script>
                            jQuery(function($) {
                                $("#more_contact_details-bot").click(function() {
                                    $("#TableData_bottom").toggleClass("hidden");
                                });
                            });
                        </script>
                    
                        <div class="contact-wrapper">
                            <div id="TableData_bottom" class="hidden">
                                <div>
                                    <div id="add_favorites_bottom" class="button <?php print $favorite_class;?>" data-postid="<?php the_ID();?>">
                                        <?php echo $favorite_text;?>
                                    </div>
                                </div>
                                <div style="margin-top:5px;">
                                <?php if($property_phone != ''){ ?><div class="listexlink"><a class="button property-phone" onClick="return trackForGoogleGoal('<?php echo esc_attr(get_the_title()) ?>');" href="tel:<?php echo $property_phone ?>">Phone</a></div><?php } ?>
                                <div class="col-md-12 reservation_buttons">
                                    <div id="contact_host_bottom" class="button col-md-6 contact_host"  data-postid="<?php the_ID();?>">
                                        <?php esc_html_e('More Details Please','wpestate');?>
                                    </div>
                                </div>
                                <div id="extemailform_bottom" style="display:none;">
                                <div id="booking_form_request_mess_b"></div>

                                    <input type="hidden" id="listing_edit" name="listing_edit" value="<?php echo $post_id;?>" />
                                    <input type="text" class="hidden" value="" id="formwebsitelinkb" name="formwebsitelink" data-link="<?php echo $website_url; ?>" />

                                    <div class="submit_booking_front_wrapper">
                                    <input type="text" placeholder="Enter your email address" id="emailb1" name="emailb1" value="" required/>
                                    <input type="text" placeholder="Confirm your email address" id="emailb2" name="emailb2" value="" required/>
                                    <input type="text" value="" class="hidden" id="type" name="type"/>
                                    <input type="checkbox" id="permb" name="cotact_owner_permission" style="
                                        width: 18px !important;
                                        height: 18px !important;
                                        background: none !important;
                                        border: 3px #666 solid !important;
                                        padding: 0;
                                        margin-top: 3px;
                                        margin-right: 12px;
                                    " required>
                                    <span class="enqperms">I give permission for the club owner to contact me.</span>
                                    <input type="submit" id="submit_booking_bottom" class="wpb_btn-small wpestate_vc_button submit_booking_front vc_button button" value="<?php esc_html_e('Continue to Book Now','wpestate');?>" />
                                    <?php wp_nonce_field( 'booking_ajax_nonce', 'security-register-booking_front' );?>
                                </div>
                                </div>

                                <!-- end new form -->
                                    <div class="socialcon">
                                <?php if($facebook_url != ''){
                                     $facebook_url = empty(parse_url($facebook_url)['scheme']) ? 'http://' . ltrim($facebook_url, '/') : $facebook_url;
                                 ?><a id="facebookb"onClick="return trackForGoogleGoal('<?php echo esc_attr(get_the_title()) ?>');" href="#booking_form_request_bottom" data-link="<?php echo $facebook_url; ?>" class="listesoc"><i class="clubthumblink fa fa-facebook fa-2"></i></a><?php } ?>
                                <?php if($twitter_url != ''){
                                     $twitter_url = empty(parse_url($twitter_url)['scheme']) ? 'http://' . ltrim($twitter_url, '/') : $twitter_url;
                                    ?><a id="twitterb" onClick="return trackForGoogleGoal('<?php echo esc_attr(get_the_title()) ?>');" href="#booking_form_request_bottom" data-link="<?php echo $twitter_url; ?>" class="listesoc"><i class="clubthumblink fa fa-twitter fa-2"></i></a><?php } ?>
                                <?php if($instagram_url != ''){
                                     $instagram_url = empty(parse_url($instagram_url)['scheme']) ? 'http://' . ltrim($instagram_url, '/') : $instagram_url;
                                 ?><a id="instagramb" onClick="return trackForGoogleGoal('<?php echo esc_attr(get_the_title()) ?>');" href="#booking_form_request_bottom" data-link="<?php echo $instagram_url; ?>" class="listesoc"><i class="clubthumblink fa fa-instagram fa-2"></i></a><?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
						<script type="text/javascript">
                            //handle different button presses for contact form DT 11/03/2021
                            function changeForm(type, buttontext) {
                                jQuery('#submit_booking_bottom').val(buttontext);
                                jQuery('#submit_booking_front').val(buttontext);
                                jQuery('#type').val(type);
                                jQuery('#extemailform_bottom').show();
                                jQuery('#extemailform').show();
                            }
                            //pass data-link from social media link to owner_insert_book()
                            function socialLink(social) {
                                $link = jQuery(social).data('link');
                                jQuery('#formwebsitelink').val($link);
                                jQuery('#formwebsitelinkb').val($link);
                            }
                            
                            jQuery('#facebookb').click(function(){
                                changeForm("facebook", "Continue to Facebook");
                                socialLink("#facebook");
                            });
                            
                            jQuery('#facebook').click(function(){
                                changeForm("facebook", "Continue to Facebook");
                                socialLink("#facebook");
                            });
                            
                            jQuery('#twitterb').click(function(){
                                changeForm("twitter", "Continue to Twitter");
                                socialLink("#twitter");
                            });
                            
                            jQuery('#twitter').click(function(){
                                changeForm("twitter", "Continue to Twitter");
                                socialLink("#twitter");
                            });
                            
                            jQuery('#instagramb').click(function(){
                                changeForm("instagram", "Continue to Instagram");
                                socialLink("#instagram");
                            });
                            
                            jQuery('#instagram').click(function(){
                                changeForm("instagram", "Continue to Instagram");
                                socialLink("#instagram");
                            });
                            
				            
                            jQuery(function(){
                                jQuery('#booking_form_request_bottom').on('click', '.expander', function(){
								    jQuery('#TableData_bottom').toggle();
								});
				            });
				        </script>

                    </div><div style="margin-top:15px" ts-widget="" ts-border-radius="15px" ts-suppress-review-link="true"></div></div></div>
                <?php
                print '<h4 id="listing_details" style="margin-top:15px;" class="panel-title-description">'.$property_description_text.'</h4>';          
                print $content.'</div>';
            }
        ?>
        </div>
            <div id="view_more_desc"><?php esc_html_e('View more','wpestate');?></div> 
        
        <!-- Features and Amenities -->
        <div class="panel-wrapper features_wrapper">
            <?php 

            if ( count( $feature_list_array )!=0 && !count( $feature_list_array )!=1 ){ //  if are features and ammenties
                    print '<a class="panel-title" id="listing_ammenities" data-toggle="collapse" data-parent="#accordion_prop_addr" href="#collapseFour"><span class="panel-title-arrow"></span>'.esc_html__( 'Benefits', 'wpestate').'</a>';
                  
                ?>
                <div id="collapseFour" class="panel-collapse collapse in">
                    <div class="panel-body panel-body-border">
                        <?php print estate_listing_features($post->ID); ?>
                    </div>
                </div>
            <?php
            } // end if are features and ammenties
            ?>
        </div>
        
         
      
        <?php
        endwhile; // end of the loop
        $show_compare=1;
        ?>
        
        
        
      
        <?php     get_template_part ('/templates/listing_reviews'); ?>

    
        
       
        
    
        
        </div><!-- end single content -->
        <div class="panel-wrapper">
            <!-- property address   -->             
            <a class="panel-title" data-toggle="collapse" data-parent="#accordion_prop_addr" href="#collapseTwo">  <span class="panel-title-arrow"></span>
                <?php esc_html_e('Activity Location','wpestate');
                                ?>
            </a>    
            <div id="collapseTwo" class="panel-collapse collapse in">
                <div class="panel-body panel-body-border">
                    <?php print estate_listing_address($post->ID); ?>
                </div>
            </div>
        </div>

        <div class="property_page_container"> 
            <h3 class="panel-title" id="on_the_map"><?php esc_html_e('On the Map','wpestate');?></h3>
            <div class="google_map_on_list_wrapper">            
                <div id="gmapzoomplus"></div>
                <div id="gmapzoomminus"></div>
                <div id="gmapstreet"></div>

                <div id="google_map_on_list" 
                    data-cur_lat="<?php   echo $gmap_lat;?>" 
                    data-cur_long="<?php echo $gmap_long ?>" 
                    data-post_id="<?php echo $post->ID; ?>">
                </div>
            </div>    
        </div>            
         <?php if($packagecalendar){ ?>
        <div class="property_page_container ">
            <?php
            get_template_part ('/templates/show_avalability');
            wp_reset_query();
            ?>  
        </div>    
        <?php } ?> 
		
		
		<!-- repeating contact options by gd -->
			
					
					<?php 
					if (has_post_thumbnail()){
						$pinterest = wp_get_attachment_image_src(get_post_thumbnail_id(),'wpestate_property_full_map');
					}
					?>

					<div class="prop_social">
						<span class="prop_social_share"><?php esc_html_e('Share','wpestate');?></span>
						<a onClick="return trackForGoogleGoal('<?php echo esc_attr(get_the_title()) ?>');" href="http://www.facebook.com/sharer.php?u=<?php echo esc_url(get_permalink()); ?>&amp;t=<?php echo urlencode(get_the_title()); ?>" target="_blank" class="share_facebook"><i class="fa fa-facebook fa-2"></i></a>
						<a onClick="return trackForGoogleGoal('<?php echo esc_attr(get_the_title()) ?>');" href="http://twitter.com/home?status=<?php echo urlencode(get_the_title() .' '.esc_url( get_permalink()) ); ?>" class="share_tweet" target="_blank"><i class="fa fa-twitter fa-2"></i></a> 
						<?php if (isset($pinterest[0])){ ?>
							<a onClick="return trackForGoogleGoal('<?php echo esc_attr(get_the_title()) ?>');" href="http://pinterest.com/pin/create/button/?url=<?php echo esc_url(get_permalink()); ?>&amp;media=<?php echo $pinterest[0];?>&amp;description=<?php echo urlencode(get_the_title()); ?>" target="_blank" class="share_pinterest"> <i class="fa fa-pinterest fa-2"></i> </a>      
						<?php } ?>           
					</div>             

			</div>
			<div class="panel-wrapper">
            <?php
                //shortcode to report club
                //update 11-02-2020
                //position moved 12-03-2020
                echo do_shortcode( '[clubhub_broken_club]');
            ?>
        </div>
		</div>
		<!-- end of contact options -->
        
        
    </div><!-- end 8col container-->

    <div class="row">
        <div class="col-md-12">
            <?php   
                $show_sim_two=1;
                get_template_part ('/templates/similar_listings');
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="clubhubuk_advert_mobile">
                <?php 
                    echo do_shortcode( '[bsa_pro_ad_space id=1]');        
                ?>
            </div>
        </div>
    </div>
    
    
<?php get_footer(); ?>