<?php
global $curent_fav;
global $show_remove_fav;
global $options;
global $isdashabord;
global $align;
global $align_class;
global $is_shortcode;
global $is_widget;
global $row_number_col;
global $full_page;
global $listing_type;
global $property_unit_slider;
global $post;
//  global $distancearr;
//console_log($post);
if (isset($post->ID)) {
    $home = home_url();
    $post_id = $post->ID;
    $previe = '';
    $extra = '';

    $col_class = 'col-md-6';
    $col_org = 4;
    $title = $post->post_title;

    $req_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    // echo "--> ".$req_url. "--- ".home_url('/');
    if ( $req_url == home_url('/') ) {
        $col_class = 'col-md-3';
    }

    if (isset($is_shortcode) && $is_shortcode == 1) {
        $col_class = 'col-md-' . $row_number_col . ' shortcode-col';
    }

    if (isset($is_widget) && $is_widget == 1) {
        $col_class = 'col-md-12';
        $col_org = 12;
    }

    if (isset($full_page) && $full_page == 1) {
        $col_class = 'col-md-4 ';
        $col_org = 3;
        if (isset($is_shortcode) && $is_shortcode == 1 && $row_number_col == '') {
            $col_class = 'col-md-' . $row_number_col . ' shortcode-col';
        }
    }

    $link = esc_url(get_permalink());
    $preview = array();
    $preview[0] = '';
    $favorite_class = 'icon-fav-off';
    $fav_mes = esc_html__('add favourite', 'wpestate');
    if ($curent_fav) {
        if (in_array($post_id, $curent_fav)) {
            $favorite_class = 'icon-fav-on';
            $fav_mes = esc_html__('remove favourite', 'wpestate');
        }
    }

    $listing_type_class = 'property_unit_v2';
    if ($listing_type == 1) {
        $listing_type_class = '';
    }

//David Distance Display from cookies on home page for example where no location search has been made
 if ( empty($post->distance) ) {
    $currentLat = '';
    $currentLng = '';

    if ( isset($_COOKIE['lat']) && !isset($_REQUEST['advanced_lat']) ) {
        $currentLat = $_COOKIE['lat'];
    } else if ( isset( $_REQUEST['advanced_lat']) ) {
        $currentLat = $_REQUEST['advanced_lat'];
    }
    if ( isset($_COOKIE['lng']) && !isset($_REQUEST['advanced_lng']) ) {
        $currentLng = $_COOKIE['lng'];
    } else if ( isset( $_REQUEST['advanced_lng'] ) ) {
        $currentLng = $_REQUEST['advanced_lng'];
    }

    $propertyLatitude = $post->property_latitude;
    $propertyLongitude = $post->property_longitude;

    if (!empty($currentLat) && !empty($currentLng) ){
        $distance = getDistanceRadius($propertyLatitude, $propertyLongitude, $currentLat, $currentLng);
    }
 } else {
    $distance = round($post->distance, 1);
}

//age range added by gd
    $minage = $post->minimum_age;
    $maxage = $post->maximum_age;
    ?>



    <div class="listing_wrapper <?php echo $col_class . ' ' . $listing_type_class; ?> ssx property_flex " data-org="<?php echo $col_org; ?>" data-listid="<?php echo $post_id; ?>" data-sort="<?php echo $distance; ?>" >
        <div class="property_listing " data-link="<?php echo $link; ?>">
            <?php
            $preview = wp_get_attachment_image_src(get_post_thumbnail_id(), 'wpestate_property_listings');
            $extra = array(
                'data-original' => ( isset($preview[0]) && !empty($preview[0]) ) ? $preview[0] : '',
                'class' => 'b-lazy img-responsive',
            );

            if (isset($preview[0]) && !empty($preview[0])) {
                $thumb_prop = '<img src="' . $preview[0] . '"   class="b-lazy img-responsive wp-post-image lazy-hidden" alt="' . $title . '" />';
                $prop_featured_img_url = $preview[0];
            } else {
                $thumb_prop_default = get_template_directory_uri() . '/img/defaultimage_prop.jpg';
                $prop_featured_img_url = $thumb_prop_default;
                $thumb_prop = '<img src="' . $thumb_prop_default . '" class="b-lazy img-responsive wp-post-image  lazy-hidden" alt="Kids Activity" />';
            }


            $featured = intval($post->prop_featured);
            $localman = intval($post->prop_localman);

            $agent_id = wpsestate_get_author($post_id);
            $thumb_id_agent = get_post_thumbnail_id($agent_id);
            $preview_agent = wp_get_attachment_image_src($thumb_id_agent, 'wpestate_user_thumb');
            $preview_agent_img = (isset($preview_agent[0]) && !empty($preview_agent[0]) ) ? $preview_agent[0] : '';

            if ($preview_agent_img == '') {
                $preview_agent_img = get_template_directory_uri() . '/img/default_user_small.png';
            }

            $property_city = get_the_term_list($post_id, 'property_city', '', ', ', '');
            $property_area = get_the_term_list($post_id, 'property_area', '', ', ', '');
            ?>


            <div class="listing-unit-img-wrapper">
                <?php
                if ($property_unit_slider == 'yes') {
                    //slider
                    $arguments = array(
                        'numberposts' => 2,
                        'post_type' => 'attachment',
                        'post_mime_type' => 'image',
                        'post_parent' => $post_id,
                        'post_status' => null,
                        'exclude' => get_post_thumbnail_id(),
                        'orderby' => 'rand',
                        'order' => 'ASC'
                    );
                    $post_attachments = get_posts($arguments);

                    $slides = '';

                    $no_slides = 0;
                    //foreach ($post_attachments as $attachment) {
                    $no_slides++;
                    $preview = wp_get_attachment_image_src($attachment[0]->ID, 'wpestate_property_listings');
                    $slides .= '<div class="item lazy-load-item">
                                            <a href="' . $link . '"><img loading="lazy" data-lazy-load-src="' . $preview[0] . '" alt="' . $title . '2" class="img-responsive" /></a>
                                        </div>';

                    // }// end foreach
                    $unique_prop_id = uniqid();
                    print '
                    <div id="property_unit_carousel_' . $unique_prop_id . '" class="carousel property_unit_carousel slide  " data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">
                            <div class="item active">
                                <a href="' . $link . '">' . $thumb_prop . '</a>
                            </div>';

                    print $slides . '
                        </div>




                    <a href="' . $link . '"> </a>';

                    if ($no_slides > 0) {
                        print '<a class="left  carousel-control" href="#property_unit_carousel_' . $unique_prop_id . '" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>

                        <a class="right  carousel-control" href="#property_unit_carousel_' . $unique_prop_id . '" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>';
                    }
                    print'</div>';
                } else {
                    ?>
                    <a href="<?php echo $link; ?>">
                        <div class="property_hero_thumb">
                            <img loading="lazy" src="<?php echo $prop_featured_img_url; ?>">
                            <div class="cross"></div>
                        </div>
                    </a>
                <?php } ?>

            <!-- after thumb logo -->
            <?php
            $user_custom_picture = '';
            $prop_user_id = $post->post_author;
            $user_custom_picture = get_the_author_meta( 'custom_picture' , $prop_user_id );

            if ( filter_var($user_custom_picture, FILTER_VALIDATE_URL) === FALSE ) {
                //if ( get_headers($user_custom_picture)[0] != 'HTTP/1.1 200 OK' ) { //real slow DT better to correct data when it is set than handle it in the loop can be used as one time to replace missing profile images
                $logoimg = get_template_directory_uri().'/img/default_user.png';
                update_user_meta($prop_user_id, 'custom_picture', $logoimg );
            } else {
                $logoimg = $user_custom_picture;
            }
            //}?>

            <div class="property-logo" style="background-image: url('<?php echo $logoimg; ?>');"> </div>
            <!-- end after thumb logo -->
            </div>

            <?php
            if ($featured == 1 || $localman == 1) {
                print '<div class="featured_div">' . esc_html__('Featured', 'wpestate') . '</div>';
            }
            ?>

            <div class="title-container">

                <div class="category_name">
                    <a href="<?php echo $link; ?>" class="listing_title_unit">

                        <?php

                        echo mb_substr(html_entity_decode($title), 0, 26, "UTF8");
                        if (strlen($title) > 20) {
                            echo '...';
                        }
                        ?></a>

                    <div class="category_tagline">
                        <img loading="lazy" src="<?php echo $home; ?>/wp-content/uploads/mappin.svg"  alt="club type">
                        <?php
                            if ($property_area != '') {
                                echo $property_area , ', ';
                            }
                            echo $property_city;
                        ?>
                    </div>

                    <div class="category_tagline">
                        <img loading="lazy" src="<?php echo $home; ?>/wp-content/uploads/cal.svg"  alt="club type"><span>
                            <?php
                            $dayslist = $post->dayheld;
                            $timelist = $post->timheld;
                            if ($dayslist != '') {
                                echo $dayslist;
                            }
                            if ($timelist != '') {
                                echo '<br/>' , $timelist;
                            }
                            ?></span>
                    </div>


                    <?php if (!empty($minage) && !empty($maxage)): ?>
                        <div class="category_tagline">
                            <img loading="lazy" src="<?php echo $home; ?>/wp-content/uploads/baby.svg"  alt="club type">
                            <span>
                                <?php
                                echo 'Age Range: ' , $minage , '-' , $maxage;
                                ?>
                            </span>
                        </div>
                    <?php endif; ?>
                    <?php if(isset($distance) ) { ?>
                    <div class="category_tagline">
                        <img loading="lazy" src="<?php echo get_template_directory_uri(); ?>/css/css-images/location_icon.png"  alt="club type"><span style="vertical-align: -8px;">
                            <?php echo $distance , ' miles';
                            ?></span>
                    </div>
                    <?php  }   ?>

                </div>
                <a href="#" class="clubthumblink">Find Out More</a>
                <div class="property_unit_action">
                    <span class="icon-fav <?php echo $favorite_class; ?>" data-original-title="<?php echo $fav_mes; ?>" data-postid="<?php echo $post_id; ?>"><img class="favheartimg" src="<?php echo $home; ?>/wp-content/uploads/wish.svg" /></span>
                </div>
            </div>


            <?php
            if (isset($show_remove_fav) && $show_remove_fav == 1) {
                print '<span class="icon-fav icon-fav-on-remove" data-postid="' . $post_id . '"> ' . $fav_mes . '</span>';
            }
            ?>

        </div>
    </div>
    <?php
}
