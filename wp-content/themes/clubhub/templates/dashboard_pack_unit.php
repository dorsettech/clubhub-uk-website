<?php
global $currency; 


$postid             = $post->ID;
$pack_list          = get_post_meta($postid, 'pack_listings', true);
$pack_featured      = get_post_meta($postid, 'pack_featured_listings', true);
$pack_featured_local      = get_post_meta($postid, 'local_featured', true);
$pack_price         = get_post_meta($postid, 'pack_price', true);
$unlimited_lists    = get_post_meta($postid, 'mem_list_unl', true);
$biling_period      = esc_html(get_post_meta($postid, 'biling_period', true));
$billing_freq       = get_post_meta($postid, 'billing_freq', true); 
$pack_time          = get_post_meta($postid, 'pack_time', true);
$unlimited_listings = get_post_meta($postid,  'mem_list_unl',true);
$pack_desc          = get_post_meta($postid, 'pack_desc',true);

if($billing_freq>1){
    $biling_period.='s'; 
}
?>

<div class="col-md-3"> 
    <div class="user_dashboard_panel pack_unit_list">
        <h4 class="user_dashboard_panel_title">
           <?php $packtitle = get_the_title();
           echo $packtitle; ?>
        </h4>    
        <?php if($pack_price > 0.01){
            echo '<p id="packPrice">&pound;'.$pack_price.'</p>';
        } else {
            echo '<p id="packPrice">&pound;0</p>';
        } ?>
        <div class="pack-listing-period">
            <?php echo $billing_freq.' '. wpestate_show_bill_period($biling_period); ?>
        </div>

        <?php
        if($unlimited_listings==1 && $packtitle != 'Calendar Booking System'){
            print'<div class="pack-listing-period">Unlimited Club Listings</div>';
        }else{
            /*if($pack_list > 0){
            print'<div class="pack-listing-period">'.$pack_list.' '.esc_html__( 'Listings','wpestate').' </div>';    
            }*/
        }
        ?>
        
        <?php
        if($packtitle == "Discounted Membership" || $packtitle == "New Membership" || $packtitle == "Monthly Membership"){
            print'<div class="pack-listing-period">';
            echo $pack_desc;
            print'</div>';
        }
        ?>
        
        <?php if($pack_featured > 0){ ?>
    <div class="pack-listing-period">
        <?php echo 'Club Featured on the Homepage and Search Results'; 
        //echo $pack_featured.' Featured Club';?> 
    </div> 
    <?php } ?>
    <?php if($pack_featured_local > 0){ ?>
    <div class="pack-listing-period">
        <?php echo 'Club Featured on the Local Search Results'; 
        //echo $pack_featured.' Featured Club';?> 
    </div> 
    <?php } ?>
    </div>
</div>