<?php
// Template Name: Advanced Search Results
// Wp Estate Pack
//populateClubFields();
global $distancearr1;
$distancearr =  array();
get_header();
$current_user = wp_get_current_user();
$options        =   wpestate_page_details($post->ID);
$show_compare   =   1;
$area_array     =   '';
$agearr			=	''; 
$categ_array    =   '';  
$city_array     =   '';  
$action_array   =   '';
$categ_array    =   '';
$tax_query      =   '';
$allowed_html=array();
$compare_submit         =   wpestate_get_compare_link();
$currency               =   esc_html( get_option('wp_estate_currency_symbol', '') );
$where_currency         =   esc_html( get_option('wp_estate_where_currency_symbol', '') );
$prop_no                =   intval(get_option('wp_estate_prop_no'));
$show_compare_link      =   'yes';
$userID                 =   $current_user->ID;
$user_option            =   'favorites'.$userID;
$curent_fav             =   get_option($user_option);
$custom_advanced_search =   get_option('wp_estate_custom_advanced_search','');
$meta_query             =   array();
$no_of_meta             =   0;
$adv_search_what        =   '';
$adv_search_how         =   '';
$adv_search_label       =   '';             
$adv_search_type        =   '';  

/// !!!WARNING!!! /// 'childagesearch' = category, someone was lazy ///

echo '<span id="scrollhere"></span>';

if(isset($_GET['category']) && $_GET['category'] == '') {
	$_GET['category'] = 'all';
}
$categoryParts = explode(',',$_GET['category']); //online-classes-and-activity-boxes,science

if(count($categoryParts) > 1) {
	$_GET['category'] = $categoryParts;
}else{
	$_GET['category'] = array($_GET['category']);
}

$allowed_html   =   array();
                      
//console_log($_GET);

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

// overwriting with new Args as per phone app search algo
$args = array( 
    'post_type' => 'estate_property',
    'status' => 'publish',
    'posts_per_page' => $prop_no,
    'order' => 'ASC',
    'paged' => 1
);

if($_GET['category'] && !in_array('all',$_GET['category'])){

    $categ_array = array(
    'taxonomy'     => 'property_category',
    'field'        => 'slug',
    'terms'        => $_GET['category']
    );
    $args['tax_query'] = array(
        'relation' => 'AND',
        $categ_array,
    );
}

##### Check if Search age OR advanced lat are set #####
if($_GET['search_age'] || $_GET['advanced_lat']) {
    //child age
    if( isset($_GET['search_age']) ){
        $no_of_meta++;
        $childage = explode(',',$_GET['search_age']);
        $minage_array   = array();
        $maxage_array   = array();
        $meta_query     = array();
        sort($childage);
        $minAgeVal = (int) intval($childage[0]);
        $maxAgeVal = (int) intval(end($childage));
        $meta_query['relation'] = 'AND';
        $minage_array = array(
            'key'   => 'minimum_age',
            'value' => $minAgeVal,
            'type'  => 'NUMERIC',
            'compare' => '<='
        );
        $maxage_array = array(
            'key'   => 'maximum_age',
            'value' => $maxAgeVal,
            'type'  => 'NUMERIC',
            'compare' => '>='
        );
        $meta_query[] = $minage_array;
        $meta_query[] = $maxage_array;
        $args['meta_query'][] = $meta_query;
    }
    
    if($_GET['search_location'] != '' && $_GET['advanced_lat'] && !in_array('activity-boxes',$_GET['category']) && !in_array('online-classes',$_GET['category']) )
    {
        $lat = substr($_GET['advanced_lat'], 0, 9);
        $lng = substr($_GET['advanced_lng'], 0, 9);
        $distance = 30;

        // earth's radius in miles = ~3959
        $radius = 3959;

        // latitude boundaries
        $maxlat = substr(($lat + rad2deg($distance / $radius)), 0, 9);
        $minlat = substr(($lat - rad2deg($distance / $radius)), 0, 9);

        // longitude boundaries (longitude gets smaller when latitude increases)
        $maxlng = substr(($lng + rad2deg($distance / $radius / cos(deg2rad($lat)))), 0, 9);
        $minlng = substr(($lng - rad2deg($distance / $radius / cos(deg2rad($lat)))), 0, 9);

        $lat_lng_meta_query = array ( //not in use if geoquery in place
            'relation' => 'AND',
                array(
                    'key' => 'property_latitude',
                    'value' =>  array((float)$minlat, (float)$maxlat),
                    'compare' => 'BETWEEN',
                    'type' => 'DECIMAL'
                ),
                array(
                    'key' => 'property_longitude',
                    'value' =>  array((float)$minlng, (float)$maxlng),
                    'compare' => 'BETWEEN',
                    'type' => 'DECIMAL'
                )
        );
    }
}

###### END Check if Search age OR advanced lat are set ######
if ( class_exists("WP_GeoQuery") ) { //WIP
    $args['latitude'] = $lat;
    $args['longitude'] = $lng;
    $args['radius'] = 50;
    $prop_selection_sort = new WP_GeoQuery($args);
    $og_meta_query = $args['meta_query'];
    // Add featured up to 10 miles
    //$args['radius'] = 10;
    $args['meta_query'][] = array(
        'feature' => array (
            'key'   => 'prop_featured',
            'value' => 1
        )
    );
    $feat_listings = new WP_GeoQuery($args);
    // Add local up to 15 miles
    $args['meta_query'] = $og_meta_query;
    $args['radius'] = 15;
    $args['meta_query'][] = array(
        'feature' => array (
            'key'   => 'prop_localman',
            'value' => 1
        )
    );
    $local_listings = new WP_GeoQuery($args);

    $featured_posts = array_merge($feat_listings->posts, $local_listings->posts);
    usort( $featured_posts, function($a, $b) { //sort by distance
        if ($a->distance == $b->distance) return 0;
        return ($a->distance < $b->distance) ? -1 : 1;
    });
    
    $listings = array_merge($featured_posts, $prop_selection_sort->posts);
    
    $listings = array_unique($listings, SORT_REGULAR);
    $listings = array_values($listings);
    $prop_selection_sort->posts = $listings;
    $prop_selection_sort->post_count = count($listings);

} else {
    if ( !empty($lat_lng_meta_query) ) {
        $args['meta_query'][] = $lat_lng_meta_query;
        $no_of_meta++;
        if ($no_of_meta > 1) {
            $args['meta_query']['relation'] = 'AND';
        }
    }
    $prop_selection_sort = new WP_Query($args);
}

$prop_selection = $prop_selection_sort;       

$selected_pins  = wpestate_listing_pins($args,1,1);//call the new pins  

/////////////////////////////////// get template and display results ///////////////////////////////////////////////////////////

$property_list_type_status =  esc_html(get_option('wp_estate_property_list_type_adv',''));
if ( $property_list_type_status == 2 ){
    get_template_part('templates/half_map_core');
}else{
    get_template_part('templates/normal_map_core');
}

wp_localize_script('wpestate_googlecode_regular', 'googlecode_regular_vars2', 
    array(  
        'markers2'           =>  $selected_pins,
    )
);
get_footer(); 
?>
