(function($) {

   // we create a copy of the WP inline edit post function
   var $wp_inline_edit = inlineEditPost.edit;
   // and then we overwrite the function with our own code
   inlineEditPost.edit = function( id ) {

      // "call" the original WP edit function
      // we don't want to leave WordPress hanging
      $wp_inline_edit.apply( this, arguments );

      // now we take care of our business

      // get the post ID
      var $post_id = 0;
      if ( typeof( id ) == 'object' )
         $post_id = parseInt( this.getId( id ) );			
      if ( $post_id > 0 ) {

         // define the edit row
         var $edit_row = $( '#edit-' + $post_id );

         // get the release date
	 var $release_date = $( '#club_owner-' + $post_id ).text();

	 // populate the release date
	 $edit_row.find( 'input[name="club_owner"]' ).val( $release_date );

      }

   };

})(jQuery);

jQuery( '#bulk_edit' ).on( 'click', function(e) {		
   // define the bulk edit row
   var $bulk_row = jQuery( '#bulk-edit' );

   // get the selected post ids that are being edited
   var $post_ids = new Array();
   $bulk_row.find( '#bulk-titles' ).children().each( function() {
      $post_ids.push( jQuery( this ).attr( 'id' ).replace( /^(ttle)/i, '' ) );
   });

   // get the release date
  // var $release_date = $bulk_row.find( 'input[name="club_owner"]' ).val();
	var prop_weblink = 0;
	if($bulk_row.find( 'input:checkbox[name="prop_weblink"]' ).is(":checked")){
		prop_weblink = 1;
	}
	var prop_featured = 0;
	if($bulk_row.find( 'input:checkbox[name="prop_featured"]' ).is(":checked")){
		prop_featured = 1;
	}
	var prop_localman = 0;
	if($bulk_row.find( 'input:checkbox[name="prop_localman"]' ).is(":checked")){
		prop_localman = 1;
	}

   // save the data
   jQuery.ajax({
      url: '/wp-admin/admin-ajax.php', //ajaxurl this is a variable that WordPress has already defined for us
      type: 'POST',
     // async: false,
      cache: false,
      data: {
         action: 'dt_save_bulk_edit', // this is the name of our WP AJAX function that we'll set up next
         post_ids: $post_ids, // and these are the 2 parameters we're passing to our function
    	 prop_weblink: prop_weblink,
		 prop_featured: prop_featured,
		 prop_localman: prop_localman	,
      }/* ,
	   success: function(){
	   	jQuery("tr#bulk-edit").css('display','none');
	   } */
   });

});