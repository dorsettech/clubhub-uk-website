<?php

require_once 'ad-recurring-paypal.php';

/**
 * Removed this function getDistance() from below files and updated new function with this getDistanceRadius()
 * advanced_search_results.php Line no.: 100
 * advanced_search_results.php Line no.: 542
 * ajax_functions.php Line No.: 1321 
 */
function getDistanceRadius( $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 3959){    
    // convert from degrees to radians
    $latFrom = (float) $latitudeFrom;
    $lonFrom = (float) $longitudeFrom;
    $latTo = (float) $latitudeTo;
    $lonTo = (float) $longitudeTo;

    $latFrom = deg2rad($latFrom);
    $lonFrom = deg2rad($lonFrom);

    $latTo = deg2rad($latTo);
    $lonTo = deg2rad($lonTo);

    $lonDelta = $lonTo - $lonFrom;
    $a = pow(cos($latTo) * sin($lonDelta), 2) + pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
    $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

    $angle = atan2(sqrt($a), $b);
    //echo ' dist: '.number_format($angle * $earthRadius,1);
    return number_format($angle * $earthRadius,1);
    //return 1;
}

/* customize */
if ( !function_exists( 'ch_header_footer' ) ) {
    function ch_header_footer( $meta_value ) {
        $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => $meta_value
        ));
        $page_id = $pages[0]->ID;
        // echo "<pre>";
        // print_r($page_id);
        // echo "</pre>";

        $page_data = get_page( $page_id ); 

        //store page title and content in variables
        if ( class_exists("WPBMap") ) {
            WPBMap::addAllMappedShortcodes();
        }
        echo apply_filters('the_content', $page_data->post_content);
    }
}


add_shortcode('ch_nav_menu', 'ch_nav_menu');
function ch_nav_menu( $atts ) {

    $atts = shortcode_atts(
        array(
            'menu_name' => 'left-header-menu',
    ), $atts);
    // echo "<pre>";
    // print_r( $atts['menu_name']);
    // echo "</pre>";
    echo "<nav id='access'>".ch_get_nav_menu( $atts['menu_name'], true )."</nav>";
}


if ( !function_exists( 'ch_get_nav_menu' ) ) {
    function ch_get_nav_menu($slug='', $custom_walker=false) {
        $menu = $slug;
        $args = array(
            'menu'              => $menu,
            'container'         => '',
            'container_class'   => '',
            'container_id'      => '',
            'items_wrap'        => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'menu_class'        => (!empty($slug) ? 'menu' : 'menu'),
            'menu_id'           => (!empty($slug) ? $slug : 'menu'),
            'echo'              => false,
            'fallback_cb'       => '',
            'before'            => '',
            'after'             => '',
            'link_before'       => '',
            'link_after'        => '',
            'depth'             => 11,
            // 'walker'            => new themerex_custom_menu_walker,
            // 'theme_location'    => $slug
        );
        // if (!empty($slug))
        //     $args['theme_location'] = $slug;
        // if ($custom_walker && class_exists('themerex_custom_menu_walker') && themerex_get_theme_option('custom_menu')=='yes')
        //     $args['walker'] = new themerex_custom_menu_walker;
        return wp_nav_menu($args);
    }
}

function club_listing_count() {
    $args = array(
        'post_type' => 'estate_property',
        'post_status' => 'publish',
    );
    $club_listing_count = new WP_Query( $args );
    return number_format( $club_listing_count->found_posts );
}

add_action('wp_head', 'ch_custom_style');
function ch_custom_style() { ?>
    <style type="text/css">
        /*# Desktop*/
        @media only screen and (min-width: 799px) {

            .header_wrapper {
                display: none;
            }

        }

        /*# mobile, tab*/
        @media only screen and (max-width: 799px) {
            body  .custom-header {
                display: none;
            }
        }

        .property-logo {
            border-radius: 50%;
            /*margin: 0px auto;*/
            width: 90px;
            height: 90px;
            /*border: 3px solid #838895;*/
            background-size: cover;
            background-position: 50% 50%;
        }

        /* owl crousel dots */
         .popular-cats .owl-pagination {
            text-align: center;
         }
        .popular-cats .owl-pagination .owl-page {
                width: 12px;
                height: 12px;
                border: 1px solid;
                border-radius: 50%;
                margin-right: 5px;
                display: inline-block;
        }
        .popular-cats .owl-pagination .owl-page:last-child {
            margin-right: 0;
        }
        .popular-cats .owl-pagination .owl-page.active {
            /*color: #fbb430;*/
            border-color: #fbb430;
            background-color: #fbb430;
        }
        /* end owl crousel dots */

    </style>
<?php }


/* Popular cats crousel */
add_shortcode( 'popular_categories', 'popular_categories' );
function popular_categories( $atts ) {

    $atts = shortcode_atts(
        array(
            'per_page' => 12,
    ), $atts);

    $club_cats = get_terms(
        array(
            'taxonomy' => 'property_category',
            'hide_empty' => false,
            'number'     => $atts['per_page'],
            'meta_query' => array(
                array(
                    'key'       => 'is_popular',
                    'value'     => 1,
                    'compare' => 'LIKE',
                ),
            ),
        ),
    );
    // print_r($club_cats);
    ob_start();

    $placeholder_img = "https://placehold.jp/200x200.png";
?>

    <div class="popular-cats">
        <div class="owl-carousel owl-theme owl-loaded owl-drag">
            
            <?php foreach ($club_cats as $club_cat): ?>
            <div class="category">
                <a href="<?php echo get_term_link( $club_cat->term_id); ?>">
                    <div class="img-thumb mb-4">
                        <?php
                            $cat_img = get_term_meta( $club_cat->term_id, 'cat_image_upload', true );
                            console_log($cat_img);
                            console_log($club_cat);
                            $img_url = isset($cat_img) ? $cat_img : $placeholder_img;
                        ?>
                        <img src="<?php echo $img_url; ?>" alt="">
                    </div>
                    <div class="title">
                        <h3><?php echo $club_cat->name; ?></h3>
                    </div>
                </a>
            </div>
            <?php endforeach; ?>

        </div>
    </div>

    <script type="text/javascript">
        jQuery(function($) {
            $('.owl-carousel').owlCarousel({
                loop:true,
                responsive:{
                    0:{
                        items:1,
                        nav:false
                    },
                    600:{
                        items:3,
                        nav:false
                    },
                    1000:{
                        items:3,
                        nav:false
                    }
                }
            });
        });
    </script>

<?php

    return ob_get_clean();
}


add_shortcode( 'user_information', 'user_information' );
function user_information() {
    get_template_part('templates/top_user_menu');
}

add_shortcode( 'hand_picked_activities', 'hand_picked_activities' );
function hand_picked_activities( $atts ) {

    $atts = shortcode_atts(
        array(
            'listings_per_page' => 8,
    ), $atts);

    $compare_array = array();
    $compare_array['key']        = 'prop_featured';
    $compare_array['value']      = 1;
    $compare_array['type']       = 'numeric';
    $compare_array['compare']    = '=';
    $meta_query[]                = $compare_array;

    $args = array(
        'post_type'         => 'estate_property',
        'post_status'       => 'publish',
        'paged'             => 0,
        'posts_per_page'    => $atts['listings_per_page'],
         'orderby'           => 'meta_value_num',
         'meta_key'          => 'prop_featured',
         'order'             => 'DESC',
         'meta_query'        => array(
             array(
                 'key'       => 'prop_featured',
                 'value'     => 1,
                 'compare'   => '='
             ),
         ),
    );
 
    $listings = new WP_Query($args);

    ob_start();

    while ($listings->have_posts()): $listings->the_post();
        get_template_part('templates/property_unit');
    endwhile;

    return ob_get_clean();
}
/* end customize */


/* Trustist slider */
add_shortcode( 'trustist-slider', 'trustist_slider' );
function trustist_slider(){

    $ts_data = file_get_contents("https://widget.trustist.com/reviewlistdata?pageSize=5&businessKey=u2cuywj6&businessKey=u2cuywj6&locationKey=pq0onfrc");
    $ts = json_decode($ts_data);

    ob_start();
    // if ( get_current_user_id() == 8216) {
        // code...
?>
    
    <div class="trustist-slider">
        <?php for ($i=0; $i < sizeof($ts); $i++) : $rate =  trim($ts[$i]->reviewRating->ratingValue)/2 ?>
            <div class="item">
                <h3 style="font-size: 1.1em;">
                    <div class="logo-img">
                        <a href="<?php echo $ts[$i]->isBasedOn; ?>" rel="opener" target="_blank">
                            <img src="<?php echo $ts[$i]->image ?>" alt="image" style="width: auto; margin: 0 auto;" />
                        </a>
                    </div>
                    <div class="reviewer">
                        <p>
                            <a href="<?php echo $ts[$i]->isBasedOn; ?>" rel="opener" target="_blank" style="color: #111b47; font-weight: bold; text-decoration: none;">
                                Review By <?php echo $ts[$i]->author->name; ?> left on <?php echo date('d M Y') ?>
                            </a>
                        </p>
                        <p>
                            <a href="<?php echo $ts[$i]->isBasedOn; ?>" rel="opener" target="_blank">
                                <span class="ts-stars-val" aria-label="Rated <?php echo $rate; ?> out of 5">
                                    <?php
                                        for($j=0;$j<$rate;$j++) { ?>
                                            <span class="fa fa-star"></span>
                                        <?php }
                                    ?>
                                </span>
                            </a>
                        </p>
                    </div>
                </h3>
                <div class="ts-sliderText">
                    <?php echo $ts[$i]->reviewBody; ?>
                </div>

            </div>
        <?php endfor; ?>

    </div>

    <style type="text/css">
        .trustist-slider {
            color: #5a6a74 !important;
            border: 1px solid rgba(0, 0, 0, 0.15) !important;
            box-shadow: 2px 2px 14px rgb(0 0 0 / 25%);
            border-radius: 10px;
            padding: 5px 20px;
            height: auto;
        }
        .logo-img img { max-height: 50px; }
    </style>
    <script type="text/javascript">
         jQuery(function($) {
            $('.trustist-slider').owlCarousel({
                loop:true,
                items: 1,
                mouseDrag: true,
                nav: true
            });
        });

    </script>
<?php 
    // }
    return ob_get_clean();
}

/**
 * MANAGE RECURRING SUBSCRIPTION PACKAGE
 * @return array()
 */

function manage_subscription_type( $pp_subs_id ) {

    global $wpdb;

    // GET SUBCSCRIPTION DATA QUERY
    $pack_subscription = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}postmeta WHERE `meta_value` = %s", $pp_subs_id ), ARRAY_A );
    $subscription_type = explode('_', $pack_subscription[0]['meta_key']); 
    $subscription_type = $subscription_type[0]."_".$subscription_type[1];

    $subs_pack = array(
        'active_subscription'   => "",
        'prop_pack_key'         => "",
        'exp_key'               => "",
        'exp_value'             => "",
    );
    switch ($subscription_type) {
        case 'localboost_pack':
            $active_subscription = $subscription_type;
            $prop_pack_key = 'prop_localman';
            $exp_key       = 'local-expires';
            //$exp_key       = 'prop_localman';
            $exp_value     = strtotime("+2 weeks");
            break;

        case 'feature_pack':
            $active_subscription = $subscription_type;
            $prop_pack_key = 'prop_featured';
            $exp_key       = 'feature-expires';
            $exp_value     = strtotime("+1 month");
            break;
        
        default:
            $active_subscription = 'club';
            $prop_pack_key = 'prop_weblink';
            $exp_key       = 'weblink-expires';
            $exp_value     = strtotime("+1 year");
            break;
    }
    $exp_value = date('Y-m-d', $exp_value);
    return $subs_pack = array(
        'active_subscription'   => $active_subscription,
        'prop_pack_key'         => $prop_pack_key,
        'exp_key'               => $exp_key,
        'exp_value'             => $exp_value,
    );

}

/**
 *  SETUP SUBSCRIPTION TYPE
 * @return string
 */
function setup_subscription_type( $pack_lbl ) {
    if ( !empty($pack_lbl) && $pack_lbl == "Featured Package" ) {
        $subscription_type = 'feature_pack';
    } elseif ( !empty($pack_lbl) && $pack_lbl == "Local Boost Package" ) {
        $subscription_type = 'localboost_pack';
    } elseif ( !empty($pack_lbl) && $pack_lbl == "Local Boost Package" ) {
        $subscription_type = 'weblink_pack';
    } else {
        $subscription_type = 'club';
    }
    return $subscription_type;
}

add_action( 'wp_login_failed', 'dt_front_end_login_fail', 10, 2 );  // hook failed login

function dt_front_end_login_fail( $username, $error ) {
   $referrer = $_SERVER['HTTP_REFERER'];  // where did the post submission come from?
   // if there's a valid referrer, and it's not the default log-in screen
    $error = $error->get_error_message();
   if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
      wp_redirect( $referrer . '?error=' . $error );  // let's append some information (login=failed) to the URL for the theme to use
      exit;
   }
}

add_action( 'wp_login', 'dt_create_club_from_cookie', 999, 2);
function dt_create_club_from_cookie( $login, $user ) {
    $data = $_COOKIE['form_data'];
    if ( !empty($data) ) {
        $url = common_to_save_club($data, $user);
        unset($_COOKIE['form_data']);
        setcookie('form_data', '', time() - 3600, '/');
        header("Location: ".$url);
    }
}