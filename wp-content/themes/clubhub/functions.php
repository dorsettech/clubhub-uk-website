<?php
require_once get_template_directory() . '/libs/css_js_include.php';
require_once get_template_directory() . '/libs/metaboxes.php';
require_once get_template_directory() . '/libs/plugins.php';
require_once get_template_directory() . '/libs/help_functions.php';
require_once get_template_directory() . '/libs/pin_management.php';
require_once get_template_directory() . '/libs/ajax_functions.php';
require_once get_template_directory() . '/libs/ajax_functions_edit.php';
require_once get_template_directory() . '/libs/ajax_functions_booking.php';
require_once get_template_directory() . '/libs/ajax_upload.php';
require_once get_template_directory() . '/libs/3rdparty.php';
require_once get_template_directory() . '/libs/theme-setup.php';
require_once get_template_directory() . '/libs/general-settings.php';
require_once get_template_directory() . '/libs/listing_functions.php';
require_once get_template_directory() . '/libs/theme-slider.php';
require_once get_template_directory() . '/libs/agents.php';
require_once get_template_directory() . '/libs/invoices.php';
require_once get_template_directory() . '/libs/searches.php';
require_once get_template_directory() . '/libs/membership.php';
require_once get_template_directory() . '/libs/property.php';
require_once get_template_directory() . '/libs/booking.php';
require_once get_template_directory() . '/libs/messages.php';
require_once get_template_directory() . '/libs/shortcodes_install.php';
require_once get_template_directory() . '/libs/shortcodes.php';
require_once get_template_directory() . '/libs/widgets.php';
require_once get_template_directory() . '/libs/events.php';
require_once get_template_directory() . '/libs/icalendar.php';
require_once get_template_directory() . '/libs/dashboard_view.php';
require_once get_template_directory() . '/custom-functions.php';

function app_output_buffer() {

    ob_start();
}

add_action('init', 'app_output_buffer');
load_theme_textdomain('wpestate', get_template_directory() . '/languages');

define('ULTIMATE_NO_EDIT_PAGE_NOTICE', true);
define('ULTIMATE_NO_PLUGIN_PAGE_NOTICE', true);
# Disable check updates - 
define('BSF_6892199_CHECK_UPDATES', false);

# Disable license registration nag -
define('BSF_6892199_NAG', false);

function wpestate_admin_notice() {
    global $pagenow;
    global $typenow;

    if (!empty($_GET['post'])) {
        $allowed_html = array();
        $post = get_post(wp_kses($_GET['post'], $allowed_html));
        $typenow = $post->post_type;
    }



    //print  $pagenow.' / '.$typenow .' / '.basename( get_page_template($post) );

    /* if (is_admin() &&   $pagenow=='post.php' && $typenow=='page' && basename( get_page_template($post))=='property_list_half.php' ){
      $header_type    =   get_post_meta ( $post->ID, 'header_type', true);

      if ( $header_type != 5){
      print '<div class="error">
      <p>'.esc_html__( 'Half Map Template - make sure your page has the "media header type" set as google map ', 'wpestate' ).'</p>
      </div>';
      }

      } */

    if (is_admin() && $pagenow == 'edit-tags.php' && $typenow == 'estate_property') {

        print '<div class="error">
            <p>' . esc_html__('Please do not manually change the slugs when adding new terms. If you need to edit a term name copy the new name in the slug field also.', 'wpestate') . '</p>
        </div>';
    }


    if (is_admin() && ( $pagenow == 'post-new.php' || $pagenow == 'post.php') && $typenow == 'estate_property') {

        print '<div class="error">
            <p>' . esc_html__('Please add clubs from front end interface using an user with subscriber level registered in front end', 'wpestate') . '</p>
        </div>';
    }

    /* if(wpestate_get_ical_link()==home_url()){
      print '<div class="update-nag">
      <p>'.esc_html__( 'You need to create a page with the template ICAL FEED (if you want to use icalendar export/import feature)', 'wpestate' ).'</p>
      </div>';
      }

      if(wpestate_get_dashboard_allinone()==home_url()){
      print '<div class="update-nag">
      <p>'.esc_html__( 'You need to create a page with the template All in one calendar (if you want to use all in one calendar feature)', 'wpestate' ).'</p>
      </div>';
      }

      $current_tz= date_default_timezone_get();
      if( wpestate_isValidTimezoneId2($current_tz)!= 1 ){
      print '<div class="update-nag">
      <p>'.esc_html__( 'It looks like you may have a problem with the server date.timezone settings and may encounter errors like the one described here:', 'wpestate' ).'<a href="http://help.wprentals.org/2015/12/21/calendar-doesnt-work-calendar-issues/">http://help.wprentals.org/2015/12/21/calendar-doesnt-work-calendar-issues/</a> '.esc_html__('Please resolve these issues with your hosting provider.','wpestate').' </p>
      </div>';
      } */
}

function wpestate_isValidTimezoneId2($tzid) {
    $valid = array();
    $tza = timezone_abbreviations_list();

    foreach ($tza as $zone)
        foreach ($zone as $item)
            $valid[$item['timezone_id']] = true;
    unset($valid['']);
    return !!$valid[$tzid];
}

add_action('admin_notices', 'wpestate_admin_notice');

/* function edit_user_notification_email($wp_new_user_notification_email,$user,$blogname) {

  $newpass = wp_generate_password( 10, true, false );
  wp_set_password($newpass,$user->ID);
  $message = sprintf(__( "Welcome to Club Hub UK! You can login now using the below credentials:" ), $blogname ) . "\r\n";
  $message .= wp_login_url() . "\r\n";
  $message .= sprintf(__( 'Username: %s' ), $user->user_login ) . "\r\n";
  $message .= sprintf(__( 'Password: %s' ), $newpass) . "\r\n\r\n";
  $message .= 'If you have any problems, please contact me at info@club-hub-app.co.uk'."\r\n";
  $message .= __('Thank You!');
  $wp_new_user_notification_email['message'] = $message;
  return $wp_new_user_notification_email;
  }
  add_filter( 'wp_new_user_notification_email' , 'edit_user_notification_email', 10, 3 ); */




add_action('after_setup_theme', 'wp_estate_init');
if (!function_exists('wp_estate_init')):

    function wp_estate_init() {

        global $content_width;
        if (!isset($content_width)) {
            $content_width = 1800;
        }

        load_theme_textdomain('wpestate', get_template_directory() . '/languages');
        set_post_thumbnail_size(940, 198, true);
        add_editor_style();
        add_theme_support('post-thumbnails');
        add_theme_support('automatic-feed-links');
        add_theme_support('custom-background');
        add_theme_support("title-tag");
        wp_estate_setup();
        add_action('widgets_init', 'register_wpestate_widgets');
        add_action('init', 'wpestate_shortcodes');
        wp_oembed_add_provider('#https?://twitter.com/\#!/[a-z0-9_]{1,20}/status/\d+#i', 'https://api.twitter.com/1/statuses/oembed.json', true);
        wpestate_image_size();
        add_filter('excerpt_length', 'wp_estate_excerpt_length');
        add_filter('excerpt_more', 'wpestate_new_excerpt_more');
        add_action('tgmpa_register', 'wpestate_required_plugins');
        add_action('wp_enqueue_scripts', 'wpestate_scripts'); // function in css_js_include.php
        add_action('admin_enqueue_scripts', 'wpestate_admin'); // function in css_js_include.php
        update_option('image_default_link_type', 'file');
    }

endif; // end   wp_estate_init  

/* bulk edit menu for users */
/* add_filter('wp_dropdown_users', 'MySwitchUser');
  function MySwitchUser($output)
  {
  if(substr( $_SERVER['REQUEST_URI'], 0, 18 ) != '/wp-admin/post.php'){
  global $post, $user_ID, $pagenow;

  // return if this isn't the theme author override dropdown
  // if (!preg_match('/post_author_override/', $output)) return $output;

  // return if we've already replaced the list (end recursion)
  //if (preg_match ('/post_author_override_replaced/', $output)) return $output;



  //global $post is available here, hence you can check for the post type here
  $users = get_users();

  $output = "<select id=\"post_author_override\" name=\"post_author_override\" class=\"\">";

  //Leave the admin in the list
  $output .= "<option value=\"1\">Admin</option>";
  foreach($users as $user)
  {
  $sel = ($post->post_author == $user->ID)?"selected='selected'":'';
  $output .= '<option value="'.$user->ID.'"'.$sel.'>'.$user->user_login.'</option>';
  }
  $output .= "</select>";

  // put the original name back
  // $output = preg_replace('/post_author_override_replaced/', 'post_author_override', $output);

  }
  return $output;
  } */
add_filter('wp_dropdown_users_args', 'add_subscribers_to_dropdown', 10, 2);

function add_subscribers_to_dropdown($query_args, $r) {

    $query_args['who'] = '';
    return $query_args;
}

add_action('manage_posts_custom_column', 'rkv_post_columns_data', 10, 2);
add_filter('manage_edit-estate_property_columns', 'rkv_post_columns_display');

function rkv_post_columns_data($column, $post_id) {
    switch ($column) {
        case 'modified':
            $m_orig = get_post_field('post_modified', $post_id, 'raw');
            $m_stamp = strtotime($m_orig);
            $modified = date('n/j/y @ g:i a', $m_stamp);
            $modr_id = get_post_meta($post_id, '_edit_last', true);
            $auth_id = get_post_field('post_author', $post_id, 'raw');
            $user_id = !empty($modr_id) ? $modr_id : $auth_id;
            $user_info = get_userdata($user_id);

            echo '<p class="mod-date">';
            echo '<em>' . $modified . '</em><br />';
            echo 'by <strong>' . $user_info->display_name . '<strong>';
            echo '</p>';
            break;
        // end all case breaks
    }
}

function rkv_post_columns_display($columns) {
    $columns['modified'] = 'Last Modified';
    return $columns;
}

function last_modified_column_register_sortable($columns) {
    $columns["modified"] = "last_modified";
    return $columns;
}

add_filter("manage_edit-estate_property_sortable_columns", "last_modified_column_register_sortable");

add_filter('manage_posts_columns', 'dt_managing_my_posts_columns', 10, 2);

function dt_managing_my_posts_columns($columns, $post_type) {
    switch ($post_type) {
        case 'estate_property':
            $new_columns = array();
            foreach ($columns as $key => $value) {
                $new_columns[$key] = $value;
                if ($key == 'title')
                    $new_columns['club_owner'] = 'Club Owner';
            }
            return $new_columns;
    }
    return $columns;
}

add_action('manage_posts_custom_column', 'dt_populating_my_posts_columns', 10, 2);

function dt_populating_my_posts_columns($column_name, $post_id) {
    switch ($column_name) {
        case 'club_owner':
            echo '<div id="club_owner-' . $post_id . '">';
            $user_info = get_userdata(get_post_meta($post_id, 'property_agent', true));
            $username = $user_info->user_login;
            echo $username . '</div>';
            break;
    }
}

add_action('bulk_edit_custom_box', 'dt_add_to_bulk_quick_edit_custom_box', 10, 2);
function dt_add_to_bulk_quick_edit_custom_box($column_name, $post_type) {
    switch ($post_type) {
        case 'estate_property':

            switch ($column_name) {
                case 'estate_price':
                    ?><table style='margin-top:20px;'>
                        <tr>
                            <td valign="top" align="left">  
                                <p class="meta-options">                                
                                    <input type="checkbox" id="prop_weblink" name="prop_weblink" value="1">
                                    <label for="prop_weblink">Enable the weblink package</label>
                                </p>
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td valign="top" align="left">  
                                <p class="meta-options">                                
                                    <input type="checkbox" id="prop_localman" name="prop_localman" value="1">
                                    <label for="prop_localman">Make it a Local Boost Club</label>
                                </p>
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td valign="top" align="left">  
                                <p class="meta-options">                                
                                    <input type="checkbox" id="prop_featured" name="prop_featured" value="1">
                                    <label for="prop_featured">Make it a Featured Club</label>
                                </p>
                            </td>
                        </tr>

                    </table>
                    <table>
                        <tr>
                            <td valign="top" align="left">  
                                <p class="meta-options">                                
                                    <input type="checkbox" id="invoice_payment" name="invoice_payment" value="1">
                                    <label for="invoice_payment">Manual Invoice Payment?</label>
                                </p>
                            </td>
                        </tr>

                    </table>
                    <table>
                        <tr>
                            <td valign="top" align="left">  
                                <p class="meta-options">  
                                    <label for="package_activation">Enter Latest Payment Date "yyyy-mm-dd hh:mm:ss"</label>
                                    <br>
                                    <input type="text" id="package_activation" name="package_activation" value="">
                                </p>
                            </td>
                        </tr>

                    </table>
                    <table>
                        <tr>
                            <td valign="top" align="left">  
                                <p class="meta-options">  
                                    <label for="package_id">Choose Membership Type</label>
                                    <br>
                                    <select id="package_id" name="package_id">
                                        <option value=""></option>
                                        <option value="0">No Membership</option>
                                        <option value="137320">Monthly Membership</option>
                                        <option value="120676">Charity Membership</option>
                                        <option value="119535">Discounted Membership</option>
                                        <option value="122972">Full Price Membership</option>
                                    </select>
                                </p>
                            </td>
                        </tr>

                    </table>
                    <?php
                    break;
            }
            break;
    }
}

add_action('admin_print_scripts-edit.php', 'dt_enqueue_edit_scripts');

function dt_enqueue_edit_scripts() {
    wp_enqueue_script('dt-admin-edit', get_bloginfo('stylesheet_directory') . '/quick_edit.js', array('jquery', 'inline-edit-post'), '', true);
}

add_action('wp_ajax_dt_save_bulk_edit', 'dt_save_bulk_edit');

function dt_save_bulk_edit() {
    // get our variables
    $post_ids = ( isset($_POST['post_ids']) && !empty($_POST['post_ids']) ) ? $_POST['post_ids'] : array();
    // $release_date = ( isset( $_POST[ 'property_agent' ] ) && !empty( $_POST[ 'property_agent' ] ) ) ? $_POST[ 'property_agent' ] : NULL;
    $prop_featured = isset($_POST['prop_featured']) ? $_POST['prop_featured'] : 0;
    $prop_localman = isset($_POST['prop_localman']) ? $_POST['prop_localman'] : 0;
    $prop_weblink = isset($_POST['prop_weblink']) ? $_POST['prop_weblink'] : 0;
    $invoice_payment = isset($_POST['invoice_payment']) ? $_POST['invoice_payment'] : 0;
    $prop_package_activation = isset($_POST['package_activation']) ? $_POST['package_activation'] : 0;
    $prop_package_id = isset($_POST['package_id']) ? $_POST['package_id'] : 0;

    // if everything is in order
    if (!empty($post_ids) && is_array($post_ids)) {
        foreach ($post_ids as $post_id) {
            if ($prop_featured) {
                update_post_meta($post_id, 'prop_featured', $prop_featured);
            }
            if ($prop_localman) {
                update_post_meta($post_id, 'prop_localman', $prop_localman);
                update_post_meta($post_id, 'local-expires', date('Y-m-d',strtotime("+50 years")));
            }
            if ($prop_weblink) {
                update_post_meta($post_id, 'prop_weblink', $prop_weblink);
            }
            $author = get_post_field('post_author', $post_id);
            if ($invoice_payment) {
                update_user_meta($author, 'invoice_payment', 'Invoice');
            } else {
                update_user_meta($author, 'invoice_payment', ''); 
            }
            if ($prop_package_activation) {
                update_user_meta($author, 'package_activation', $prop_package_activation);
            }
            if ($prop_package_id) {
                update_user_meta($author, 'package_id', $prop_package_id);
                $get_pkg_name = wpestate_get_package_name_by_id($prop_package_id);
                if ($get_pkg_name) {
                    update_user_meta($author, 'package_name', $get_pkg_name);
                }
                update_user_meta($author, 'package_listings', '-1');
                if ($prop_package_id == 0) {
                    update_user_meta($author, 'package_listings', '0');
                }
            }
            /*
              update_post_meta( $post_id, 'property_agent', $release_date );
              $arg = array(
              'ID' => $post_id,
              'post_author' => $_POST[ 'property_agent' ],
              );
              wp_update_post( $arg );
              wp_update_post( $arg ); */
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
/////// If admin create the menu
///////////////////////////////////////////////////////////////////////////////////////////
if (is_admin()) {
    add_action('admin_menu', 'wpestate_manage_admin_menu');
}

if (!function_exists('wpestate_manage_admin_menu')):

    function wpestate_manage_admin_menu() {
        global $theme_name;
        add_theme_page(esc_html__('WpRentals Options', 'wpestate'), esc_html__('WpRentals Options', 'wpestate'), 'administrator', 'libs/theme-admin.php', 'wpestate_new_general_set');
        require_once get_template_directory() . '/libs/property-admin.php';
        require_once get_template_directory() . '/libs/pin-admin.php';
        require_once get_template_directory() . '/libs/theme-admin.php';
    }

endif; // end   wpestate_manage_admin_menu 
//////////////////////////////////////////////////////////////////////////////////////////////
// page details : setting sidebar position etc...
//////////////////////////////////////////////////////////////////////////////////////////////

if (!function_exists('wpestate_page_details')):

    function wpestate_page_details($post_id) {
        $return_array = array();


        if ($post_id != '' && !is_home() && !is_tax() && !is_search()) {
            $sidebar_name = esc_html(get_post_meta($post_id, 'sidebar_select', true));
            $sidebar_status = esc_html(get_post_meta($post_id, 'sidebar_option', true));
        } else {
            $sidebar_name = esc_html(get_option('wp_estate_blog_sidebar_name', ''));
            $sidebar_status = esc_html(get_option('wp_estate_blog_sidebar', ''));
        }

        if ('' == $sidebar_name) {
            $sidebar_name = 'primary-widget-area';
        }
        if ('' == $sidebar_status) {
            $sidebar_status = 'right';
        }


        if ('left' == $sidebar_status) {
            $return_array['content_class'] = 'col-md-8 col-md-push-4 ';
            $return_array['sidebar_class'] = 'col-md-4 col-md-pull-8 ';
        } else if ($sidebar_status == 'right') {
            $return_array['content_class'] = 'col-md-8 ';
            $return_array['sidebar_class'] = 'col-md-4 ';
        } else {
            $return_array['content_class'] = 'col-md-12';
            $return_array['sidebar_class'] = 'none';
        }

        $return_array['sidebar_name'] = $sidebar_name;

        return $return_array;
    }

endif; // end   wpestate_page_details 

/*
 * Let Editors manage users, and run this only once.
 */
/* function isa_editor_manage_users() {

  if ( get_option( 'isa_add_cap_editor_once' ) != 'done' ) {

  // let editor manage users

  $edit_editor = get_role('editor'); // Get the user role
  $edit_editor->add_cap('edit_users');
  $edit_editor->add_cap('list_users');
  $edit_editor->add_cap('promote_users');
  $edit_editor->add_cap('create_users');
  $edit_editor->add_cap('add_users');
  $edit_editor->add_cap('delete_users');

  update_option( 'isa_add_cap_editor_once', 'done' );
  }

  }
  add_action( 'init', 'isa_editor_manage_users' ); */

//prevent editor from deleting, editing, or creating an administrator
// only needed if the editor was given right to edit users

class ISA_User_Caps {

    // Add our filters
    function __construct() {
        add_filter('editable_roles', array(&$this, 'editable_roles'));
        add_filter('map_meta_cap', array(&$this, 'map_meta_cap'), 10, 4);
    }

    // Remove 'Administrator' from the list of roles if the current user is not an admin
    function editable_roles($roles) {
        if (isset($roles['administrator']) && !current_user_can('administrator')) {
            unset($roles['administrator']);
        }
        return $roles;
    }

    // If someone is trying to edit or delete an
    // admin and that user isn't an admin, don't allow it
    function map_meta_cap($caps, $cap, $user_id, $args) {
        switch ($cap) {
            case 'edit_user':
            case 'remove_user':
            case 'promote_user':
                if (isset($args[0]) && $args[0] == $user_id)
                    break;
                elseif (!isset($args[0]))
                    $caps[] = 'do_not_allow';
                $other = new WP_User(absint($args[0]));
                if ($other->has_cap('administrator')) {
                    if (!current_user_can('administrator')) {
                        $caps[] = 'do_not_allow';
                    }
                }
                break;
            case 'delete_user':
            case 'delete_users':
                if (!isset($args[0]))
                    break;
                $other = new WP_User(absint($args[0]));
                if ($other->has_cap('administrator')) {
                    if (!current_user_can('administrator')) {
                        $caps[] = 'do_not_allow';
                    }
                }
                break;
            default:
                break;
        }
        return $caps;
    }

}

$isa_user_caps = new ISA_User_Caps();


//////////////////////////////////////////////////////////////////////////////////////
/////// generate custom css
///////////////////////////////////////////////////////////////////////////////////////////

add_action('wp_head', 'wpestate_generate_options_css');
if (!function_exists('wpestate_generate_options_css')):

    function wpestate_generate_options_css() {
        $general_font = esc_html(get_option('wp_estate_general_font', ''));
        $custom_css = stripslashes(get_option('wp_estate_custom_css'));
        $color_scheme = esc_html(get_option('wp_estate_color_scheme', ''));

        if ($general_font != '' || $color_scheme == 'yes' || $custom_css != '') {
            echo "<style type='text/css'>";
            if ($general_font != '') {
                require_once get_template_directory() . '/libs/custom_general_font.php';
            }


            if ($color_scheme == 'yes') {
                require_once get_template_directory() . '/libs/customcss.php';
            }
            print $custom_css;
            echo "</style>";
        }
    }

endif; // end   generate_options_css 

if ( !function_exists( 'dt_move_additional_css' ) ) {
    function dt_move_additional_css() {
        wp_enqueue_style( 'dt_move_additional_css', trailingslashit( get_stylesheet_directory_uri() ) . 'css/customizer.css' );
    }
}
add_action( 'wp_enqueue_scripts', 'dt_move_additional_css', 999 );
///////////////////////////////////////////////////////////////////////////////////////////
///////  Display navigation to next/previous pages when applicable
///////////////////////////////////////////////////////////////////////////////////////////

if (!function_exists('wp_estate_content_nav')) :

    function wp_estate_content_nav($html_id) {
        global $wp_query;

        if ($wp_query->max_num_pages > 1) :
            ?>
            <nav id="<?php echo esc_attr($html_id); ?>">
                <h3 class="assistive-text"><?php esc_html_e('Post navigation', 'wpestate'); ?></h3>
                <div class="nav-previous"><?php next_posts_link(esc_html__('<span class="meta-nav">&larr;</span> Older posts', 'wpestate')); ?></div>
                <div class="nav-next"><?php previous_posts_link(esc_html__('Newer posts <span class="meta-nav">&rarr;</span>', 'wpestate')); ?></div>
            </nav><!-- #nav-above -->
            <?php
        endif;
    }

endif; // wpestate_content_nav
///////////////////////////////////////////////////////////////////////////////////////////
///////  Comments
///////////////////////////////////////////////////////////////////////////////////////////

/* if (!function_exists('wpestate_comment')) :

  function wpestate_comment($comment, $args, $depth) {
  $GLOBALS['comment'] = $comment;
  switch ($comment->comment_type) :
  case 'pingback' :
  case 'trackback' :
  ?>
  <li class="post pingback">
  <p><?php esc_html_e('Pingback:', 'wpestate'); ?> <?php comment_author_link(); ?><?php edit_comment_link(esc_html__( 'Edit', 'wpestate'), '<span class="edit-link">', '</span>'); ?></p>
  <?php
  break;
  default :
  ?>




  <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">

  <?php
  $avatar =esc_url( wpestate_get_avatar_url(get_avatar($comment, 55)));
  print '<div class="blog_author_image singlepage" style="background-image: url(' . esc_url($avatar) . ');">';
  print '</div>';
  ?>

  <div id="comment-<?php comment_ID(); ?>" class="comment">
  <?php edit_comment_link(esc_html__( 'Edit', 'wpestate'), '<span class="edit-link">', '</span>'); ?>
  <div class="comment-meta">
  <div class="comment-author vcard">
  <?php
  print '<div class="comment_name">' . get_comment_author_link() . '</div>';
  print '<span class="comment_date">' . esc_html__( ' on ', 'wpestate') . ' ' . get_comment_date() . '</span>';
  ?>
  </div><!-- .comment-author .vcard -->

  <?php if ($comment->comment_approved == '0') : ?>
  <em class="comment-awaiting-moderation"><?php esc_html_e('Your comment is awaiting moderation.', 'wpestate'); ?></em>
  <br />
  <?php endif; ?>

  </div>

  <div class="comment-content">
  <?php comment_text(); ?>

  <?php comment_reply_link(array_merge($args, array('reply_text' => '<i class="fa fa-reply"></i> ' . esc_html__( 'Reply', 'wpestate'), 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
  </div>

  </div><!-- #comment-## -->
  <?php
  break;
  endswitch;
  }

  endif; */ // ends check for  wpestate_comment 
////////////////////////////////////////////////////////////////////////////////
/// Add new profile fields
////////////////////////////////////////////////////////////////////////////////

add_filter('user_contactmethods', 'wpestate_modify_contact_methods');
if (!function_exists('wpestate_modify_contact_methods')):

    function wpestate_modify_contact_methods($profile_fields) {

        // Add new fields
        $profile_fields['facebook'] = esc_html__('Facebook', 'wpestate');
        $profile_fields['twitter'] = esc_html__('Twitter', 'wpestate');
        $profile_fields['linkedin'] = esc_html__('Linkedin', 'wpestate');
        $profile_fields['pinterest'] = esc_html__('Pinterest', 'wpestate');
        $profile_fields['phone'] = esc_html__('Phone', 'wpestate');
        $profile_fields['mobile'] = esc_html__('Mobile', 'wpestate');
        $profile_fields['skype'] = esc_html__('Skype', 'wpestate');
        $profile_fields['title'] = esc_html__('Title/Position', 'wpestate');
        $profile_fields['custom_picture'] = esc_html__('Picture Url', 'wpestate');
        $profile_fields['small_custom_picture'] = esc_html__('Small Picture Url', 'wpestate');
        $profile_fields['package_id'] = esc_html__('Package Id', 'wpestate');
        $profile_fields['package_name'] = esc_html__('Package Name', 'wpestate');
        $profile_fields['package_activation'] = esc_html__('Package Activation', 'wpestate');
        $profile_fields['package_listings'] = esc_html__('Listings available', 'wpestate');
        $profile_fields['package_pay_status'] = esc_html__('Package Pay Status', 'wpestate');
        $profile_fields['package_featured_listings'] = esc_html__('Featured Listings available', 'wpestate');
        $profile_fields['prop_featured'] = esc_html__('Featured Package', 'wpestate');
        $profile_fields['invoice_payment'] = esc_html__('Manual Invoice Payment?', 'wpestate');
        $profile_fields['emailed_date'] = esc_html__('Last Sub Reminder Email Date', 'wpestate');
        $profile_fields['profile_id'] = esc_html__('Paypal Recuring Profile', 'wpestate');
        $profile_fields['user_agent_id'] = esc_html__('User Owner Id', 'wpestate');
        $profile_fields['stripe'] = esc_html__('Stripe Consumer Profile', 'wpestate');
        //$profile_fields['i_speak']      = esc_html__('I Speak','wpestate');
        //$profile_fields['live_in']      = esc_html__('Live In','wpestate');
        //$profile_fields['user_type']    = esc_html__('User Type(0-can rent and book / 1 can only book)','wpestate');
        return $profile_fields;
    }

endif; // end   wpestate_modify_contact_methods 




if (!current_user_can('activate_plugins')) {

    if (!function_exists('wpestate_admin_bar_render')):

        function wpestate_admin_bar_render() {
            global $wp_admin_bar;
            $wp_admin_bar->remove_menu('edit-profile', 'user-actions');
        }

    endif;

    add_action('wp_before_admin_bar_render', 'wpestate_admin_bar_render');

    if (!current_user_can('edit_users')) {

        add_action('admin_init', 'wpestate_stop_access_profile');
        if (!function_exists('wpestate_stop_access_profile')):

            function wpestate_stop_access_profile() {
                global $pagenow;

                if (defined('IS_PROFILE_PAGE') && IS_PROFILE_PAGE === true) {
                    wp_die(esc_html__('Please edit your profile page from site interface.', 'wpestate'));
                }

                if ($pagenow == 'user-edit.php') {
                    wp_die(esc_html__('Please edit your profile page from site interface.', 'wpestate'));
                }
            }

        endif; // end   wpestate_stop_access_profile 
    }
}// end user can activate_plugins
///////////////////////////////////////////////////////////////////////////////////////////
// get attachment info
///////////////////////////////////////////////////////////////////////////////////////////

if (!function_exists('wpestate_get_attachment')):

    function wpestate_get_attachment($attachment_id) {

        $attachment = get_post($attachment_id);
        return array(
            'alt' => get_post_meta($attachment->ID, '_wp_attachment_image_alt', true),
            'caption' => $attachment->post_excerpt,
            'description' => $attachment->post_content,
            'href' => esc_url(get_permalink($attachment->ID)),
            'src' => $attachment->guid,
            'title' => $attachment->post_title
        );
    }

endif;


add_action('get_header', 'wpestate_my_filter_head');
if (!function_exists('wpestate_my_filter_head')):

    function wpestate_my_filter_head() {
        remove_action('wp_head', '_admin_bar_bump_cb');
    }

endif;

///////////////////////////////////////////////////////////////////////////////////////////
// loosing session fix
///////////////////////////////////////////////////////////////////////////////////////////
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');

///////////////////////////////////////////////////////////////////////////////////////////
// forgot pass action
///////////////////////////////////////////////////////////////////////////////////////////

add_action('wp_head', 'wpestate_hook_javascript');
if (!function_exists('wpestate_hook_javascript')):

    function wpestate_hook_javascript() {
        global $wpdb;
        $allowed_html = array();
        if (isset($_GET['key']) && $_GET['action'] == "reset_pwd") {
            $reset_key = sanitize_text_field(wp_kses($_GET['key'], $allowed_html));
            $user_login = sanitize_text_field(wp_kses($_GET['login'], $allowed_html));
            $user_data = $wpdb->get_row($wpdb->prepare("SELECT ID, user_login, user_email FROM $wpdb->users 
    WHERE user_activation_key = %s AND user_login = %s", $reset_key, $user_login));


            if (!empty($user_data)) {
                $user_login = $user_data->user_login;
                $user_email = $user_data->user_email;

                if (!empty($reset_key) && !empty($user_data)) {
                    $new_password = wp_generate_password(7, false);
                    wp_set_password($new_password, $user_data->ID);
                    //mailing the reset details to the user
                    $message = esc_html__('Your new password for the account at:', 'wpestate') . "\r\n\r\n";
                    $message .= get_bloginfo('name') . "\r\n\r\n";
                    $message .= sprintf(esc_html__('Username: %s', 'wpestate'), $user_login) . "\r\n\r\n";
                    $message .= sprintf(esc_html__('Password: %s', 'wpestate'), $new_password) . "\r\n\r\n";
                    $message .= esc_html__('You can now login with your new password at: ', 'wpestate') . esc_url(get_option('siteurl')) . "/" . "\r\n\r\n";

                    $headers = 'From: noreply  <noreply@' . $_SERVER['HTTP_HOST'] . '>' . "\r\n" .
                            'Reply-To: noreply@' . $_SERVER['HTTP_HOST'] . "\r\n" .
                            'X-Mailer: PHP/' . phpversion();

                    if ($message && !wp_mail($user_email, esc_html__('Your Password Was Reset', 'wpestate'), $message, $headers)) {
                        $mess = "<div class='error'>" . esc_html__('Email sending has failed for some unknown reason', 'wpestate') . "</div>";
                        //exit();
                    } else {
                        $mess = '<div class="login-alert">' . esc_html__('A new password was sent via email!', 'wpestate') . '</div>';
                    }
                } else {
                    exit('Not a Valid Key.');
                }
            }// end if empty
            print '<div class="login_alert_full" id="forgot_notice">' . esc_html__('We have just sent you a new password. Please check your email!', 'wpestate') . '</div>';
        }
    }

endif;

if (!function_exists('wpestate_get_pin_file_path_read')):

    function wpestate_get_pin_file_path_read() {
        if (function_exists('icl_translate')) {
            $path = trailingslashit(get_template_directory_uri()) . '/pins-' . apply_filters('wpml_current_language', 'en') . '.txt';
        } else {
            $path = trailingslashit(get_template_directory_uri()) . '/pins.txt';
        }

        return $path;
    }

endif;

if (!function_exists('wpestate_get_pin_file_path_write')):

    function wpestate_get_pin_file_path_write() {
        if (function_exists('icl_translate')) {
            $path = get_template_directory() . '/pins-' . apply_filters('wpml_current_language', 'en') . '.txt';
        } else {
            $path = get_template_directory() . '/pins.txt';
        }

        return $path;
    }

endif;


add_filter('redirect_canonical', 'wpestate_disable_redirect_canonical', 10, 2);

function wpestate_disable_redirect_canonical($redirect_url, $requested_url) {
    //print '$redirect_url'.$redirect_url;
    //print '$requested_url'.$requested_url;
    if (is_page_template('property_list.php') || is_page_template('property_list_half.php')) {
        //  print 'bag false';
        $redirect_url = false;
    }


    return $redirect_url;
}

if (!function_exists('wpestate_check_user_level')):

    function wpestate_check_user_level() {
        $current_user = wp_get_current_user();
        $userID = $current_user->ID;
        $user_login = $current_user->user_login;
        $separate_users_status = esc_html(get_option('wp_estate_separate_users', ''));
        $publish_only = esc_html(get_option('wp_estate_publish_only', ''));


        if (trim($publish_only) != '') {
            $user_array = explode(',', $publish_only);

            if (in_array($user_login, $user_array)) {
                return true;
            } else {
                return false;
            }
        }


        if ($separate_users_status == 'no') {
            return true;
        } else {
            $user_level = intval(get_user_meta($userID, 'user_type', true));

            if ($user_level == 0) { // user can book and rent
                return true;
            } else {// user can only book
                if (basename(get_page_template()) == 'user_dashboard.php' ||
                        basename(get_page_template()) == 'user_dashboard_add_step1.php' ||
                        basename(get_page_template()) == 'user_dashboard_edit_listing.php' ||
                        basename(get_page_template()) == 'user_dashboard_my_bookings.php' ||
                        basename(get_page_template()) == 'user_dashboard_packs.php' ||
                        basename(get_page_template()) == 'user_dashboard_searches.php' ||
                        basename(get_page_template()) == 'user_dashboard_allinone.php') {

                    return false;
                }
            }
        }
    }

endif;

function estate_create_onetime_nonce($action = -1) {
    $time = time();
    // print $time.$action;
    $nonce = wp_create_nonce($time . $action);
    return $nonce . '-' . $time;
}

//1455041901register_ajax_nonce_topbar

function estate_verify_onetime_nonce($_nonce, $action = -1) {
    $parts = explode('-', $_nonce);
    $nonce = $toadd_nonce = $parts[0];
    $generated = $parts[1];

    $nonce_life = 60 * 60;
    $expires = (int) $generated + $nonce_life;
    $time = time();

    if (!wp_verify_nonce($nonce, $generated . $action) || $time > $expires) {
        return false;
    }

    $used_nonces = get_option('_sh_used_nonces');

    if (isset($used_nonces[$nonce])) {
        return false;
    }

    if (is_array($used_nonces)) {
        foreach ($used_nonces as $nonce => $timestamp) {
            if ($timestamp > $time) {
                break;
            }
            unset($used_nonces[$nonce]);
        }
    }

    $used_nonces[$toadd_nonce] = $expires;
    asort($used_nonces);
    update_option('_sh_used_nonces', $used_nonces);
    return true;
}

function estate_verify_onetime_nonce_login($_nonce, $action = -1) {
    $parts = explode('-', $_nonce);
    $nonce = $toadd_nonce = $parts[0];
    $generated = $parts[1];

    $nonce_life = 60 * 60;
    $expires = (int) $generated + $nonce_life;
    $expires2 = (int) $generated + 120;
    $time = time();

    if (!wp_verify_nonce($nonce, $generated . $action) || $time > $expires) {
        return false;
    }

    //Get used nonces
    $used_nonces = get_option('_sh_used_nonces');

    if (isset($used_nonces[$nonce])) {
        return false;
    }

    if (is_array($used_nonces)) {
        foreach ($used_nonces as $nonce => $timestamp) {
            if ($timestamp > $time) {
                break;
            }
            unset($used_nonces[$nonce]);
        }
    }

    //Add nonce in the stack after 2min
    if ($time > $expires2) {
        $used_nonces[$toadd_nonce] = $expires;
        asort($used_nonces);
        update_option('_sh_used_nonces', $used_nonces);
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////////////////
// prevent changing the author id when admin hit publish
///////////////////////////////////////////////////////////////////////////////////////////

add_action('transition_post_status', 'wpestate_correct_post_data', 10, 3);

if (!function_exists('wpestate_correct_post_data')):

    function wpestate_correct_post_data($strNewStatus, $strOldStatus, $post) {
        /* Only pay attention to posts (i.e. ignore links, attachments, etc. ) */
        if ($post->post_type !== 'estate_property')
            return;

        if ($strOldStatus === 'new') {
            update_post_meta($post->ID, 'original_author', $post->post_author);
        }



        /* If this post is being published, try to restore the original author */
        if ($strNewStatus === 'publish') {


            $originalAuthor_id = $post->post_author;


            $user = get_user_by('id', $originalAuthor_id);
            if (!$user) {
                return;
            }
            $user_email = $user->user_email;




            if ($user->roles[0] == 'subscriber') {
                $arguments = array(
                    'post_id' => $post->ID,
                    'property_url' => get_permalink($post->ID),
                    'property_title' => get_the_title($post->ID)
                );

                ///modified email message text
                if ($strOldStatus == 'pending') {
                    $message = '';
                    /* $message=__('Hi there,
                      Your listing, %property_title was approved on  %website_url! The listing is: %property_url.
                      You should go check it out.','wpestate'); */

                    $message = esc_html__("Welcome to Club Hub!", "wpestate") . "\r\n\r\n";
                    $message .= get_permalink($post->ID) . "\r\n\r\n";
                    $message .= esc_html__("We hope you've found everything you need to get your activities listed on our site. With over 80,000 visitors to our site every month and over 22,000 kids activities listed Club Hub is growing fast.", "wpestate") . "\r\n\r\n";


                    $message .= "Have you enabled bookings yet? You can now Enable Free Trial or Upgrade to the £2.50 web link package. This enables parents to go straight through to your desired website link. We don't take any commission, just a one off fee of £2.50 per listing per year. https://clubhubuk.co.uk/user-dashboard/" . "\r\n\r\n";

                    $message .= esc_html__("We pride ourselves on offering a personal service to each and every one of our providers and offer a number of competitively priced packages that allow you to boost your business at a time that suits your needs. https://clubhubuk.co.uk/club-owner/", "wpestate") . "\r\n\r\n";

                    $message .= esc_html__("We'd be happy to discuss your requirements with you directly to see how we can best support you and your business and help you to grow with Club Hub. Contact Us or call me on: 07984 000 300.", "wpestate") . "\r\n\r\n";

                    $message .= esc_html__("You can join our Facebook Club Hub Support group on https://www.facebook.com/groups/club.hub.kids.activity.providers/", "wpestate") . "\r\n\r\n";

                    $message .= esc_html__("You can join our Facebook Counties Groups on https://www.facebook.com/pg/clubhubapp/groups/", "wpestate") . "\r\n\r\n";

                    $message .= esc_html__("Have you seen your Club Hub Listings on our free app?", 'wpestate') . "\r\n";

                    //$message .= esc_html__('You should go check it out.','wpestate');
                    $message .= esc_html__('App Store - https://itunes.apple.com/in/app/club-hub-uk-free/id1139824440?mt=8', 'wpestate') . "\r\n\r\n";

                    $message .= esc_html__("Google Play - https://play.google.com/store/apps/details?id=uk.co.clubhubukfree.", 'wpestate') . "\r\n\r\n";

                    $message .= esc_html__("Entry for the Club Hub Awards open on the 1st of November each year and close on the 31st of January each year. There is an administration fee of £5 to enter as many categories as you like. https://clubhubuk.co.uk/club-hub-event-2020/", 'wpestate') . "\r\n\r\n";


                    /* $message = "<p><b><u>Have you enabled bookings yet?</u></b></p>";

                      $message .= "<p>You can now <span style='padding: 5px; background: #FEFF00;'>\"Enable Free Trial\"</span> on your <span style='padding: 5px; background: #00FF00;'>\"Edit My Activities\"</span> page. This enables a Book Now Button where parents go straight through to your desired website link and you also receive the parents email address so you can contact them directly. </p>";

                      $message .= "<p>After the free trial it is a one off fee of £2.50 per year per listing to upgrade to the web link package and we take <b>0% commission</b>.</p>";

                      $message .= "<p>If you have any questions on the above or need any help, please do let us know.</p>"; */

                    $message .= esc_html__("Best Wishes,", 'wpestate') . "\r\n";
                    $message .= esc_html__("Tessa Robinson", 'wpestate') . "\r\n";
                    $message .= esc_html__("Founder and Director", 'wpestate') . "\r\n";
                    // $message .= esc_html__("", 'wpestate'). "\r\n";
                    // $message .= esc_html__("", 'wpestate'). "\r\n";
                    // $message .= esc_html__("", 'wpestate'). "\r\n";
                    // $message .= __("By far our most popular package is 'The Web Link Package' - this really is the must have package and offers exceptional value for money. Costing just <u>£2.50 per listing for 1 year</u> you can add a direct link to your website making it easier and more accessible for parents to reach you and book onto your classes.", "wpestate"). "\r\n\r\n";
                    // $message .= esc_html__("UPGRADE TO THE WEB LINK PACKAGE", "wpestate"). "\r\n";
                    // $message .= esc_html__("Why not boost your listings so you appear at the top of search results within 5 miles of your listing location for 2 weeks with our 'Local Package'.", "wpestate"). "\r\n\r\n";
                    // $message .= esc_html__("UPGRADE TO THE LOCAL PACKAGE", "wpestate"). "\r\n";
                    // $message .= esc_html__("We offer a number of other bespoke packages including News Articles, Home Page Articles and Adverts. We'd be happy to discuss your requirements with you directly to see how we can best support you and your business and help you to grow with Club Hub. Contact Us or call me on: 07984 000 300.", "wpestate"). "\r\n";
                    // $message .= esc_html__('You can join our Facebook Club Hub Support group on https://www.facebook.com/groups/club.hub.kids.activity.providers/','wpestate'). "\r\n\r\n";
                    // $message .= esc_html__('You can join our Facebook Counties Groups on https://www.facebook.com/pg/clubhubapp/groups/','wpestate'). "\r\n\r\n";
                    // $message .= esc_html__('You can also take a look at low-cost packages here. https://clubhubuk.co.uk/club-owner/'). "\r\n\r\n";
                    // $message .= esc_html__('You can also sign up to our newsletter email list. We only send an email once a month to keep you up to date with everything that is going on. http://eepurl.com/dl0URH','wpestate'). "\r\n\r\n";
                    //$message .= esc_html__(''). "\r\n\r\n";

                    /* $message .= esc_html__('We will share your Club Hub Listing on our Facebook, Twitter and Instagram Social Media Pages for FREE? If you can please like Our Facebook Page and Share it in return. https://www.facebook.com/clubhubapp/','wpestate'). "\r\n\r\n";

                      $message .= esc_html__('Want to be nominated for the Club Hub Awards 2019? Share this link below now with your followers. https://clubhubuk.co.uk/club-hub-event-2019/nominations/','wpestate'). "\r\n\r\n"; */

                    // $message .= esc_html__('Have you seen your Club Hub Listings on our free app?','wpestate'). "\r\n";
                    // $message .= esc_html__('App Store - https://itunes.apple.com/in/app/club-hub-uk-free/id1139824440?mt=8','wpestate'). "\r\n";
                    // $message .= esc_html__('Google Play - https://play.google.com/store/apps/details?id=uk.co.clubhubukfree','wpestate'). ".\r\n\r\n";
                    // $message .= esc_html__('Could we schedule a call? It would be great to talk to you about how we can help you and your business?','wpestate'). "\r\n\r\n";
                    // $message .= esc_html__('Best wishes','wpestate'). "\r\n";
                    // $message .= esc_html__('Tessa Robinson','wpestate'). "\r\n";             
                    // $message .= esc_html__('Founder and Director','wpestate'). "\r\n\r\n";
                    // $message .= esc_html__('Club Hub UK','wpestate'). "\r\n";
                    // $message .= esc_html__('https://youtu.be/VRkSePXGgic','wpestate'). "\r\n";
                    // $message .= esc_html__('tessarobinson@club-hub-app.co.uk','wpestate'). "\r\n";
                    // $message .= esc_html__('www.clubhubuk.co.uk','wpestate'). "\r\n";
                    // $message .= esc_html__('07984 000300','wpestate'). "\r\n";
                    //$message.=' / '.$strOldStatus.'/'.$strNewStatus;

                    $headers = 'From: noreply  <noreply@' . $_SERVER['HTTP_HOST'] . '>' . "\r\n" .
                            'Reply-To: noreply@' . $_SERVER['HTTP_HOST'] . "\r\n" .
                            'X-Mailer: PHP/' . phpversion();
                    /* wp_mail(
                      $user_email,
                      esc_html__( 'Your listing was approved','wpestate'),
                      htmlspecialchars_decode($message, ENT_QUOTES),
                      $headers
                      ); */
                }
            }
        }
    }

endif; // end   wpestate_correct_post_data 

function wpestate_double_tax_cover($property_area, $property_city, $post_id) {
    $prop_city_selected = get_term_by('name', $property_city, 'property_city');
    $prop_area_selected = get_term_by('name', $property_area, 'property_area');
    if (isset($prop_area_selected->term_id)) { // we have this tax
        //print  $prop_area_selected->term_id.' / '.$prop_area_selected->name;
        //print  $prop_city_selected->term_id.' / '.$prop_city_selected->name;
        $term_meta = get_option("taxonomy_$prop_area_selected->term_id");

        if ($term_meta['cityparent'] != $property_city) {
            $new_property_area = $property_area . ', ' . $property_city;
        } else {
            $new_property_area = $property_area;
        }
        wp_set_object_terms($post_id, $new_property_area, 'property_area');
        return $new_property_area;
    } else {
        wp_set_object_terms($post_id, $property_area, 'property_area');
        return $property_area;
    }
}


add_filter('get_meta_sql', 'cast_decimal_precision');

function cast_decimal_precision($array) {

    $array['where'] = str_replace('DECIMAL', 'DECIMAL(10,3)', $array['where']);

    return $array;
}

add_action("wp_ajax_clone_club", "clone_club");
add_action("wp_ajax_nopriv_clone_club", "clone_club");

function clone_club() {
    global $wpdb;
    $post_id = $_POST['club_id'];

    $post = get_post($post_id);
    $current_user = wp_get_current_user();
    $new_post_author = $current_user->ID;

    // $add_club = array(
    //                 'post_content' => $club_data->post_content,
    //                 'post_status' => 'draft', 
    //                 'post_title' => $club_data->post_title, 
    //                 'post_type' =>  $club_data->post_type,
    //                 'post_author' => $club_data->post_author
    //             );
    $args = array(
        'comment_status' => $post->comment_status,
        'ping_status' => $post->ping_status,
        'post_author' => $new_post_author,
        'post_content' => $post->post_content,
        'post_excerpt' => $post->post_excerpt,
        'post_name' => $post->post_name,
        'post_parent' => $post->post_parent,
        'post_password' => $post->post_password,
        'post_status' => 'draft',
        'post_title' => $post->post_title,
        'post_type' => $post->post_type,
        'to_ping' => $post->to_ping,
        'menu_order' => $post->menu_order
    );
    $new_post_id = wp_insert_post($args);

    $taxonomies = get_object_taxonomies($post->post_type);
    foreach ($taxonomies as $taxonomy) {
        $post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
        wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
    }

    $post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");


    if (count($post_meta_infos) != 0) {
        $sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
        foreach ($post_meta_infos as $meta_info) {
            $meta_key = $meta_info->meta_key;
            $meta_value = addslashes($meta_info->meta_value);

            $handle_subs_pack_key = $meta_key != 'feature_pack_product_id' && $meta_key != 'localboost_pack_product_id' && $meta_key != 'feature_pack_subscription_id' && $meta_key != 'localboost_pack_subscription_id' && $meta_key != 'feature_pack_plan_id' && $meta_key != 'localboost_pack_plan_id';
            if ($meta_key != 'pay_status' && $meta_key != 'prop_calendar' && $meta_key != 'prop_featured' && $meta_key != 'calendar-expires' && $meta_key != 'prop_localman' && $meta_key != 'prop_weblink' && $meta_key != 'weblink-expires' && $meta_key != 'local-expires' && $meta_key != 'club_subscription_id' && $meta_key != 'club_product_id' && $meta_key != 'club_plan_id' && $meta_key != 'club_active_subscription' && $meta_key != 'social_package' && $handle_subs_pack_key) {
                $sql_query_sel[] = "SELECT $new_post_id, '$meta_key', '$meta_value'";
            }
        }
        $sql_query_sel[] = "SELECT $new_post_id, 'pay_status', 'not paid'";
        $sql_query_sel[] = "SELECT $new_post_id, 'prop_calendar', '0'";
        $sql_query_sel[] = "SELECT $new_post_id, 'prop_featured', '0'";
        $sql_query_sel[] = "SELECT $new_post_id, 'prop_localman', '0'";
        $sql_query_sel[] = "SELECT $new_post_id, 'prop_weblink', '0'";
        $sql_query_sel[] = "SELECT $new_post_id, 'weblink-expires', '0'";
        $sql_query_sel[] = "SELECT $new_post_id, 'calendar-expires', '0'";
        $sql_query_sel[] = "SELECT $new_post_id, 'local-expires', '0'";
        $sql_query_sel[] = "SELECT $new_post_id, 'social_package', '0'";
        $sql_query_sel[] = "SELECT $new_post_id, 'feature-expires', '0'";

        $sql_query .= implode(" UNION ALL ", $sql_query_sel);
        $wpdb->query($sql_query);
    }
    /* Post attachments */
    $arguments = array(
        'numberposts' => -1,
        'post_type' => 'attachment',
        'post_parent' => $post_id,
        'post_status' => null,
        'orderby' => 'menu_order',
        'order' => 'ASC'
    );

    $curent_thumb = get_post_thumbnail_id($post_id);

    $post_attachments = get_posts($arguments);
    if (count($post_attachments)) {
        foreach ($post_attachments as $attachment) {
            $old_filename = get_attached_file($attachment->ID);
            $file = explode('/', $old_filename);
            $file = array_slice($file, 0, count($file) - 1);
            $path = implode('/', $file);
            //legacy commented here
            // $filename = $path.'/'.time().'-'.basename($old_filename);
            //new attachment file name 10-01-2020
            $old_name = explode(".", basename($old_filename), 2);
            $arr = explode("-club", $old_name[0], 2);
            $old_f_name = $arr[0];
            $random_string_logo = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
            $filename = $path . '/' . $old_f_name . '-club' . $random_string_logo . '.' . $old_name[1];
            //new attachment end here

            copy($old_filename, $filename); //clone attachment
            // The ID of the post this attachment is for.
            $parent_post_id = $new_post_id;

            // Check the type of file. We'll use this as the 'post_mime_type'.
            $filetype = wp_check_filetype(basename($filename), null);

            // Get the path to the upload directory.
            $wp_upload_dir = wp_upload_dir();

            // Prepare an array of post data for the attachment.
            $attachment = array(
                'post_mime_type' => $filetype['type'],
                'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
                'post_content' => '',
                'post_status' => 'inherit'
            );

            // Insert the attachment.
            $attach_id = wp_insert_attachment($attachment, $filename, $parent_post_id);
            require_once(ABSPATH . "wp-admin" . '/includes/image.php');
            $attach_data = wp_generate_attachment_metadata($attach_id, $filename);
            wp_update_attachment_metadata($attach_id, $attach_data);

            if ($curent_thumb == $attachment->ID) { // set post thumbnail
                set_post_thumbnail($parent_post_id, $attach_id);
            }
        }
    }



    echo $new_post_id;
    exit;
}

function auto_login() {
    if (isset($_GET['refcode']) && strlen($_GET['refcode']) > 4 && (is_singular('estate_property') || is_page(1800))) {
        $code = str_replace('/', '', $_GET['refcode']);
        $user_id = substr($code, 2, -2);
        if ($user_id > 0) {
            $url = get_permalink();
            if (!is_user_logged_in()) {
                wp_clear_auth_cookie();
                wp_set_current_user($user_id);
                wp_set_auth_cookie($user_id, true, false);
                wp_redirect($url);
                exit;
            }
        }
    }
}

add_action('wp', 'auto_login');

add_filter('wp_dropdown_cats', 'wp_dropdown_cats_multiple', 10, 2);

function wp_dropdown_cats_multiple($output, $r) {

    if (isset($r['multiple']) && $r['multiple']) {

        $output = preg_replace('/^<select/i', '<select multiple', $output);

        $output = str_replace("name='{$r['name']}'", "name='{$r['name']}[]'", $output);

        foreach (array_map('trim', explode(",", $r['selected'])) as $value)
            $output = str_replace("value=\"{$value}\"", "value=\"{$value}\" selected", $output);
    }

    return $output;
}

/* Namrata */

function enqueue_select2_jquery() {
    wp_register_style('select2css', '//cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.css', false, '1.0', 'all');
    wp_register_script('select2', '//cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.js', array('jquery'), '1.0', true);

    wp_enqueue_style('select2css');
    wp_enqueue_script('select2');
}

add_action('admin_enqueue_scripts', 'enqueue_select2_jquery');

function custom_admin_js() {
    ?>
    <script>
        function matchCustom(params, data)
        {
            // If there are no search terms, return all of the data
            if ($.trim(params.term) === '') {
                return data;
            }

            // Do not display the item if there is no 'text' property
            if (typeof data.text === 'undefined') {
                return null;
            }

            // `params.term` should be the term that is used for searching
            // `data.text` is the text that is displayed for the data object
            if (data.text.indexOf(params.term) > -1) {
                var modifiedData = $.extend({}, data, true);
                modifiedData.text += ' (matched)';

                // You can return modified objects from here
                // This includes matching the `children` how you want in nested data sets
                return modifiedData;
            }

            // Return `null` if the term should not be displayed
            return null;
        }
    </script>
    <style>
        .hideit {
            display: none;
        }
        .post_author_select {
            pointer-events: none;
        }
    </style>
    <?php
}

add_action('admin_footer', 'custom_admin_js');


// Filter to fix the Post Author Dropdown
add_filter('wp_dropdown_users', 'theme_post_author_override');

function theme_post_author_override($output) {
    global $post, $user_ID;

    // return if this isn't the theme author override dropdown
    if (!preg_match('/post_author/', $output))
        return $output;

    // return if we've already replaced the list (end recursion)
    if (preg_match('/post_author_replaced/', $output))
        return $output;

    // replacement call to wp_dropdown_users
    $output = wp_dropdown_users(array(
        'role__in' => array('subscriber', 'editor', 'author', 'contributor'),
        'echo' => 0,
        'show_option_none' => '--No Change--',
        'name' => 'post_author_replaced',
        //'selected' => empty($post->ID) ? $user_ID : $post->post_author,
        'include_selected' => true,
        'class' => 'post_author_select', //hideit
        'id' => 'post_author_select'
    ));

    $output .= '<input type="text" name="authors_autocomplete_post_author_input" id="post_author_autocomplete_input" class="post_author_autocomplete_input" style="width:89%" autocomplete="off" value="" placeholder="Change author here. Start typing ..." />';
// autocomplete of users in quick edit
    $output .= '
            <script> 
            jQuery(function(){
                jQuery(".post_author_autocomplete_input").focus(function(){
                    var pid = "' . $post->ID . '";      
                    jQuery(this).autocomplete({
                        delay: 100,
                        minLength: 1,
                        source: function( $request, $response ){
                            jQuery.ajax({
                                url: "' . admin_url('admin-ajax.php') . '",
                                type: "POST",
                                async: true,
                                cache: false,
                                dataType: "json",
                                data: {
                                    action: "authors_autocomplete_mb_autocomplete_callback",
                                    authors_autocomplete_mb_search_term: $request.term,
                                    authors_autocomplete_mb_post_id: ' . $post->ID . ',
                                    authors_autocomplete_mb_post_type: "estate_property"
                                },
                                success: function( $data ){
                                    $response( jQuery.map( $data, function( $item ) {
                                        return {
                                            user_id: $item.user_id,
                                            user_login: $item.user_login,
                                            display_name: $item.display_name,
                                            email: $item.email,
                                            value: $item.label,
                                            label: $item.label
                                        };
                                    }));
                                }
                            });
                        },
                        select: function( $event, $ui ) {
                            jQuery("#post_author_select").val($ui.item.user_id);
            
                            // change the saved post author
                            //authors_autocomplete_mb_change_post_author( $ui.item.user_id, $ui.item.display_name );
                            
                        }
                    }).data( "ui-autocomplete" )._renderItem = function( $ul, $item ) {
                            return jQuery( "<li>" ).append( "<a><strong>" + $item.display_name + "</strong><br />Username: <em>" + $item.user_login + "</em><br />E-mail: <em>" + $item.email + "</em></a>" ).appendTo( $ul );
                        };
                });
        
            
            }); // end IIFE
            
            </script>
            ';

    // put the original name back
    $output = preg_replace('/post_author_replaced/', 'post_author', $output);
    return "<span style='float: right;width: 83.5%;'>" . $output . "</span>";
    //return $output;
}


function getClubsByUserId($user_id) {
    $args = array(
        'author' => $user_id,
        'post_type' => 'estate_property',
        'post_status' => array('any'),
        'posts_per_page' => -1
    );
    $clubs = get_posts($args);
    return $clubs;
}

function download_all_users() {
    add_menu_page('Download Owners', 'Download Owners', 'create_users', 'downloadusers', 'downloaduseroutput');
}

function downloaduseroutput() {
    if (!current_user_can('create_users')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }

    echo '<div class="wrap">';

    echo '<h1>Download Club Owner List</h1>';
    echo '<a href="' . admin_url('admin.php?page=downloadusers') . '&action=download_csv&_wpnonce=' . wp_create_nonce('download_csv') . '" class="getFile">Download List</a></div>';
    if ($users = get_users(array(
        'orderby' => 'nicename',
        'order' => 'ASC',
        'has_published_posts' => array('estate_property'),
            //'blog_id'             => absint( get_current_blog_id() ) 
            ))) {
        echo '<table id="cotable">';
        foreach ($users as $user) {
            echo '<tr><td>' . $user->user_email . '</td>';

            //print_r($user);
            $args = array(
                'author' => $user->ID,
                'post_type' => 'estate_property',
                'orderby' => 'post_date',
                'order' => 'ASC',
                'posts_per_page' => 1
            );

            $current_user_posts = get_posts($args);
            // echo '<br/>Primary Category: '.$current_user_posts$user->user_email;
            echo '<td>' . $current_user_posts[0]->post_title . '</td>';
            echo '<td>' . get_the_terms($current_user_posts[0]->ID, 'property_category')[0]->name . '</td>';
            echo '<td>' . date("d/m/Y", strtotime($user->user_registered)) . '</td>';
            echo '</tr>';
        }
        echo '</table>';
    }
}

add_action('admin_menu', 'download_all_users');

function csv_export() {
    // Check for current user privileges 
    if (!current_user_can('create_users')) {
        return false;
    }
    // Check if we are in WP-Admin
    if (!is_admin()) {
        return false;
    }
    // Nonce Check
    $nonce = isset($_GET['_wpnonce']) ? $_GET['_wpnonce'] : '';
    if (!wp_verify_nonce($nonce, 'download_csv')) {
        die('Security check error');
    }

    ob_start();
    $domain = $_SERVER['SERVER_NAME'];
    $filename = 'users-' . $domain . '-' . time() . '.csv';

    $header_row = array(
        'Email',
        '1st Club',
        'Primary Category',
        'Date Registered'
    );
    $data_rows = array();
    if ($users = get_users(array(
        'orderby' => 'nicename',
        'order' => 'ASC',
        'has_published_posts' => array('estate_property'),
            //'blog_id'             => absint( get_current_blog_id() )
            ))) {
        foreach ($users as $user) {

            //print_r($user);
            $args = array(
                'author' => $user->ID,
                'post_type' => 'estate_property',
                'orderby' => 'post_date',
                'order' => 'ASC',
                'posts_per_page' => 1
            );

            $current_user_posts = get_posts($args);

            $row = array(
                $user->user_email,
                $current_user_posts[0]->post_title,
                get_the_terms($current_user_posts[0]->ID, 'property_category')[0]->name,
                date("d/m/Y", strtotime($user->user_registered))
            );
            $data_rows[] = $row;
        }
    }
    $fh = @fopen('php://output', 'w');
    fprintf($fh, chr(0xEF) . chr(0xBB) . chr(0xBF));
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Content-Description: File Transfer');
    header('Content-type: text/csv');
    header("Content-Disposition: attachment; filename={$filename}");
    header('Expires: 0');
    header('Pragma: public');
    fputcsv($fh, $header_row);
    foreach ($data_rows as $data_row) {
        fputcsv($fh, $data_row);
    }
    fclose($fh);

    ob_end_flush();

    die();
}

if (isset($_GET['action']) && $_GET['action'] == 'download_csv') {
    // Handle CSV Export
    add_action('admin_init', 'csv_export');
}

add_filter('authors_autocomplete_mb_allow_user_role', 'filter_authors_autocomplete_mb_allow_user_role', 1, 4);

function filter_authors_autocomplete_mb_allow_user_role($allow_user_role, $user_role, $post_id, $post_type) {
    if (!in_array($user_role, array('subscriber', 'editor', 'author', 'contributor'))) {
        return false;
    }
    return $allow_user_role;
}

add_action('wp_ajax_getauthorname', 'my_ajax_getauthorname');

function my_ajax_getauthorname() {
    $pid = $_POST['pid'];
    $post = get_post($pid);
    $userDetails = get_userdata($post->post_author);
    echo $userDetails->user_login;
    // Don't forget to stop execution afterward.
    wp_die();
}

add_action('wp_head', 'gd_footer_scripts');

function gd_footer_scripts() {
    ?>
    <script>
        function trackForGoogleGoal(clubName) {
            NewclubName = "'" + clubName + "'";
            ga('send', 'event', {eventCategory: 'Club Enquiry', eventAction: 'click', eventLabel: NewclubName, eventValue: 1});
        }
    </script>
    <?php
}

//Custom --V
/*
 * Function to filter clubs with listings with local boost package ticked, then all the ones with the featured box ticked and then all the ones with web link box ticked
 * Action: restrict_manage_posts
 */
add_action('restrict_manage_posts', 'add_filter_on_club_list');
if (!function_exists('add_filter_on_club_list')) {

    function add_filter_on_club_list() {
        global $typenow;

        $club_filter = '';
        if (isset($_GET['club_filter']) && $_GET['club_filter'] != '0') {
            $club_filter = $_GET['club_filter'];
        }
        // echo $club_filter;exit;
        if ($typenow == 'estate_property') {
            ?>
            <select name="club_filter">
                <option value="0" selected="selected">Filter Clubs</option>
                <option value="featured_club" <?php
                if ($club_filter == 'featured_club') {
                    echo "selected='selected'";
                }
                ?>>Featured Clubs</option>
                <option value="weblink_club" <?php
                if ($club_filter == 'weblink_club') {
                    echo "selected='selected'";
                }
                ?>>Weblink Clubs</option>
                <option value="local_boost_club" <?php
                if ($club_filter == 'local_boost_club') {
                    echo "selected='selected'";
                }
                ?>>Local Boost Clubs</option>
            </select>
            <?php
        }
    }

}

//Custom --V
/*
 * Function to filter clubs post type list
 * Filter: parse_query
 */
add_filter('parse_query', 'parse_cottges_new_filter');

function parse_cottges_new_filter($query) {

    if (!(is_admin() AND $query->is_main_query())) {
        return $query;
    }

    if (!('estate_property' === $query->query['post_type'] )) {
        return $query;
    }


    if (isset($_GET['post_type']) && $_GET['post_type'] == 'estate_property') {

        if (isset($_GET['club_filter']) && $_GET['club_filter'] != '0') {
            $club_filter = $_GET['club_filter'];
            if ($club_filter != '0') {
                if ($club_filter == 'featured_club') {
                    $query->set('meta_query', [
                        [
                            'key' => 'prop_featured',
                            'value' => 1,
                            'compare' => '='
                        ]
                    ]);
                } elseif ($club_filter == 'weblink_club') {
                    $query->set('meta_query', [
                        [
                            'key' => 'prop_weblink',
                            'value' => 1,
                            'compare' => '='
                        ]
                    ]);
                } elseif ($club_filter == 'local_boost_club') {
                    $query->set('meta_query', [
                        [
                            'key' => 'prop_localman',
                            'value' => 1,
                            'compare' => '='
                        ]
                    ]);
                }
            }
        }
    }
    return $query;
}

//add_filter('custom_menu_order', function() {
//    return true;
//});
////add_filter('menu_order', 'clubhub_menu_order_update');
//
///**
// * Filters WordPress' default menu order
// */
//function clubhub_menu_order_update($menu_order) {
//
//    foreach ($menu_order as $key => $value) {
//        if ('edit.php?post_type=estate_property' == $value) {
//            $oldkey = $key;
//        }
//    }
//
//
//    $new_positions = array(
//        'users.php' => $oldkey + 1,
//    );
//
//    function move_element(&$array, $a, $b) {
//        $out = array_splice($array, $a, 1);
//        array_splice($array, $b, 0, $out);
//    }
//
//    foreach ($new_positions as $value => $new_index) {
//        if ($current_index = array_search($value, $menu_order)) {
//            move_element($menu_order, $current_index, $new_index);
//        }
//    }
//    return $menu_order;
//}

//Custom --V --end here

function console_log($output, $with_script_tags = true) {
    $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . ');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
}

/* * ******************* Add custom input field in bulk edit code ********************** */

add_filter('manage_posts_columns', 'manage_wp_posts_be_qe_manage_posts_columns', 100, 2);

function manage_wp_posts_be_qe_manage_posts_columns($columns, $post_type) {

    switch ($post_type) {

        case 'estate_property':

            // building a new array of column data
            $new_columns = array();

            foreach ($columns as $key => $value) {

                // default-ly add every original column
                $new_columns[$key] = $value;

                /**
                 * If currently adding the title column,
                 * follow immediately with our custom columns.
                 */
                if ($key == 'title') {
                    $new_columns['website_url'] = 'Website Link';
                    $new_columns['trustist_code'] = 'TRUSTist Code';
                    $new_columns['package_check'] = 'Discrepancies';
                }
            }
            return $new_columns;
    }

    return $columns;
}

//add_action('manage_estate_property_posts_custom_column', 'dt_manage_clubs_custom_column', 100, 2);


function dt_column_value_add_discrepancy($value, $id, AC\Column $column) {

    if ($column instanceof AC\Column\CustomField) {
        $meta_key = $column->get_meta_key();
        $userID = get_post_field('post_author', $id);
        $pay_status = get_the_author_meta('package_pay_status', $userID);
        if ($meta_key == "package_check") {
            $pack = get_the_author_meta('package_id', $userID);
            $unlimited_lists = get_the_author_meta('package_listings', $userID);
            $today = strtotime(date("Y-m-d H:i:s"));
        
            $value = "";
            
            $expired = wpestate_get_user_package_expiry_date($userID);
            
            
            if ($unlimited_lists >= 0) {
                $value .= " Limited listings,";
            }
            if (!($pack == 122972 || $pack == 120676 || $pack == 119535 || $pack == 137320)) {
                $value .= " Wrong package";
            }
            if (($pack == 122972 || $pack == 120676 || $pack == 119535 || $pack == 137320) && $unlimited_lists < 0 && $expired > $today && $pay_status != "paid") {
                $value .= " Possible Pay Status error";
            }
            if ($expired < $today) {
                $value .= " Expired: " . date('d-m-Y',  $expired);
            }
            if ($pay_status == "paid") {
                $value .= " $pay_status";
            }
        }
        if ($meta_key == 'pay_status') {
            if ($pay_status) {
                $value = $pay_status;
            } else {
                $value = "unpaid";
            }
        }
        //update_post_meta($id,'discrepancy',$value);
    }
    
    return $value;
}

add_filter('ac/column/value', 'dt_column_value_add_discrepancy', 10, 3);

add_action('pre_get_posts', 'dt_pre_get_posts', 1);

function dt_pre_get_posts($query) {

    if ($query->is_main_query() && ( $orderby = $query->get('orderby') )) {

        switch ($orderby) {

            // If we're ordering by 'wpcf-list-type'
            case 'website_url':

                // set our query's meta_key, which is used for custom fields
                $query->set('meta_key', 'website_url');

                $query->set('orderby', 'meta_value');

                break;
        }
    }
}

add_filter('posts_clauses', 'dt_posts_clauses', 1, 2);

function dt_posts_clauses($pieces, $query) {
    global $wpdb;

    if ($query->is_main_query() && ( $orderby = $query->get('orderby') )) {

        // Get the order query variable - ASC or DESC
        $order = strtoupper($query->get('order'));

        // Make sure the order setting qualifies. If not, set default as ASC
        if (!in_array($order, array('ASC', 'DESC')))
            $order = 'ASC';

        switch ($orderby) {

            // If we're ordering by release_date
            case 'website_url':

                $pieces['join'] .= " LEFT JOIN $wpdb->postmeta wp_rd ON wp_rd.post_id = {$wpdb->posts}.ID AND wp_rd.meta_key = 'website_url'";

                // Then tell the query to order by our date
                $pieces['orderby'] = "STR_TO_DATE( wp_rd.meta_value,'%m/%d/%Y' ) $order, " . $pieces['orderby'];

                break;
        }
    }
    return $pieces;
}

add_action('bulk_edit_custom_box', 'dt_bulk_quick_edit_custom_box', 10, 2);

//add_action( 'quick_edit_custom_box', 'manage_wp_posts_be_qe_bulk_quick_edit_custom_box', 10, 2 );
function dt_bulk_quick_edit_custom_box($column_name, $post_type) {
    //echo $column_name;
    switch ($post_type) {

        case 'estate_property':

            switch ($column_name) {

                case 'website_url':
                    ?><fieldset class="inline-edit-col-left">
                        <div class="inline-edit-col">
                            <label>
                                <span class="title">Website Link</span>
                                <span class="input-text-wrap">
                                    <input class="websiteinput" type="text" name="website_url" value="">
                                </span>
                            </label>
                        </div>
                    </fieldset>
                    <?php
                    break;

                case 'trustist_code':
                    ?><fieldset class="inline-edit-col-left">
                        <div class="inline-edit-col">
                            <label>
                                <span class="title">TRUSTist Code</span>
                                <span class="input-text-wrap">
                                    <input class="trustistinput" type="text" name="trustist_code" value="">
                                </span>
                            </label>
                        </div>
                    </fieldset>
                    <?php
                    break;
            }
            break;
    }
}

add_filter('manage_estate_property_posts_columns', 'dt_add_columns', 100);

/**
 * Add columns to management page
 *
 * @param array $columns
 *
 * @return array
 */
function dt_add_columns($columns) {
    $columns['website_url'] = 'Website URL';
    return $columns;
}

//add_action( 'admin_footer', 'quick_edit_javascript' );
function quick_edit_javascript() {
    global $current_screen;

    if ('estate_property' != $current_screen->post_type) {
        return;
    }
    ?>
    <script type="text/javascript">
        function custom_fields_load(webValue, trustValue) {
            inlineEditPost.revert();
            jQuery(function () {
                jQuery('.trustistinput').val(trustValue);
                jQuery('.websiteinput').val('webValue');

            })
        }
    </script>
    <?php
}

add_action('admin_print_scripts-edit.php', 'manage_wp_posts_be_qe_enqueue_admin_scripts');

function manage_wp_posts_be_qe_enqueue_admin_scripts() {

    wp_enqueue_script("my-ajax-handle", get_stylesheet_directory_uri() . "/js/populate.js", array('jquery', 'inline-edit-post'), '', true);
}

//add_filter( 'post_row_actions', 'expand_quick_edit_link', 10, 2 );

/**
 * Pass custom field values to custom_fields_load javascript function
 *
 * @param array $actions
 * @param array $post
 *
 * @return array
 */
function expand_quick_edit_link($actions, $post) {
    global $current_screen;

    if ('estate_property' != $current_screen->post_type) {
        return $actions;
    }

    $webdata = get_post_meta($post->ID, 'website_url', true);
    $trustdata = get_post_meta($post->ID, 'trustist_code', true);
    $actions['inline hide-if-no-js'] = '<a href="#" class="editinline" title="';
    $actions['inline hide-if-no-js'] .= esc_attr('Edit this item inline') . '"';
    $actions['inline hide-if-no-js'] .= " onclick=\"custom_fields_load('{$webdata}',' {$trustdata}')\" >";
    $actions['inline hide-if-no-js'] .= 'Quick Edit';
    $actions['inline hide-if-no-js'] .= '</a>';

    return $actions;
}

add_action('save_post', 'manage_wp_posts_be_qe_save_post', 10, 2);

function manage_wp_posts_be_qe_save_post($post_id, $post) {
    // pointless if $_POST is empty (this happens on bulk edit)

    switch ($post->post_type) {

        case 'estate_property':

            /**
             * Because this action is run in several places, checking for the array key
             * keeps WordPress from editing data that wasn't in the form, i.e. if you had
             * this post meta on your "Quick Edit" but didn't have it on the "Edit Post" screen.
             */
            $url_fields = array('website_url');
            $trust_fields = array('trustist_code');
            $membership_fields = array('package_id');
            $date_fields = array('package_activation');
            $invoice_fields = array('invoice_payment');
            $local_boost_fields = array('prop_localman');
            $featured_fields = array('prop_featured');
            $weblink_fields = array('prop_weblink');

            foreach ($local_boost_fields as $field) {
                if (isset($_REQUEST[$field]) && $_REQUEST[$field] != '') {
                    update_post_meta($post_id, $field, $_REQUEST[$field]);
                }
            }foreach ($featured_fields as $field) {
                if (isset($_REQUEST[$field]) && $_REQUEST[$field] != '') {
                    update_post_meta($post_id, $field, $_REQUEST[$field]);
                }
            }foreach ($weblink_fields as $field) {
                if (isset($_REQUEST[$field]) && $_REQUEST[$field] != '') {
                    update_post_meta($post_id, $field, $_REQUEST[$field]);
                }
            }
            
            
            foreach ($url_fields as $field) {
                if (isset($_REQUEST[$field]) && $_REQUEST[$field] != '') {
                    update_post_meta($post_id, $field, $_REQUEST[$field]);
                }
            }

            foreach ($trust_fields as $field) {
                if (isset($_REQUEST[$field]) && $_REQUEST[$field] != '') {
                    update_post_meta($post_id, $field, $_REQUEST[$field]);
                }
            }

            foreach ($membership_fields as $field) {
                if (isset($_REQUEST[$field]) && $_REQUEST[$field] != '') {
                    $author_id = $post->post_author;
                    update_user_meta($author_id, $field, $_REQUEST[$field]);
                    if ($field == 'package_id' && isset($_REQUEST[$field])) {
                        $get_pkg_name = wpestate_get_package_name_by_id($_REQUEST[$field]);
                        if ($get_pkg_name) {
                            update_user_meta($author_id, 'package_name', $get_pkg_name);
                            update_user_meta($author_id, 'package_pay_status', 'paid');
                        }
                    }

                    if ($field != "0") {
                        update_user_meta($author_id, 'package_listings', '-1');
                    }
                }
            }
            foreach ($invoice_fields as $field) {
                $author_id = $post->post_author;
                if (isset($_REQUEST[$field]) && $_REQUEST[$field] != '') {
                    update_user_meta($author_id, $field, "Invoice");
                } else {
                    update_user_meta($author_id, $field, '');
                }
            }
            foreach ($date_fields as $field) {
                if (isset($_REQUEST[$field]) && $_REQUEST[$field] != '') {
                    $author_id = $post->post_author;
                    update_user_meta($author_id, $field, $_REQUEST[$field]);
                }
            }

            break;
    }
}

add_action('wp_ajax_manage_wp_posts_using_bulk_quick_save_bulk_edit', 'manage_wp_posts_using_bulk_quick_save_bulk_edit');

function manage_wp_posts_using_bulk_quick_save_bulk_edit() {
    
    $post_ids = (!empty($_POST['post_ids']) ) ? $_POST['post_ids'] : NULL;

    // if we have post IDs
    if (!empty($post_ids) && is_array($post_ids)) {

        // get the custom fields
        $custom_field = (!empty($_POST['website_url'])) ? $_POST['website_url'] : NULL;
        $custom_field_2 = (!empty($_POST['trustist_code'])) ? $_POST['trustist_code'] : NULL;

        if (!empty($custom_field)) {

            foreach ($post_ids as $post_id) {

                $check_exist = get_post_meta($post_id, 'website_url');

                if ($check_exist) {
                    update_post_meta($post_id, 'website_url', $custom_field);
                } else {
                    add_post_meta($post_id, 'website_url', $custom_field);
                }
            }
        }

        if (!empty($custom_field_2)) {

            foreach ($post_ids as $post_id) {

                $check_exist = get_post_meta($post_id, 'trustist_code');


                if ($check_exist) {
                    update_post_meta($post_id, 'trustist_code', $custom_field);
                } else {
                    add_post_meta($post_id, 'trustist_code', $custom_field);
                }
            }
        }
    }
}

add_action('rest_api_init', function () {

    register_rest_route('wp/v1', '/add-booking/', array(
        'methods' => 'POST',
        'callback' => 'add_form_booking',
        'permission_callback' => '__return_true', //needed from 5.5
    ));
});

function add_form_booking($request) {

    $response = array();
    $parameters = $request->get_json_params();
    $json = json_decode(file_get_contents('php://input'), true);
    //print_r($json); die();

    $allowded_html = array();
    $usremail = '';
    $user_id = sanitize_text_field($json['user_id']);
    $usremail = sanitize_text_field($json['usremail']);
    $listing_edit = sanitize_text_field($json['listing_edit']);
    $type = sanitize_text_field($json['type']);


    $userdata = get_user_by('id', $user_id);
    $from = $userdata->user_login;

    $status = 'pending';

    $usremail = wp_kses($parameters['usremail'], $allowded_html);
    $booking_guest_no = 0;

    $property_id = intval($parameters['listing_edit']);
    $post_data = get_post($property_id);
    $owner_id = $post_data->post_author;

    if ($type == "book") {
        $event_name = esc_html__('Booking Request', 'wpestate');
    } else if ($type == "contact") {
        $event_name = esc_html__('Contact Request', 'wpestate');
    } else if ($type == "facebook") {
        $event_name = esc_html__('Facebook Request', 'wpestate');
    } else if ($type == "twitter") {
        $event_name = esc_html__('Twitter Request', 'wpestate');
    } else if ($type == "facebook") {
        $event_name = esc_html__('Instagram Request', 'wpestate');
    }
    $post = array(
        'post_title' => $event_name,
        'post_content' => $usremail,
        'post_status' => 'publish',
        'post_type' => 'wpestate_booking',
        'post_author' => $user_id
    );
    $post_id = wp_insert_post($post);

    $post = array(
        'ID' => $post_id,
        'post_title' => $event_name . ' ' . $post_id
    );
    wp_update_post($post);

    update_post_meta($post_id, 'booking_id', $property_id);
    update_post_meta($post_id, 'type', 'emailonly');
    update_post_meta($post_id, 'owner_id', $owner_id);

    $subject = esc_html__('New Booking Request from ', 'wpestate');
    $description = esc_html__('You have received a new booking request', 'wpestate');
    $from = $current_user->ID;
    $to = $owner_id;

    $receiver = get_userdata($owner_id);
    $receiver_email = $receiver->user_email;
    //$receiver_email    =   $receiver->user_email.',krishankmr.bbdit01@gmail.com';
    //$receiver_email = 'krishankmr.bbdit01@gmail.com';
    //print " email to ".$receiver_email.' pr id '.$property_id;
    //print $userID." / ".$userID."/".$to;
    wpestate_add_to_inbox($user_id, $user_id, $to, $subject, $description, "external_book_req");
    wpestate_send_booking_email('newbook', $receiver_email, $property_id);


    $content_post = get_post($post_id);
    $response['data'] = $content_post;


    return new WP_REST_Response($response, 123);
}

add_action('rest_api_init', function () {

    register_rest_route('wp/v1', '/book-now/', array(
        'methods' => 'GET',
        'callback' => 'book_now_form',
        'permission_callback' => '__return_true',
    ));
});

function book_now_form() {
    
}

// update 10 02
// If post type is wpestate_invoice add filter button to export all invoices as CSV
// function:: clubhub_download_invoice
// hook:: restrict_manage_posts
function clubhub_download_invoice() {
    $type = 'post';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }

    // Export to CSV button
    if ($type == 'wpestate_invoice') {
        $url = add_query_arg(array('clubhub_export_invoice' => 'csv'));
        echo '<a href="' . esc_url($url) . '" class="button alignright button-primary sliced-export-csv" >' . __('Export as CSV', 'sliced-invoices') . '</a>';
    }
}

add_action('restrict_manage_posts', 'clubhub_download_invoice');


// update 10 02
// Handle CSV creation and export
// function:: clubhub_export_csv
// hook:: admin_init
add_action('admin_init', 'clubhub_export_csv');

function clubhub_export_csv() {
    if ( !array_key_exists( 'clubhub_export_invoice', $_GET ) || $_GET['clubhub_export_invoice'] !== 'csv' ) return;

    //set up columns
    $header_row = array(
        0 => __('Invoice ID', 'clubhub'),
        1 => __('Club Name', 'clubhub'),
        2 => __('Price', 'clubhub'),
        3 => __('Billing For', 'clubhub'),
        4 => __('Invoice Type', 'clubhub'),
        5 => __('Status', 'clubhub'),
        6 => __('Purchased By', 'clubhub'),
        7 => __('Purchase Date', 'clubhub'),
    );

    $data_rows = array();

    //get all invoices
    $args = array(
        'post_type' => 'wpestate_invoice',
        'posts_per_page' => -1,
        'post_status' => 'publish',
    );
    $the_query = new WP_Query($args);

    // loop through invoice and prepare data
    if ($the_query->have_posts()) {
        while ($the_query->have_posts()) {
            $the_query->the_post();
            $row = array();
            $row[0] = get_the_title();
            $row[1] = get_the_title(get_post_meta(get_the_ID(), 'item_id', true));
            $row[2] = get_post_meta(get_the_ID(), 'item_price', true);
            $row[3] = get_post_meta(get_the_ID(), 'invoice_type', true);
            $row[4] = get_post_meta(get_the_ID(), 'biling_type', true);
            $status = esc_html(get_post_meta(get_the_ID(), 'invoice_status', true));
            if ($status === 'confirmed') {
                $status = $status . ' / ' . esc_html__('paid', 'wpestate');
            }
            $row[5] = $status;
            $purchased_by = get_userdata(get_post_meta(get_the_ID(), 'buyer_id', true));
            $purchase_email = '';
            if (!empty($purchased_by)) {
                $purchase_email = $purchased_by->user_email;
            }
            $row[6] = $purchase_email;

            $row[7] = get_post_meta(get_the_ID(), 'purchase_date', true);
            $data_rows[] = $row;
        }
    }

    //set up export filename
    $filename = sanitize_file_name($type . '-export-' . date('Y-m-d') . '.csv');

    //setup csv header
    clubhub_set_csv_headers($filename);

    // go feed them the CSV
    $fh = @fopen('php://output', 'w');
    fprintf($fh, chr(0xEF) . chr(0xBB) . chr(0xBF));
    fputcsv($fh, $header_row);

    foreach ($data_rows as $data_row) {
        fputcsv($fh, $data_row);
    }

    fclose($fh);
    die();
}

//setup CSV export file headers
function clubhub_set_csv_headers($file_name) {
    //set headers for csv
    $now = date("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    /*
     * Forces the download
     */
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    /*
     * disposition / encoding on response body
     */
    header("Content-Disposition: attachment;filename={$file_name}");
    header("Content-Transfer-Encoding: binary");
}

// update 11 02 2020
// Shortcode to display report club button
// function:: report_broken_club
// shortcode:: clubhub_broken_club
function report_broken_club() {
    global $post;
    return "<div style='width: 250px;margin: 0 auto;'><button type='button' id='report_club' class='button' data-listingid='" . $post->ID . "'>Report club</button></div>";
}

add_shortcode('clubhub_broken_club', 'report_broken_club');

/*
 * @added by gd
 * @purpose creating monday to tuesday array
 */

function getMonSunDaysOptions() {
    $array = array(
        'monday' => 'Monday',
        'tuesday' => 'Tuesday',
        'wednesday' => 'Wednesday',
        'thursday' => 'Thursday',
        'friday' => 'Friday',
        'saturday' => 'Saturday',
        'sunday' => 'Sunday'
    );

    return $array;
}

function add_age_field_meta_box() {
    add_meta_box(
            'your_fields_meta_box', // $id
            'Your Fields', // $title
            'show_your_fields_meta_box', // $callback
            'Club', // $screen
            'normal', // $context
            'high' // $priority
    );
}

add_action('add_meta_boxes', 'add_age_field_meta_box');

add_filter('style_loader_src', 'sdt_remove_ver_css_js', 9999, 2);
add_filter('script_loader_src', 'sdt_remove_ver_css_js', 9999, 2);

function sdt_remove_ver_css_js($src, $handle) {
    $handles_with_version = ['style']; // <-- Adjust to your needs!

    if (strpos($src, 'ver=') && !in_array($handle, $handles_with_version, true))
        $src = remove_query_arg('ver', $src);

    return $src;
}

// CUSTOM Media template //EDIT David T: override_media_templates not found on post.php edit load
//add_action('wp_enqueue_media', 'add_media_overrides', 99);
//
//function add_media_overrides() {
//    add_action('admin_footer-post.php', 'override_media_templates', 999);
//}

add_action('admin_footer', 'club_attachment_list_template');

function club_attachment_list_template() {
    ?>
    <script type="text/html" id="tmpl-attachment_custom">
        <div class="attachment-preview js--select-attachment type-{{ data.type }} subtype-{{ data.subtype }} {{ data.orientation }}">
            <div class="thumbnail">
                <# if ( data.uploading ) { #>
                <div class="media-progress-bar"><div style="width: {{ data.percent }}%"></div></div>
                <# } else if ( 'image' === data.type && data.sizes ) { #>
                <div class="centered">
                    <img src="{{ data.size.url }}" draggable="false" alt=""  style="height: 90%; transform: translate(-50%,-55%);"/>
                    <div style="
                         transform: translate(-50%,-100%);
                         background-color: black;
                         color: white;
                         margin-top: 50%;
                         font-size: 80%;
                         z-index: 9999;
                         position: relative;
                         vertical-align: middle;
                         ">{{ data.filename }}</div>
                </div>
                <# } else { #>
                <div class="centered 112">

                    <# if ( data.image && data.image.src && data.image.src !== data.icon ) { #>
                    <img src="{{ data.image.src }}" class="thumbnail 0" draggable="false" alt="" />
                    <# } else if ( data.sizes && data.sizes.medium ) { #>
                    <img src="{{ data.sizes.medium.url }}" class="thumbnail 01" draggable="false" alt="" />
                    <# } else if ( data.size.url != '' && ( (data.size.url.indexOf('.png') >= 0) || (data.size.url.indexOf('.jpg') >= 0) || (data.size.url.indexOf('.jpeg') >= 0) || (data.size.url.indexOf('.gif') >= 0) || (data.size.url.indexOf('.tiff') >= 0) ) ) { #>
                    <img src="{{data.size.url}}" class="thumbnail 03" draggable="false" alt="" />
                    <# } else { #>
                    <img src="{{ data.icon }}" class="icon" draggable="false" alt="" />
                    <# } #>
                </div>
                <div class="filename">
                    <div>{{ data.filename }}</div>
                </div>
                <# } #>
            </div>
            <# if ( data.buttons.close ) { #>
            <button type="button" class="button-link attachment-close media-modal-icon"><span class="screen-reader-text"><?php _e('Remove'); ?></span></button>
            <# } #>
        </div>
        <# if ( data.buttons.check ) { #>
        <button type="button" class="check" tabindex="-1"><span class="media-modal-icon"></span><span class="screen-reader-text"><?php _e('Deselect'); ?></span></button>
        <# } #>
        <#
        var maybeReadOnly = data.can.save || data.allowLocalEdits ? '' : 'readonly';
        if ( data.describe ) {
        if ( 'image' === data.type ) { #>
        <input type="text" value="{{ data.caption }}" class="describe" data-setting="caption"
               placeholder="<?php esc_attr_e('Caption this image&hellip;'); ?>" {{ maybeReadOnly }} />
               <# } else { #>
               <input type="text" value="{{ data.title }}" class="describe" data-setting="title"
               <# if ( 'video' === data.type ) { #>
               placeholder="<?php esc_attr_e('Describe this video&hellip;'); ?>"
               <# } else if ( 'audio' === data.type ) { #>
               placeholder="<?php esc_attr_e('Describe this audio file&hellip;'); ?>"
               <# } else { #>
               placeholder="<?php esc_attr_e('Describe this media file&hellip;'); ?>"
               <# } #> {{ maybeReadOnly }} />
               <# }
               } #>
    </script>
    <script>
        jQuery(document).ready(function ($) {
            if (typeof wp.media.view.Attachment != 'undefined') {
                wp.media.view.Attachment.prototype.template = wp.media.template('attachment_custom');
            }
        });
    </script>

    <?php
}

// set full size to avoide the broken thumbnail issue
function change_featured_image_thumbail_size($size, $thumbnail_id, $post) {
    return 'full';
}

add_filter('admin_post_thumbnail_size', 'change_featured_image_thumbail_size', 10, 3);





add_action('admin_head', 'club_admin_custom_css');

function club_admin_custom_css() {
    echo '<style>
    table.media .column-title .media-icon img {
        width: 100%;   
    }
  </style>';
}

//add_action('wp_footer','addtrustitcode');
function addtrustitcode() {
    if (!is_front_page()) {
        ?>
        <!--START OF TRUSTist REVIEWer CODE - DO NOT CHANGE-->
        <script src="https://widget.trustist.com/sLlEIaQ4jUKULBnZvTvSFw/trustistreviewer.js"></script>
        <div ts-widget="big" ts-border-radius="15px" ts-suppress-review-link="true" ts-reviews-url="https://clubhubuk.co.uk/"></div>
        <!--END OF TRUSTist REVIEWer CODE-->
        <?php
    }
}

function dt_registration_redirect() {
    return home_url('#');
}
add_filter('registration_redirect', 'dt_registration_redirect');

function dt_login_redirect() {
    return home_url('my-profile/');
}
add_filter('login_redirect', 'dt_login_redirect');
//update users with the updated membership package to ensure they get unlimited listings on their users profiles
function dt_update_user_unl_listings($post_id) {
    global $wpdb;
    $unlimited = get_post_meta($post_id, 'mem_list_unl');
    $packName = get_the_title($post_id);
    $users = get_users(array('search' => array($post_id)));

    foreach ($users as $user) {
        $pack = $user->package_id;

        if ($pack == 122972 || $pack == 120676 || $pack == 119535 || $pack == 137320) {
            update_user_meta($user->ID, 'package_listings', $unlimited[0]);
            if ( dt_is_paid($user) ) {
                update_user_meta($user->ID, 'package_pay_status', 'paid');
            } else {
                update_user_meta($user->ID, 'package_pay_status', 'unpaid');
            }
            update_user_meta($user->ID, 'package_name', $packName);
        } else {
            update_user_meta($user->ID, 'package_listings', 0);
            if (get_user_meta($user->ID, 'package_name') == $packName) {
                update_user_meta($user->ID, 'package_id', $post_id);
            }
        }
    }
}

add_action('save_post_membership_package', 'dt_update_user_unl_listings', 20, 1);

function dt_get_pay_status($uid) {
    return get_user_meta($uid, 'package_pay_status', true);
}
function dt_is_paid($user) { //check if a user has the 'paid' status, if blank assign 'not paid' status in hopes it will then be picked up if incorrect
    if (!$user) $user = wp_get_current_user();
    if (!user) return;
    
    $paid = dt_get_pay_status($user->ID);
    if ( empty($paid) ) { 
        update_user_meta($user->ID, 'package_pay_status', 'not paid', true);
        $paid = 'not paid';
    }
    if ( $paid === 'paid') {
        return true;
    } else if ( $paid === 'not paid') {
        return false;
    }
}

function wpestate_get_user_package_expiry_date($user_id = FALSE) {
    if (!$user_id) {
        return FALSE;
    }
    $get_pkg_id = get_user_meta($user_id, 'package_id', TRUE);
    if (!$get_pkg_id) {
        return FALSE;
    }
    $get_activation_date = get_user_meta($user_id, 'package_activation', TRUE);
    $get_pkg_valid_session = get_post_meta($get_pkg_id, 'biling_period', TRUE);
    $get_pkg_freq = get_post_meta($get_pkg_id, 'billing_freq', TRUE);
    $pkg_valid_session = 0;
    if ($get_pkg_valid_session && $get_activation_date) {
        $get_pkg_valid_session = strtolower($get_pkg_valid_session);

        $re_day = $get_pkg_valid_session == 'day' ? 1 : ($get_pkg_valid_session == 'week' ? 7 : ($get_pkg_valid_session == 'month' ? 30 : ($get_pkg_valid_session == 'year' ? 365 : 0)));
        $re_day = $re_day * $get_pkg_freq;
        $pkg_valid_session_ = date('Y-m-d H:i:s', strtotime($get_activation_date . " + $re_day day"));
        $pkg_valid_session = strtotime($pkg_valid_session_);
    }
    return $pkg_valid_session;
}

function wpestate_get_property_author_ids($pay_status = 'paid') {
    global $wpdb;
    $posts_tbl = $wpdb->prefix . 'posts';
    $postmeta_tbl = $wpdb->prefix . 'postmeta';
    if ($pay_status == 'none') {
//        $squl_query = "SELECT p.post_author user_id FROM `$posts_tbl` p LEFT JOIN `$postmeta_tbl` pm ON p.ID = pm.post_id AND p.post_status = 'publish' AND p.post_type = 'estate_property' WHERE pm.meta_key != 'pay_status' GROUP BY p.post_author ORDER BY `p`.`post_author`  ASC";
        $squl_query = "SELECT p.post_author user_id FROM `$posts_tbl` p LEFT JOIN `$postmeta_tbl` pm ON p.ID = pm.post_id AND p.post_status = 'publish' AND p.post_type = 'estate_property' WHERE pm.meta_key != 'pay_status' GROUP BY p.post_author ORDER BY `p`.`post_author`  ASC";
    } else {
        $squl_query = "SELECT p.post_author user_id FROM `$posts_tbl` p LEFT JOIN `$postmeta_tbl` pm ON p.ID = pm.post_id AND p.post_status = 'publish' AND p.post_type = 'estate_property' WHERE pm.meta_key = 'pay_status' AND pm.meta_value = '$pay_status' GROUP BY p.post_author ORDER BY `p`.`post_author`  ASC";
    }
    $get_property_author = $wpdb->get_results($squl_query, 'ARRAY_A');
    if ($get_property_author) {
        return $get_property_author;
    }

    return FALSE;
}

function wpestate_get_property_ids_by_ex_authors($user_id) {
    if (!$user_id || $user_id == 0) {
        return FALSE;
    }
    global $wpdb;
    $posts_tbl = $wpdb->prefix . 'posts';
    $postmeta_tbl = $wpdb->prefix . 'postmeta';

    $sql_query = "SELECT p.ID pkg_id, pm.meta_value pay_status FROM `$posts_tbl` p LEFT JOIN `$postmeta_tbl` pm ON p.ID = pm.post_id AND p.post_status = 'publish' AND p.post_type = 'estate_property' WHERE pm.meta_key = 'pay_status' AND pm.meta_value = 'paid' AND p.post_author = $user_id ORDER BY p.ID ASC";
    $get_properties = $wpdb->get_results($sql_query, 'ARRAY_A');
    if ($get_properties) {
        return $get_properties;
    }
}

function wpestate_get_all_users_by_pay_status($status = 'paid') {
    global $wpdb;
    $user_tbl = $wpdb->prefix . 'users';
    $user_meta_tbl = $wpdb->prefix . 'usermeta';
    if ($status == 'all') {
        $query_string = "SELECT u.ID id, um.meta_value pay_status FROM `$user_tbl` u LEFT JOIN `$user_meta_tbl` um ON u.ID = um.user_id AND um.meta_key = 'package_pay_status ORDER BY u.ID";
    } elseif ($status == 'have') {
        $query_string = "SELECT u.ID id, um.meta_value pay_status FROM `$user_tbl` u LEFT JOIN `$user_meta_tbl` um ON u.ID = um.user_id AND um.meta_key = 'package_pay_status' WHERE um.meta_value != '' ORDER BY u.ID";
    } elseif ($status == 'empty') {
        $query_string = "SELECT u.ID id, um.meta_value pay_status FROM `$user_tbl` u LEFT JOIN `$user_meta_tbl` um ON u.ID = um.user_id AND um.meta_key = 'package_pay_status' WHERE um.meta_value = '' ORDER BY u.ID";
    } else {
        $query_string = "SELECT u.ID id, um.meta_value pay_status FROM `$user_tbl` u LEFT JOIN `$user_meta_tbl` um ON u.ID = um.user_id AND um.meta_key = 'package_pay_status' WHERE um.meta_value = '$status' ORDER BY u.ID";
    }
    $get_users = $wpdb->get_results($query_string, 'ARRAY_A');
    if ($get_users) {
        return $get_users;
    }
    return FALSE;
}

function wpestate_change_property_status($user_id, $status = 'unpaid') {
    if (!$user_id || $user_id == 0) {
        return FALSE;
    }
    $get_properties = wpestate_get_property_ids_by_ex_authors($user_id);
    update_user_meta($user_id, 'package_pay_status', $status);
    if ($get_properties) {
        $responce = [];
        foreach ($get_properties as $get_property) {
            if (strtolower($get_property['pay_status']) != $status) {
                update_post_meta($get_property['pkg_id'], 'pay_status', $status);
                $responce[$get_property['pkg_id']] = 'status change to ' . $status;
            } else {
                $responce[$get_property['pkg_id']] = 'exist status: ' . $status;
            }
        }
        return $responce;
    }
    return FALSE;
}

function wpestate_get_package_name_by_id($package_id) {
    if (!$package_id) {
        return FALSE;
    }
    $pkg_title = get_the_title($package_id);
    return $pkg_title;
}

function wpestate_get_package_id_by_name($package_name) {
    if (!$package_name) {
        return FALSE;
    }
    if ($package_name == "Monthly Membership") {
       $pkg_id = 137320;
    }
    if ($package_name == "New Membership") {
       $pkg_id = 122972;
    }
    if ($package_name == "Charity Membership") {
       $pkg_id = 120676;
    }
    if ($package_name == "Discounted Membership") {
       $pkg_id = 119535;
    }
    return $pkg_id;
}

function wpestate_set_package_name_on_user_field($user_id, $remove_name = FALSE) {
    if (!$user_id) {
        return FALSE;
    }
    $res = '';
    $get_pkg_id = get_user_meta($user_id, 'package_id', TRUE);
    $get_pkg_name = wpestate_get_package_name_by_id($get_pkg_id);
    $set_pkg_name = '';
    if ($remove_name) {
        update_user_meta($user_id, 'package_name', '');
        $res = 'remove';
    } else {
        if ($get_pkg_name) {
            $get_pkg = get_user_meta($user_id, 'package_name', TRUE);
            if ($get_pkg) {
                if ($get_pkg != $get_pkg_name) {
                    $set_pkg_name = $get_pkg_name;
                }
            } else {
                $set_pkg_name = $get_pkg_name;
            }
        } else {
            $res = 'not found';
        }
        if (!empty($set_pkg_name)) {
            update_user_meta($user_id, 'package_name', $set_pkg_name);
            $res = 'add';
        } else {
            $res = 'exist';
        }
    }

    return $res;
}

function ch_get_paypal_invoices($page = 1) {
    $paypal_status = esc_html(get_option('wp_estate_paypal_api', ''));

    $host = 'https://sandbox.paypal.com';
    if ($paypal_status == 'live') {
        $host = 'https://api.paypal.com';
    }

    $url = $host . '/v1/oauth2/token';
    $postArgs = 'grant_type=client_credentials';
    $token = wpestate_get_access_token($url, $postArgs);

    $url = $host . '/v2/invoicing/invoices?total_required=true&page=' . $page . '&page_size=100';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Authorization: Bearer ' . $token;
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);
    $result = json_decode($result);
    return $result;
}

function ch_get_paypal_payments($page = 0, $start_date = '', $end_date = '') {
    $paypal_status = esc_html(get_option('wp_estate_paypal_api', ''));

    $host = 'https://sandbox.paypal.com';
    if ($paypal_status == 'live') {
        $host = 'https://api.paypal.com';
    }
    $url = $host . '/v1/oauth2/token';
    $postArgs = 'grant_type=client_credentials';
    $token = wpestate_get_access_token($url, $postArgs);

    if (empty($end_date)) {
        $end_date = date('d-m-Y h:i:s', strtotime('now'));
    }
    if (empty($start_date)) {
        $start_date = date('d-m-Y h:i:s', strtotime($end_date . ' - 90 days'));
    }

    $start_date_p = ch_paypal_time_format($start_date);
    $end_date_p = ch_paypal_time_format($end_date);

    $url = $host . "/v1/payments/payment?count=20&start_index={$page}&start_time={$start_date_p}&end_time={$end_date_p}&sort_by=create_time";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    $headers = [];
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Authorization: Bearer ' . $token;
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);
    $result = json_decode($result);
    return $result;
}

function ch_get_paypal_all_payments($is_refresh = TRUE, $num_days = 0) {

    if (!ch_paypal_payment_data_is_table_exists()) {
        ch_paypal_payment_data_table_create();
    }
    $get_items_opt = get_option('ch_paypal_get_payments_data');
    $get_items = [];
    $limit = 20;
    if ($is_refresh) {
        for ($x = 0; $x <= $limit;) {
            $str_time_parm = 'now';
            if ($num_days > 0) {
                $str_time_parm = "- $num_days days";
            }
            $end_date = date('Y-m-d h:i:s', strtotime($str_time_parm));
            $get_payments = ch_get_paypal_payments($x, '', $end_date);
            if (!empty($get_payments->payments)) {
                $get_items[] = $get_payments->payments;
            }
            if ($get_payments->count >= 20) {
                $limit += 20;
            }
            $x += 20;
        }
    } elseif (!$get_items_opt) {
        $is_refresh = TRUE;
        for ($x = 0; $x <= $limit;) {
            $str_time_parm = 'now';
            if ($num_days > 0) {
                $str_time_parm = "- $num_days days";
            }
            $end_date = date('Y-m-d h:i:s', strtotime($str_time_parm));
            $get_payments = ch_get_paypal_payments($x, '', $end_date);
            if (!empty($get_payments->payments)) {
                $get_items[] = $get_payments->payments;
            }
            if ($get_payments->count >= 20) {
                $limit += 20;
            }
            $x += 20;
        }
    } else {
        $get_items = $get_items_opt;
    }

    $data = [];
    $user_ids = [];
    if (!empty($get_items)) {
        if ($is_refresh) {
            update_option('ch_paypal_get_payments_data', $get_items);
            foreach ($get_items as $get_payments) {
                foreach ($get_payments as $get_payment) {
                    $related_resources = $get_payment->transactions[0]->related_resources;
                    $sales = $related_resources[0]->sale;
                    $sales_data = [
                        'id' => $sales->id,
                        'state' => $sales->state,
                        'amount' => $sales->amount->total,
                        'create_time' => $sales->create_time,
                        'update_time' => $sales->update_time,
                    ];
                    $refund = isset($related_resources[1]->refund) ? $related_resources[1]->refund : '';
                    $refund_data = !empty($refund) ? [
                        'id' => $refund->id,
                        'sale_id' => $refund->state,
                        'state' => $refund->sale_id,
                        'amount' => $refund->amount->total,
                        'create_time' => $refund->create_time,
                        'update_time' => $refund->update_time,
                            ] : '';

                    $item_type = $get_payment->transactions[0]->item_list->items[0]->name;
                    $item_name = $get_payment->transactions[0]->item_list->items[0]->sku;
                    if (strpos($item_name, $item_type)) {
                        $item_name = str_replace($item_type, '', $item_name);
                    }

                    $customer_email = trim(esc_attr($get_payment->payer->payer_info->email));
                    $customer_user = get_user_by('email', $customer_email);

                    $user_data = [
                        'payid' => $get_payment->id,
                        'cart' => $get_payment->cart,
                        'customer_id' => $get_payment->payer->payer_info->payer_id,
                        'customer_name' => $get_payment->payer->payer_info->first_name . ' ' . $get_payment->payer->payer_info->last_name,
                        'customer_email' => $customer_email,
                        'customer_user_id' => $customer_user ? $customer_user->ID : 'N/A',
                        'customer_pay_account' => $get_payment->payer->status,
                        'item_type' => $item_type,
                        'item_name' => trim($item_name),
                        'total_ammount' => $get_payment->transactions[0]->amount->total,
                        'currency' => $get_payment->transactions[0]->amount->currency,
                        'create_time' => $get_payment->create_time,
                        'update_time' => $get_payment->update_time,
                        'sale' => $sales_data,
                        'refund' => $refund_data,
                    ];
                    if (ch_paypal_payment_data_check_row($get_payment->id)) {
                        ch_paypal_payment_data_update($user_data);
                    } else {
                        ch_paypal_payment_data_insert($user_data);
                    }

                    if ($customer_user) {
                        $item_type = strtolower($item_type);
                        if ($item_type === 'membership payment') {
                            $user_ids[] = $customer_user->ID;
                            update_user_meta($customer_user->ID, 'ch_paypal_membership_payid', $get_payment->id);
                            update_user_meta($customer_user->ID, 'ch_paypal_membership_transaction_data', $user_data);
                        }
                    }
                    $data[] = $user_data;
                }
            }
        }
    }
    if (!empty($data)) {
        update_option('ch_paypal_get_payments', $data);
    } else {
        $data = get_option('ch_paypal_get_payments');
    }
    return $data;
}

function dev_test_shortcode_callback() {
    echo '<style>pre{display: block;width: 100%;}</style>';
    // echo do_shortcode('[wpestate_check_property_package_validation]');
//    $get_all_payments = ch_get_paypal_all_payments(FALSE);
//    $run_num = 90 * 4;
//    $get_all_payments = ch_get_paypal_all_payments(TRUE, $run_num);
//    $get_all_payments = get_option('ch_paypal_invoives_data');
//    print "<pre>";
//    print_r($get_all_payments);
//    print "</pre>";
}

add_shortcode('dev_test_shortcode', 'dev_test_shortcode_callback');

function ch_paypal_time_format($date) {
    if (!$date) {
        return FALSE;
    }
    return date('Y-m-d', strtotime($date)) . 'T' . date('h:i:s', strtotime($date)) . 'Z';
}

function ch_paypal_time_to_timestamp($date) {
    if (!$date) {
        return FALSE;
    }
    $date_time = str_replace('T', ' ', $date);
    $date_time = str_replace('Z', '', $date_time);
    return strtotime($date_time);
}

//   Inser user into table
function ch_paypal_payment_data_insert($data) {
    global $wpdb;
    $table_name = $wpdb->prefix . 'paypal_payments_data';
    $insert_data = [
        'payid' => isset($data['payid']) ? $data['payid'] : '',
        'cart' => isset($data['cart']) ? $data['cart'] : '',
        'customer_id' => isset($data['customer_id']) ? $data['customer_id'] : '',
        'customer_name' => isset($data['customer_name']) ? $data['customer_name'] : '',
        'customer_email' => isset($data['customer_email']) ? $data['customer_email'] : '',
        'customer_user_id' => isset($data['customer_user_id']) && $data['customer_user_id'] !== 'N/A' ? $data['customer_user_id'] : 0,
        'customer_pay_account' => isset($data['customer_pay_account']) ? $data['customer_pay_account'] : '',
        'item_type' => isset($data['item_type']) ? $data['item_type'] : '',
        'item_name' => isset($data['item_name']) ? $data['item_name'] : '',
        'total_ammount' => isset($data['total_ammount']) ? $data['total_ammount'] : '',
        'currency' => isset($data['currency']) ? $data['currency'] : '',
        'create_time' => isset($data['create_time']) ? ch_paypal_time_to_timestamp($data['create_time']) : '',
        'update_time' => isset($data['update_time']) ? ch_paypal_time_to_timestamp($data['update_time']) : '',
        'sale' => isset($data['sale']) ? serialize($data['sale']) : '',
        'refund' => isset($data['refund']) ? serialize($data['refund']) : '',
    ];
    $wpdb->insert($table_name, $insert_data);
}

function ch_paypal_payment_data_update($data) {
    if (!isset($data['payid']) || empty($data['payid'])) {
        return FALSE;
    }
    global $wpdb;
    $table_name = $wpdb->prefix . 'paypal_payments_data';
    $update_data = [
        'cart' => isset($data['cart']) ? $data['cart'] : '',
        'customer_id' => isset($data['customer_id']) ? $data['customer_id'] : '',
        'customer_name' => isset($data['customer_name']) ? $data['customer_name'] : '',
        'customer_email' => isset($data['customer_email']) ? $data['customer_email'] : '',
        'customer_user_id' => isset($data['customer_user_id']) && $data['customer_user_id'] !== 'N/A' ? $data['customer_user_id'] : 0,
        'customer_pay_account' => isset($data['customer_pay_account']) ? $data['customer_pay_account'] : '',
        'item_type' => isset($data['item_type']) ? $data['item_type'] : '',
        'item_name' => isset($data['item_name']) ? $data['item_name'] : '',
        'total_ammount' => isset($data['total_ammount']) ? $data['total_ammount'] : '',
        'currency' => isset($data['currency']) ? $data['currency'] : '',
        'create_time' => isset($data['create_time']) ? ch_paypal_time_to_timestamp($data['create_time']) : '',
        'update_time' => isset($data['update_time']) ? ch_paypal_time_to_timestamp($data['update_time']) : '',
        'sale' => isset($data['sale']) ? serialize($data['sale']) : '',
        'refund' => isset($data['refund']) ? serialize($data['refund']) : '',
    ];
    $wpdb->update($table_name, $update_data, ['payid' => $data['payid']]);
}

function ch_paypal_payment_data_check_row($value, $column = 'payid', $table = 'paypal_payments_data', $DefaultReturn = FALSE) {
    if (!$value) {
        return $DefaultReturn;
    }
    global $wpdb;
    $table_name = $wpdb->base_prefix . $table;
    $results = $wpdb->get_results("SELECT * FROM $table_name WHERE {$column} = '{$value}'");
    if ($results) {
        return $results;
    }
    return $DefaultReturn;
}

function ch_paypal_payment_data_is_table_exists($table = 'paypal_payments_data') {
    global $wpdb;
    $table_name = $wpdb->base_prefix . $table;
    $query = $wpdb->prepare('SHOW TABLES LIKE %s', $wpdb->esc_like($table_name));
    if (!$wpdb->get_var($query) == $table_name) {
        return FALSE;
    }
    return TRUE;
}

function ch_paypal_payment_data_table_create($table = 'paypal_payments_data') {
    global $wpdb;
    $table_name = $wpdb->base_prefix . 'paypal_payments_data';
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE IF NOT EXISTS `$table_name` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	 `payid` varchar(255) DEFAULT NULL,
	 `cart` varchar(255) DEFAULT NULL,
	 `customer_id` varchar(255) DEFAULT NULL,
	 `customer_name` varchar(255) DEFAULT NULL,
	 `customer_email` varchar(255) DEFAULT NULL,
	 `customer_user_id` varchar(255) DEFAULT NULL,
	 `customer_pay_account` varchar(255) DEFAULT NULL,
	 `item_type` varchar(255) DEFAULT NULL,
	 `item_name` varchar(255) DEFAULT NULL,
	 `total_ammount` varchar(255) DEFAULT NULL,
	 `currency` varchar(255) DEFAULT NULL,
	 `create_time` varchar(255) DEFAULT NULL,
	 `update_time` varchar(255) DEFAULT NULL,
	 `sale` varchar(255) DEFAULT NULL,
	 `refund` varchar(255) DEFAULT NULL,
	 PRIMARY KEY (`id`)
	) $charset_collate";
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta($sql);
}
add_action('wp_footer', 'dt_detect_unsubscribe_reminder_link');

function dt_detect_unsubscribe_reminder_link() {
    //unsubscribe from email link
    if ( isset ( $_GET[ 'unsubscribe' ] )
        and isset ( $_GET[ 'uid' ] )
        and is_numeric( $_GET[ 'uid' ] )
        and $user = get_user_by( 'ID', $_GET[ 'uid' ] )
        and $_GET[ 'unsubscribe' ] === get_user_meta( $_GET[ 'uid' ], '_unsubscribe', TRUE )
        )
    {
        update_user_meta( $_GET[ 'uid' ], 'emailed_date', '2070-01-01 00:00:00');

        delete_post_meta( $_GET[ 'uid' ], '_unsubscribe' );
        
        $headers = array( 
            'Content-Type: text/html;
            charset=UTF-8;
            From: ClubHub;' 
        );
        $message = '<p>Dear Tessa,</p>';

            $email = get_userdata($_GET[ 'uid' ])->user_email;
            $display_name = (!empty(get_userdata($_GET[ 'uid' ])->first_name)) ? get_userdata($_GET[ 'uid' ])->first_name . ' ' . get_userdata($_GET[ 'uid' ])->last_name : 'Sir/Madam';

            $message .= '<p><a href="https://clubhubuk.co.uk/wp-admin/user-edit.php?user_id=' . $_GET[ 'uid' ] . '">' . $display_name . '</a>, has unsubscribed from their renewal email<br>You can contact them by clicking this Email: <a href="mailto:' . $email . '">' . $email . '</a><br>';
    
        $email_success = wp_mail(
            'tessarobinson@club-hub-app.co.uk, david@dorset.tech',
            "ClubHub Member Unsubscribed from their Reminder Emails",
            $message,
            $headers
        );
        
        if (!$email_success) {
            file_put_contents('LogCronError.log', 'user_ID:'.$userID.' Email: '.$email.' Today Date: '.$current_date.' Tessa Email Failed '.PHP_EOL,FILE_APPEND);
        }
        
        ?>
        <script>
            jQuery(function($) {
                setTimeout( function () {
                    window.location.replace("https://clubhubuk.co.uk/unsubscribed/");
                }, 1000);
            });
        </script>
        <?php
}
}

/**
 * Remove Ancient Custom Fields metabox from post editor
 * because it uses a very slow query meta_key sort query
 * so on sites with large postmeta tables it is super slow
 * and is rarely useful anymore on any site
 */
function dt_remove_post_custom_fields_metabox() {
     foreach ( get_post_types( '', 'names' ) as $post_type ) {
         remove_meta_box( 'postcustom' , $post_type , 'normal' );   
     }
}
add_action( 'admin_menu' , 'dt_remove_post_custom_fields_metabox' );

