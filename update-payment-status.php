<?php 
	define('WP_USE_THEMES', false);
	require('wp-load.php');

	ini_set('memory_limit', '-1');
	ini_set('max_execution_time', 0);
    
    $get_author_ids = wpestate_get_property_author_ids();
    $last_run = [];
    if ($get_author_ids) {
        foreach ($get_author_ids as $key => $get_author) {
            $author_id = $get_author['user_id'];
            $invoiceField = get_user_meta($author_id, 'invoice_payment');
            if (!$invoiceField) {
                add_user_meta($author_id, 'invoice_payment', '');
                $added = "added invoice field";
            } else {
                $added = "already added";
            }
            $last_run[] = [
                'user' => $author_id,
                'added-invoice' => $added
            ];
        }
    }

    $get_paid_users = wpestate_get_all_users_by_pay_status('have');
    $last_run_2 = [];
    if ($get_paid_users) {
        $current_date_A = date('Y-m-d H:i:s');
        $current_date = strtotime($current_date_A);
        foreach ($get_paid_users as $get_paid_user) {
            
            
            $p_user_id = $get_paid_user['id'];
            
            if(empty($p_user_id)){
                file_put_contents('LogCronError.log', 'user_ID:'.$p_user_id.' no package id or name '.PHP_EOL,FILE_APPEND);
            }
            
            $p_user_status = $get_paid_user['pay_status'];
            $get_invoice = get_user_meta($p_user_id, 'invoice_payment', true);
            
            $get_paypal = get_user_meta($p_user_id, 'paypal_pack_transfer', true);
            $paypal_payment_date = $get_paypal[$p_user_id]['date'];

            $get_ex_date = wpestate_get_user_package_expiry_date($p_user_id);
            $package_id = get_user_meta($p_user_id, 'package_id', true);

            $billing_period = get_post_meta($package_id, 'biling_period', TRUE);
            $billing_period = strtolower($billing_period);
            $billing_period = $billing_period == 'day' ? 1 : ($billing_period == 'week' ? 7 : ($billing_period == 'month' ? 30 : ($billing_period == 'year' ? 365 : 0)));

            $paypal_payment_date = date('Y-m-d H:i:s', strtotime($paypal_payment_date . " + $billing_period day"));
            $paypal_payment_date = strtotime($paypal_payment_date);
            
            //filling pkgname or id field if empty
            $pkgId=get_user_meta($p_user_id,'package_id')[0];
            $pkgName=get_user_meta($p_user_id,'package_name')[0];
            if(empty($pkgId) AND empty($pkgName)){
                file_put_contents('LogCronError.log', 'user_ID:'.$p_user_id.' no package id or name '.PHP_EOL,FILE_APPEND);
            }else{
                if(empty($pkgName)){
                    $args = array(
                      'post_type'   => 'membership_package',
                      'include'=>array($pkgId)
                    );

                    $memberships = get_posts( $args );
                    if(!empty($memberships[0]->post_title)){
                        update_user_meta($p_user_id,'package_name',$memberships[0]->post_title);
                    }else{
                        file_put_contents('LogCronError.log', 'user_ID:'.$p_user_id.' no package id or name '.PHP_EOL,FILE_APPEND);
                    }

                }elseif(empty($pkgID)){
                    $args = array(
                      'post_type'   => 'membership_package',
                       "s" => $pkgName
                    );
                    $memberships = get_posts( $args );
                    if(!empty($memberships[0]->ID)){
                        update_user_meta($p_user_id,'package_id',$memberships[0]->ID);
                    }else{
                        file_put_contents('LogCronError.log', 'user_ID:'.$p_user_id.' no package id or name '.PHP_EOL,FILE_APPEND);
                    }
                }
            }
            
            
            
            $p_user_res = '';
            
            if ($current_date >= $get_ex_date && $package_id != 137320) { 
                update_user_meta($p_user_id, 'package_pay_status', 'unpaid');
                $p_user_res = 'identified activation date to be out of date set to unpaid';
                update_user_meta($p_user_id, 'package_listings', 0);
                $expiredIDs[] = dt_send_renewal_email($p_user_id, $current_date_A);
                
            } else if ($current_date <= $get_ex_date && $get_invoice == 'Invoice') {
                update_user_meta($p_user_id, 'package_pay_status', 'paid'); 
                update_user_meta($p_user_id, 'package_listings', -1); 
                $p_user_res = 'identified Invoice paid user setting payment status to paid';
                    
            } else if ($current_date <= $get_ex_date && $current_date <= $paypal_payment_date) {
                update_user_meta($p_user_id, 'package_pay_status', 'paid'); 
                update_user_meta($p_user_id,'package_activation',$get_paypal[$p_user_id]['date']);
                $p_user_res = 'used Paypal payment date to set payment status to paid';
                update_user_meta($p_user_id, 'package_listings', -1);
                
            } else if ($package_id == 137320 && $current_date <= $paypal_payment_date) {
                update_user_meta($p_user_id, 'package_pay_status', 'paid'); 
                $p_user_res = 'used Paypal payment date to set payment status to paid for a Monthly Sub';
                update_user_meta($p_user_id,'package_activation',$get_paypal[$p_user_id]['date']);
                update_user_meta($p_user_id, 'package_listings', -1);
                
            } else {
                $p_user_res = 'nothing to do';
            }
   
            //update_user_meta($p_user_id, 'wikipedia', $p_user_res);
            $last_run_2[] = [
                'current_date' => $current_date,
                'expiry_date' => $get_ex_date,
                'paypal' => $paypal_payment_date,
                'status' => $p_user_status,
                'res' => $p_user_res,
            ];


            //*******via api updating new payment date and status fields*******


            // Set request-specific fields.
            $PROFILEID = get_user_meta($p_user_id,'profile_id')[0];
            if(empty($PROFILEID)){
                continue;
            }
            $PROFILEID= str_replace('xxx', '-', $PROFILEID);
            // Add request-specific fields to the request string.
            $nvpStr = "&PROFILEID=$PROFILEID";
            // Execute the API operation; see the PPHttpPost function above.
            $recurringPaymentProfileDetails = PPHttpPost('GetRecurringPaymentsProfileDetails', $nvpStr); // Docuemntation => 
            if("SUCCESS" == strtoupper($recurringPaymentProfileDetails["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($recurringPaymentProfileDetails["ACK"])) {
               
                $response=$recurringPaymentProfileDetails ;
                if(!empty($response['STATUS'])){
                    $lastDate=urldecode($response['LASTPAYMENTDATE']);
                    $lastDate=str_replace('T', ' ', $lastDate);
                    $lastDate=str_replace('Z', '', $lastDate);
                    $period=urldecode($response['BILLINGPERIOD']);
                    $status=$response['STATUS'];
                    update_user_meta($p_user_id,'package_activation',$lastDate);
                    if(strtolower($status)=='active'){
                        $desc=urldecode($response['DESC']);
                        update_user_meta($p_user_id, 'wikipedia', 'Description from PayPal: ' . $desc);
                        if ($period === 'Month') {
                            update_user_meta($p_user_id,'package_id', 137320);
                            update_user_meta($p_user_id,'package_name', 'Monthly Membership');
                        } else if ( $period === 'Year') {
                            $pp_pack_id = 122972;
                            $pp_pack_name = "New Membership";
                            if ( dt_contains('Discounted', $desc) ) {
                                $pp_pack_id = 119535;
                                $pp_pack_name = "Discounted Membership";
                            } else if ( dt_contains('New', $desc) ) {
                                $pp_pack_id = 122972;
                                $pp_pack_name = "New Membership";
                            } else if ( dt_contains('Charity', $desc) ) {
                                $pp_pack_id = 120676;
                                $pp_pack_name = "Charity Membership";
                            }
                            update_user_meta($p_user_id,'package_id', $pp_pack_id);
                            update_user_meta($p_user_id,'package_name', $pp_pack_name);
                        }
                        update_user_meta($p_user_id,'package_pay_status','paid');
                        update_user_meta($p_user_id,'package_listings',-1);

                    }else{
                        update_user_meta($p_user_id,'package_pay_status',$status);
                    }
                }
            } else  {
                continue;
            }
            //****end update fields****
        }
        dt_send_tessa_update(array_values(array_filter($expiredIDs)), $current_date_A);
    }
    if (!empty($last_run)) {
        update_option('wpestate_pkg_validation_last_run', $last_run);
    }
    if (!empty($last_run_2)) {
        update_option('wpestate_pkg_validation_last_run2', $last_run_2);
    }
function dt_contains($needle, $haystack)
{
    return strpos($haystack, $needle) !== false;
}
function PPHttpPost($methodName_, $nvpStr_) {
        $environment = 'live';   //sandbox or 'beta-sandbox' or 'live'
        // Set up your API credentials, PayPal end point, and API version.
        $API_UserName = ( get_option('wp_estate_paypal_api_username', '') );
        $API_Password = ( get_option('wp_estate_paypal_api_password', '') );
        $API_Signature = ( get_option('wp_estate_paypal_api_signature', '') );

        $API_Endpoint = "https://api-3t.paypal.com/nvp";
        if("sandbox" === $environment || "beta-sandbox" === $environment) {
            $API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
        }
        $version = urlencode('51.0');
        // Set the curl parameters.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        // Turn off the server and peer verification (TrustManager Concept).
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        // Set the API operation, version, and API signature in the request.
        $nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
        // Set the request as a POST FIELD for curl.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
        // Get response from the server.
        $httpResponse = curl_exec($ch);
        if(!$httpResponse) {
            exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
        }
        // Extract the response details.
        $httpResponseAr = explode("&", $httpResponse);
        $httpParsedResponseAr = array();
        foreach ($httpResponseAr as $i => $value) {
            $tmpAr = explode("=", $value);
            if(sizeof($tmpAr) > 1) {
                $httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
            }
        }
        if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
            exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
        }
        return $httpParsedResponseAr;
    }
//$testuser = get_user_by('email', 'gitesh@dorset.tech')->ID;
//dt_send_renewal_email($testuser, $current_date_A);
function dt_send_renewal_email($userID, $current_date) {
    $emailed = get_user_meta($userID, 'emailed_date', true);
    if ($emailed) {
        $email_date = date('Y-m-d H:i:s', strtotime($emailed . " + 7 days"));
    } else {
        $email_date = date('Y-m-d H:i:s');
    }
    $email = get_userdata($userID)->user_email;
    $unique = md5( $userID );
    
    $unsubscribe = 'https://clubhubuk.co.uk/';
    $unsubscribe = add_query_arg(
        array(
            'unsubscribe'   => $unique,
            'uid'           => $userID
        ),
        $unsubscribe
    );    
    
    if ($current_date >= $email_date) {
        
        update_user_meta( $userID, '_unsubscribe', $unique);
        
        $headers = array( 
            'Content-Type: text/html;
            charset=UTF-8;
            From: ClubHub;' 
        );
        $message = '<p>Dear ';
        
        $message .= (get_userdata($userID)->first_name) ? get_userdata($userID)->first_name : 'Sir/Madam';
        
        $message .= ', Your Club Hub membership is up for renewal.</p>
            <p><a href="https://clubhubuk.co.uk/my-subscription/">Please login to your Club Hub account and visit your subscription page to renew your subscription - https://clubhubuk.co.uk/my-subscription/</a></p>
            <p>We hope you\'ve found everything you need to get your activities listed on our site.</p>';
        
        $message .= '<br>';
        
        $message .= "<p>You can use our hashtag - #clubhubmember in you social media posts and we will engage in them.</p><br>";

        $message .= '<p>You can also tag us in your Instagram stories <a href="https://www.instagram.com/clubhubuk/?hl=en">@clubhubuk</a> and we will share them.</p>';

        $message .= "<p>If you haven't already downloaded our app you can do so on the <a href='https://itunes.apple.com/in/app/club-hub-uk-free/id1139824440?mt=8'>App Store</a> or <a href='https://play.google.com/store/apps/details?id=uk.co.clubhubukfree'>Google Play</a></p>";

        $message .= '<p>You can join our <a href="https://www.facebook.com/pg/clubhubapp/groups/">Hub of Facebook Groups</a> to connect with kids activity providers across the country. and our <a href="https://www.facebook.com/groups/837432733107556">Kids Activity Providers on Club Hub UK</a> Facebook Group to connect with providers across the country.</p>';

        $message .= "<p>Please follow us on our <a href='https://instagram.com/clubhubuk'>Instagram</a>, <a href='https://www.facebook.com/clubhubapp/'>Facebook</a> and <a href='https://twitter.com/clubhubuk'>Twitter</a> pages</p>";
        
        $message .= "<p>Reminder Date: " . $email_date . "</p><p>Current Date: " . $current_date . "</p>";
        
        $message .= '<p>Please reply to this email to <a href="' . $unsubscribe . '">Unsubscribe</a></p>';
        
        $email_success = /* true; */ wp_mail(
            //$email,
             'david@dorset.tech',
            "Your ClubHub Subscription has expired",
            $message,
            $headers
        );
        if ($email_success) {
            update_user_meta($userID, 'emailed_date', $current_date);
            return $userID;
        } else {
            file_put_contents('LogCronError.log', 'user_ID:'.$userID.' Email: '.$email.' Today Date: '.$current_date.' Last Email Date: '.$email_date.' subscription reminder email failed '.PHP_EOL,FILE_APPEND);
        }
    }
}

function dt_send_tessa_update($expired, $current_date) {
    
    $headers = array( 
        'Content-Type: text/html;
        charset=UTF-8;
        From: ClubHub;' 
    );
    $message = '<p>Dear Tessa,</p><p>Please find a list of the expired clubs who have just been emailed their subscription reminders: <br>';
    
    foreach ( $expired as $userID ) {
        $emailed = get_user_meta($userID, 'emailed_date', true);
        if ($emailed) {
            $email_date = date('Y-m-d H:i:s', strtotime($emailed . " + 7 days"));
        } else {
            $email_date = date('Y-m-d H:i:s');
        }
        $email = get_userdata($userID)->user_email;
        $display_name = (!empty(get_userdata($userID)->first_name)) ? get_userdata($userID)->first_name . ' ' . get_userdata($userID)->last_name : 'Sir/Madam';
        
        $message .= '<p>Name: <a href="https://clubhubuk.co.uk/wp-admin/user-edit.php?user_id=' . $userID . '">' . $display_name . '</a>, Email: <a href="mailto:' . $email . '">' . $email . '</a><br>';

        $message .= "Reminder Date: " . $email_date . " Current Date: " . $current_date . "</p>";
    }
    if (!empty($expired)) {
        $email_success = wp_mail(
            'tessarobinson@club-hub-app.co.uk, david@dorset.tech',
            "ClubHub Subscription Expiries",
            $message,
            $headers
        );
    } else {
        file_put_contents('LogCronError.log', 'Tessa Email Failed No Reminders were sent so no need to report'.PHP_EOL,FILE_APPEND);
        return;
    }
    if (!$email_success) {
        file_put_contents('LogCronError.log', 'user_ID:'.$userID.' Email: '.$email.' Today Date: '.$current_date.' Last Email Date: '.$email_date.' Tessa Email Failed '.PHP_EOL,FILE_APPEND);
    }
}
?>