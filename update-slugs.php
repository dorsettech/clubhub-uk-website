<?php

define('WP_USE_THEMES', true);

require('wp-load.php');

ini_set('memory_limit', '-1');
ini_set('max_execution_time', 0);

global $wpdb;

wp_suspend_cache_addition(true);
set_time_limit(0);

$cnt = 0;

$args = array(
    'post_type' => 'estate_property',
    'post_status' => 'publish',
    'cache_results' => false,
    'update_post_meta_cache' => false,
    'update_post_term_cache' => false,
    'nopaging' => 'true',
    //'posts_per_page' => 2,
    'orderby' => 'ID',
    'order' => 'ASC'
);

//$path = get_template_directory() . '/slugs.txt';
$myFile = fopen("update-slugs.txt", "a+");
// get all posts
$posts = get_posts($args);
foreach ($posts as $post) {
    // Wordpress Regenerate Slugs From Title of Posts
    $new_slug = sanitize_title($post->post_title);

    // check the slug and run an update if not same as title
    if ($post->post_name != $new_slug) {
        if (post_exists_by_slug($new_slug)) {
            continue;
        }
        $cnt++;
        echo $post->ID . '=>' . $post->post_name . '!=' . $new_slug . '<br>';
        fwrite($myFile, 'Sr. No. => ' . $cnt . PHP_EOL);
        fwrite($myFile, 'Post ID => ' . $post->ID . PHP_EOL);
        fwrite($myFile, 'Post Title => ' . $post->post_title . PHP_EOL);
        fwrite($myFile, 'Post Slug Old => ' . $post->post_name . PHP_EOL);
        fwrite($myFile, 'Post Slug New => ' . $new_slug . PHP_EOL);
        fwrite($myFile, '------------------------' . PHP_EOL);
         wp_update_post(
          array (
          'ID'        => $post->ID,
          'post_name' => $new_slug
          )
          ); 
    }
}
fclose($myFile);
echo '<br>Total posts updated = ' . $cnt."<br>";

/**
 * Check if post exists by slug.
 *
 * @params string $post_slug    
 * @return mixed boolean false if no posts exist; post ID otherwise.
 */
function post_exists_by_slug($post_slug) {
    $loop_posts = new WP_Query(array('post_type' => 'estate_property', 'post_status' => 'any', 'name' => $post_slug, 'posts_per_page' => 1, 'fields' => 'ids'));
    return ( $loop_posts->have_posts() ? $loop_posts->posts[0] : false );
}

wp_reset_query();
wp_suspend_cache_addition(false);

echo "works fine!!";
exit;